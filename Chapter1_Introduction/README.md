### Chapter 1 - Introduction

This folder contains a jupyter notebook, "make_figures.ipynb", used for generating the figures in the thesis introduction chapter. Once generated, the figures are stored in the "Figures" folder. All figures are subject to some amount of post-processing.