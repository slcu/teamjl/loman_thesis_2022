\(\displaystyle\mathbf{\dot{X} = v_0 + v\frac{K^n}{Y^n+K^n} - X}\)
\(\displaystyle\mathbf{\dot{Y} = v_0 + v\frac{(i \cdot K)^n}{X^n+(i \cdot K)^n} - Y}\)
