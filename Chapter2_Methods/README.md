### Chapter 2 - Methods

This folder contains a jupyter notebook, "make_figures.ipynb", used for generating the figures in the thesis methods chapter. Once generated, the figures are stored in the "Figures" folder. All figures are subject to some amount of post-processing. The "BifurcationPoints" folder contains a separate script for generating figure 2.7.