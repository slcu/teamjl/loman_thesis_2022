### Chapter 3 - Catalyst

This folder contains a jupyter notebook, "make_figures.ipynb", used for generating the figures in the thesis Catalyst chapter. Once generated, the figures are stored in the "Figures" folder. All figures are subject to some amount of post-processing. The "benchmarks" folder contains separate code for running all the benchmarks.