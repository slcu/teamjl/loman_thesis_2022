### Fetches Required Packages ###
using Catalyst


### Declares Models ###

# Sets base parameter values.
v0 = 0.1; v = 2.5; K = 75.; n = 2;
deg = 0.01; L = 0.;

# Minimal σV model (containing σV only). 
σVmin_network = @reaction_network begin
    v0 + hill(L*σᵛ,v,K,n), ∅ → σᵛ
    deg, σᵛ → ∅
end v0 v K n deg L
σVmin_model = Model(σVmin_network,[v0, v, K, n, deg, L]);


# Sets base parameter values.
v0 = 0.1; v = 2.5; K = 60; n = 2;
kD = 5; kB = 10; kC = 0.05;
deg = 0.01; L = 0.; 

# Normal σV model, without inducable production.
σV_network = @reaction_network begin
    v0 + hill(σᵛ,v,K,n), ∅ → (σᵛ+A)
    deg, (σᵛ,A,Aσᵛ) → ∅
    (kB,kD), A + σᵛ ↔ Aσᵛ
    L*kC*Aσᵛ, Aσᵛ ⟾ σᵛ
end v0 v K n kD kB kC deg L
σV_model = Model(σV_network,[v0, v, K, n, kD, kB, kC, deg, L]);


# Expanded σV model which allows for inducing σV and RsiV production.
σVi_network = @reaction_network begin
    v0 + hill(σᵛ,v,K,n), ∅ → (σᵛ+A)
    (oS,oA), ∅ ⟾ (σᵛ,A)
    deg, (σᵛ,A,Aσᵛ) → ∅
    (kB,kD), A + σᵛ ↔ Aσᵛ
    L*kC*Aσᵛ, Aσᵛ ⟾ σᵛ
end v0 v K n kD kB kC deg L oS oA;
σVi_model = Model(σVi_network,[v0, v, K, n, kD, kB, kC, deg, L, 0., 0.]);

# Model of the σV network, with an extra copy of the (σV activated) σV operon (sV and rV is equal to 0 or 1, determines which gene(s) is doubled).
σV2_network = @reaction_network begin
    v0 + hill(σᵛ,v,K,n), ∅ → (σᵛ+A)
    sV*(v0 + hill(σᵛ,v,K,n)), ∅ → σᵛ
    rV*(v0 + hill(σᵛ,v,K,n)), ∅ → A
    deg, (σᵛ,A,Aσᵛ) → ∅
    (kB,kD), A + σᵛ ↔ Aσᵛ
    L*kC*Aσᵛ, Aσᵛ ⟾ σᵛ
end v0 v K n kD kB kC deg L sV rV;
σV2_model = Model(σV2_network,[v0, v, K, n, kD, kB, kC, deg, L, 0., 0.])

# Model for running the main model using the CLE. Then network is identical, but the parameters different.
σV_cle_model = Model(deepcopy(σV_network),[0.025,1.75,50.0,2,5.0,100.0,0.025,0.01,0.]);