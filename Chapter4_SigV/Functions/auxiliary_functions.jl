function terminated_activated_sim_cb(activation_threshold)
	condition_tas(u,t,integrator) = (u[1]>activation_threshold)&&(t>0)
	affect_tas!(integrator) = terminate!(integrator)
	terminated_activated_sim_cb = DiscreteCallback(condition_tas,affect_tas!,save_positions = (false,false));
end
