# Plots the cummulative activation plots for a single activation simulation.
cummulative_activation_plot(args...;kwargs...) = (plot(); cummulative_activation_plot!(args...;kwargs...);)
function cummulative_activation_plot!(sols,max_activity;start=-500,stop=2000,startT=start,stopT=stop,var=1,kwargs...)
    start_idx = findfirst(sols[1].t .== start); stop_idx = findfirst(sols[1].t .== stop);
    startT_idx = findfirst(sols[1].t .== startT); stopT_idx = findfirst(sols[1].t .== stopT);
    distribution = map(i ->  mean(map(sol -> on_value(sol.u[i][var],max_activity), sols)), 1:length(sols[1].t))
    return plot!(sols[1].t[startT_idx:stopT_idx], distribution[start_idx:stop_idx]; xlabel="Time (Minutes)", ylabel = "Fraction of activated cells",legend=:bottomright,framestyle=:box,grid=false,guidefontsize=13,labelfontsize=13,ylimit=(-0.015,1.03),tickfontsize=9,right_margin=3mm,title="",titlefontsize=20,kwargs...)
end

# Plots the cummulative activation plots for a set of activation simulations.
cummulative_activations_plot(args...;kwargs...) = (plot(); cummulative_activations_plot!(args...;kwargs...))
function cummulative_activations_plot!(sols,l_levels,idxs,max_activities::Union{Float64,Vector{Float64}};starts=fill(-500,length(sols)),stops=fill(2000,length(sols)),startsT=starts,stopsT=stops,label_base="Lysozyme concentration",label_end="(au)",kwargs...)
    (max_activities isa Float64) && (max_activities = fill(max_activities,length(sols)))
    for idx in idxs
        cummulative_activation_plot!(sols[idx],max_activities[idx];start=starts[idx],stop=stops[idx],startT=startsT[idx],stopT=stopsT[idx],label="$(label_base) = $(l_levels[idx]) $(label_end)",kwargs...)
    end
    plot!()
end

# Calculates the degree with which a simulation is "on".
function on_value(val,max_activity)  # Value modified (instead of just being 1 or 0), to make plots smoother.
    (val < 0.20*max_activity) && (return 0.0)
    (val > 0.70*max_activity) && (return 1.0)
    return (val-0.20*max_activity)/(0.50*max_activity)
end

# Plots the fold changes due to various levels of stress.
function plot_fold_changes(inactive_values,active_valuess,l_levels,idxs; xguide="Lysozyme concentration (au)", yguide= "Fold change", kwargs...)
    mean_inactive = mean(inactive_values)
    fold_changes = map(active_values -> active_values ./ mean_inactive, active_valuess)
    return plot(map(i -> l_levels[i], idxs),mean.(fold_changes),ribbon=std.(fold_changes);xguide=xguide,yguide=yguide,kwargs...)
end

# Creates plots of the corresponding expression profiles.
plot_expression_profile(mf,color;kwargs...) = (plot(); plot_expression_profile!(mf,color;kwargs...);)
function plot_expression_profile!(mf,color;label="",kwargs...)
    foreach(entry -> density!(entry;color=color,linealpha=0.8,label="",kwargs), mf[1,:])
    density!(vcat(mf[1,:]...);color=color,linestyle=:dash,linealpha=0.6,lw=7,label="",kwargs...)
    plot!([],[],xguide="PsigV-YFP (au)",yguide="Fraction",color=color,label=label)
end;
