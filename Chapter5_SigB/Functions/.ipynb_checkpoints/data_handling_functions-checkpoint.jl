### Fetch Packages ###
using Serialization
using Interpolations


### Sets Path To Data ###
if isdir("Data/Parameter_scan")
    datadir = "Data/Parameter_scan"
elseif isdir("../../loman_thesis_2022_uncommited/Chapter5_SigB/Data/Parameter_scan")
    datadir = "../../loman_thesis_2022_uncommited/Chapter5_SigB/Data/Parameter_scan"
else
    error("Data directory not found.")
end

### Evaluation Structures ###

# Stores the evaluation of a single parameter set.
struct Evaluation
    init_peaks::Vector{Float64}
    ss_peaks::Vector{Float64}
    init_peak_mean::Float64
    ss_peak_mean::Float64

    pre_stress_mean::Float64
    post_stress_mean::Float64
    post_stress_median::Float64

    post_stress_phos_means::Vector{Float64}
    post_stress_phos_stds::Vector{Float64}
    post_stress_phos_negatives::Vector{Int64}
end

# Stores the evaluations over a grid of parameter sets.
struct Evaluations
    evaluations::Dict{Vector{Float64},Evaluation}
    p_changes_fixed::Vector{Pair{Symbol,Float64}}
    parameter_syms::Vector{Symbol}
    parameter_grids::Vector{Vector{Float64}}

    # Creates it directly from the fields.
    function Evaluations(evaluations::Dict{Vector{Float64},Evaluation},p_changes_fixed::Vector{Pair{Symbol,Float64}},parameter_syms::Vector{Symbol},parameter_grids::Vector{Vector{Float64}})
        return new(evaluations,p_changes_fixed,parameter_syms,parameter_grids)
    end
    # Loads a set of evaluations, and some metadata.    
    function Evaluations(target_folder,iteration)
        new(deserialize(datadir*"/$(target_folder)/iteration_$(iteration).jls")...)
    end
    # Loads all simulations, and some metadata.
    function Evaluations(target_folder)
        files = filter(filename -> "evaluation"==filename[1:10], readdir(datadir*"/$(target_folder)"))
        evaluations = deserialize(datadir*"/$(target_folder)/$(files[1])")
        for file in files[2:end]
            merge!(evaluations.evaluations,deserialize(datadir*"/$(target_folder)/$(file)").evaluations)
        end
        new(evaluations...)
    end
end
get_p_idx(ev::Evaluations,par::Symbol) = findfirst(ev.parameter_syms .== par)


### Specialised Evaluation Structures ###

# Generates a vector grid of evaluations (for plotting).
struct EvaluationVector
    evaluations::Vector{Evaluation}
    par::Symbol
    grid::Vector{Float64}

    function EvaluationVector(evs,par,p_base)
        idx = get_p_idx(evs,par);
        grid = evs.parameter_grids[idx]; 
        evaluations = [evs.evaluations[setindex!(deepcopy(p_base),val,idx)] for val in grid]
        new(evaluations,par,grid)
    end
end

# Generates a matrix grid of evaluations (for heatmap plotting).
struct EvaluationMatrix
    evaluations::Matrix{Evaluation}
    par1::Symbol
    grid1::Vector{Float64}
    par2::Symbol
    grid2::Vector{Float64}

    function EvaluationMatrix(evs,par1,par2,p_base)
        idx1 = get_p_idx(evs,par1); idx2 = get_p_idx(evs,par2);
        grid1 = evs.parameter_grids[idx1]; grid2 = evs.parameter_grids[idx2]; 
        evaluations = [evs.evaluations[setindex!(setindex!(deepcopy(p_base),val1,idx1),val2,idx2)] for val1 in grid1, val2 in grid2]
        new(evaluations,par1,grid1,par2,grid2)
    end
end


### Dense Grids ###

# Creates a dense grid of a vector.
function dense_grid(x_grid,grid::Vector;dens=100,alg=BSpline(Quadratic(Reflect(OnCell()))))
    itp = interpolate(grid, alg);
    x_grid_dense = lin_grid(x_grid[1],x_grid[end],dens*length(x_grid))
    grid_dense = [itp(x) for x in range(1,stop=size(itp)[1],length=length(x_grid_dense))];
    return (x_grid_dense,grid_dense)
end

# Creates a dense grid of a matrix.
function dense_grid(y_grid,x_grid,grid::Matrix;dens=100,alg=BSpline(Quadratic(Reflect(OnCell()))))
    itp = interpolate(grid, alg);
    x_grid_dense = lin_grid(x_grid[1],x_grid[end],dens*length(x_grid))
    y_grid_dense = lin_grid(y_grid[1],y_grid[end],dens*length(y_grid))
    grid_dense = [itp(x,y) for x in range(1,stop=size(itp)[1],length=length(x_grid_dense)), y in range(1,stop=size(itp)[2],length=length(y_grid_dense))];
    return (y_grid_dense,x_grid_dense,grid_dense)
end;