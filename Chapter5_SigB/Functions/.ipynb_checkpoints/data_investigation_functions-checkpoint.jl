### Fetch Packages ###
using StatsBase


### Meassures System Properties ###

# Meassure of single evaluation properties.
initial_pulse_degree(ev) = ev.init_peak_mean/ev.ss_peak_mean
stochastic_pulsing_degree(ev) = ev.ss_peak_mean/ev.post_stress_mean
initial_pulsing_minus_stochastic_pulsing_degree(ev) = initial_pulse_degree(ev) - stochastic_pulsing_degree(ev)
stochastic_pulsing_modified_degree(ev) = ev.ss_peak_mean/(ev.post_stress_mean*max(ev.init_peak_mean/ev.ss_peak_mean,1.))
differential_pulsing_degree(ev) = ev.post_stress_mean*ev.init_peak_mean/(ev.ss_peak_mean^2)
initial_pulse_abs(ev) = ev.init_peak_mean
initial_pulse_abs_mc(ev) = ev.init_peak_mean/ev.post_stress_mean
stochastic_pulsing_abs(ev) = ev.ss_peak_mean
initial_pulse_no_init_degree(ev) = ev.ss_peak_mean/ev.init_peak_mean

# Meassure of evaluation vector properties.
function get_areas(ev::EvaluationVector)
    ais = 0; ass = 0; ac = 0;
    for e in ev.evaluations
        ipd = initial_pulse_degree(e)
        spd = stochastic_pulsing_degree(e);
        ac += min(ipd,spd)
        (ipd>spd) ? (ais += ipd-spd) : (ass += spd-ipd)
    end
    return (ais,ass,ac)
end
initial_vs_stochastic_pulsing(ev::EvaluationVector) = (areas = get_areas(ev); return sqrt(areas[1]*areas[2])/sum(areas);)
initial_vs_else(ev::EvaluationVector) = (areas = get_areas(ev); return areas[1]/sum(areas);)
stochastic_vs_else(ev::EvaluationVector) = (areas = get_areas(ev); return areas[2]/sum(areas);)
maximum_val(ev::EvaluationVector,f::Function) = maximum(f.(ev.evaluations))


### Sorts Evaluations ###

# Get list of evaluations sorted by function.
sorted_evs(evs::Evaluations,f) = sort(collect(deepcopy(evs.evaluations)),by=ev->f(ev[2]))
sorted_evs(evs::Dict{Vector{Float64},Evaluation},f) = sort(collect(deepcopy(evs)),by=ev->f(ev[2]))

# Sorts evaluations, but allows several functions and fixed dimensions.
function multi_dimnesional_sort(evs,fs,fs_combine,fixed_dims)
    parameter_combinations = [collect(Iterators.product(map(fd -> evs.grids[get_p_idx(evs,fd)], fixed_dims)...))...]
    evaluations = Vector{Tuple{Float64,Vector{Pair{Vector{Float64},Evaluation}}}}(undef,length(parameter_combinations))
    for i = 1:length(parameter_combinations)
        fd_vals_match(ev) = all(map(j-> ev[1][get_p_idx(evs,fixed_dims[j])]==parameter_combinations[i][j], 1:length(fixed_dims)))
        ev_sorts = map(f -> sorted_evs(filter(fd_vals_match,evs.evaluations),f) ,fs)
        ev_val = fs_combine(map(j->fs[j](ev_sorts[j][1]), 1:length(fs)))
        evaluations[i] = (ev_val,map(j -> ev_sorts[j][1], 1:length(fs)))
    end
    return sort(evaluations, by=ev->ev[1])
end


### Displays Evaluation Information ###

# Displays a single evaluation.
function display_evaluation(ev)
    display(Text("\n"))
    # Mean pulse length
    display(Text("Mean initial pulse heigth: $(ev.init_peak_mean)"))
    display(Text("Mean steady state pulse heigth: $(ev.ss_peak_mean)"))
    display(Text("Relative size difference: $(ev.init_peak_mean/ev.ss_peak_mean)"))
    display(Text("\n\n"))

    # Variation in length.
    bootstrap_n = 20
    quater_leng = floor(Int64,length(ev.init_peaks)/4)
    init_bootstrap = map(i -> mean(rand(ev.init_peaks,quater_leng)), 1:bootstrap_n)
    ss_bootstrap = map(i -> mean(rand(ev.ss_peaks,quater_leng)), 1:bootstrap_n)
    display(Text("Mean initial pulse heigth bootstrap (n=(bootstrap_n)) std: $(std(init_bootstrap))"))
    display(Text("Mean steady state pulse heigth bootstrap (n=(bootstrap_n)): $(std(ss_bootstrap))"))
    display(Text("\n\n"))

    # Show pulse distribution.
    density(ev.init_peaks,lw=2.5,la=0.8,label="Initial Pulse")
    density!(ev.ss_peaks,lw=2.5,la=0.8,label="Steady State Pulse",xguide="Mean Pulse Heigth")
    display(plot!(size=(700,400)))
    display(Text("\n\n"))

    # Show mean activation differences.
    plot([0.,1.,1.,3.],[ev.pre_stress_mean,ev.pre_stress_mean,ev.post_stress_mean,ev.post_stress_mean],lw=5,label="Mean activities",color=:lightgreen)
    plot!([1.,1.,3.],[ev.pre_stress_mean,ev.post_stress_median,ev.post_stress_median],lw=3,label="Median activities",color=:darkgreen,linestyle=:dash)
    plot!([1.,1.],[pre_stress_mean,ev.init_peak_mean],color=1,lw=3,linestyle=:dash,label="Initial Pulse")
    plot!([2.,2.],[post_stress_mean,ev.ss_peak_mean],color=2,lw=3,linestyle=:dash,label="Steady State Pulse")
    display(plot!(legend=:topleft,legendfontsize=8,size=(700,250),xlimit=(0.,3.),xticks=[]))
end

# For a matrix of evaluations, returns a corresponding set of heatmaps.
function display_heatmaps(evs,par1,par2,p_base)
    em = EvaluationMatrix(evs,par1,par2,p_base)

    init_pulse_degree_hm = plot_evaluation!(em,initial_pulse_degree)
    ss_pulse_degree_hm = plot_evaluation!(em,stochastic_pulsing_degree)
    init_pulse_abs_hm = plot_evaluation!(em,ev->ev.init_peak_mean)
    ss_pulse_abs_hm = plot_evaluation!(em,ev->ev.ss_peak_mean)
    mean_levels_hm = plot_evaluation(em,ev->ev.post_stress_mean)
    median_levels_hm = plot_evaluation(em,ev->ev.post_stress_median)
    
    return plot(init_pulse_degree_hm,ss_pulse_degree_hm,init_pulse_abs_hm,ss_pulse_abs_hm,mean_levels_hm,median_levels_hm,left_margin=5mm,bottom_margin=3mm,top_margin=3mm,titlefontsize=13,size=(1400,1400),layout=(3,2))
end
