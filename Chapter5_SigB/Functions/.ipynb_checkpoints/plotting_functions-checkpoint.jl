### Fetch Packages ###
using Plots, Plots.Measures
using StatsBase
using StatsPlots


### General Functions ###

# Reduces the decimals in a Float numer.
short_dec(num;places=3) = floor(Int64,num*10^places)/10^places
# Makes a linnear grid.
lin_grid(start,stop,n) = [range(start,stop=stop,length=n)...]
# Makes a logarithmic grid.
log_grid(start,stop,n)= 10 .^ range(log10(start),stop=log10(stop),length=n)


### Basic Evaluation Plots ###

# Plots an evaluation vector.
plot_evaluation(evs::Evaluations,par::Symbol,p_base::Vector{Float64},f::Function;kwargs...) = plot_evaluation(EvaluationVector(evs,par,p_base),f;kwargs...)
plot_evaluation!(evs::Evaluations,par::Symbol,p_base::Vector{Float64},f::Function;kwargs...) = plot_evaluation!(EvaluationVector(evs,par,p_base),f;kwargs...)
plot_evaluation(args...;kwargs...) = (plot(); plot_evaluation!(args...;kwargs...);)
function plot_evaluation!(evs::EvaluationVector,f::Function;label="",dense=false,dense_dens=100,dense_alg=BSpline(Quadratic(Reflect(OnCell()))),kwargs...)
    dense && return plot!(dense_grid(evs.grid,f.(evs.evaluations),dens=dense_dens,alg=dense_alg)...;xlimit=(evs.grid[1],evs.grid[end]),label=label,xguide="$(evs.par)",kwargs...)
    return plot!(evs.grid,f.(evs.evaluations);xlimit=(evs.grid[1],evs.grid[end]),label=label,xguide="$(evs.par)",kwargs...)
end

# Plots two evaluation vectors.
plot_evaluations(evs::Evaluations,par::Symbol,p_base::Vector{Float64},f1::Function,f2::Function;kwargs...) = plot_evaluations(EvaluationVector(evs,par,p_base),f1,f2;kwargs...)
function plot_evaluations(evs::EvaluationVector,f1::Function,f2::Function;labels=["" ""],titles=["" ""],dense=false,dense_dens=100,dense_alg=BSpline(Quadratic(Reflect(OnCell()))),kwargs...)
    return plot(plot_evaluation(evs,f1;label=labels[1],title=titles[1],dense=dense,dense_dens=dense_dens,dense_alg=dense_alg),plot_evaluation(evs,f2;label=labels[2],title=titles[2],dense=dense,dense_dens=dense_dens,dense_alg=dense_alg);kwargs...)
end

# Plots an evaluation matrix.
plot_evaluation(evs::Evaluations,par1::Symbol,par2::Symbol,p_base::Vector{Float64},f::Function;kwargs...) = plot_evaluation(EvaluationMatrix(evs,par1,par2,p_base),f;kwargs...)
plot_evaluation!(evs::Evaluations,par1::Symbol,par2::Symbol,p_base::Vector{Float64},f::Function;kwargs...) = plot_evaluation!(EvaluationMatrix(evs,par1,par2,p_base),f;kwargs...)
plot_evaluation(args...;kwargs...) = (plot(); plot_evaluation!(args...;kwargs...);)
function plot_evaluation!(evs::EvaluationMatrix,f::Function;hm_min=-Inf,hm_max=Inf,legend=:right,dense=false,dense_dens=100,dense_alg=BSpline(Quadratic(Reflect(OnCell()))),kwargs...)
    dense && return heatmap!(dense_grid(evs.grid2,evs.grid1,f.(evs.evaluations),dens=dense_dens,alg=dense_alg)...;xguide="$(evs.par2)",yguide="$(evs.par1)",clim=(hm_min,hm_max),legend=legend,kwargs...)
    return heatmap!(evs.grid2,evs.grid1,f.(evs.evaluations);xguide="$(evs.par2)",yguide="$(evs.par1)",clim=(hm_min,hm_max),legend=legend,kwargs...)
end

# Plots two evaluation matrices.
plot_evaluations(evs::Evaluations,par1::Symbol,par2::Symbol,p_base::Vector{Float64},f1::Function,f2::Function;kwargs...) = plot_evaluations(EvaluationMatrix(evs,par1,par2,p_base),f1,f2;kwargs...)
function plot_evaluations(evs::EvaluationMatrix,f1::Function,f2::Function;labels=["" ""],titles=["" ""],hm_min=-Inf,hm_max=Inf,dense=false,dense_dens=100,dense_alg=BSpline(Quadratic(Reflect(OnCell()))),kwargs...)
    return plot(plot_evaluation(evs,f1;label=labels[1],title=titles[1],hm_min=hm_min,hm_max=hm_max,dense=dense,dense_dens=dense_dens,dense_alg=dense_alg),plot_evaluation(evs,f2;label=labels[2],title=titles[2],hm_min=hm_min,hm_max=hm_max,dense=dense,dense_dens=dense_dens,dense_alg=dense_alg);kwargs...)
end

# Plots a grid of evaluation plots (vector).
function plot_evaluation_grids(evs,f,par,p_base,meta_par1,meta_grid1,meta_par2,meta_grid2;kwargs...)
    idx1 = get_p_idx(evs,meta_par1); idx2 = get_p_idx(evs,meta_par2);
    plots = [plot_evaluation(Eevs,par,setindex!(setindex!(deepcopy(p_base),val1,idx1),val2,idx2),f) for val1 in meta_grid1, val2 in meta_grid2]
    return plot(plots...;kwargs...)
end

# Plots a grid of evaluation plots (matrix).
function plot_evaluation_grids(evs,f,par1,par2,p_base,meta_par1,meta_grid1,meta_par2,meta_grid2;kwargs...)
    idx1 = get_p_idx(evs,meta_par1); idx2 = get_p_idx(evs,meta_par2);
    plots = [plot_evaluation(EvaluationMatrix(evs,par1,par2,setindex!(setindex!(deepcopy(p_base),val1,idx1),val2,idx2)),f) for val1 in meta_grid1, val2 in meta_grid2]
    return plot(plots...;kwargs...)
end


### Specialised Plots ###

# Plots the distinctness areas and labels them.
function make_behaviour_distinctness_area_plot(evs,η2,scale;display_data=false,set_title=true,n=1000,b_fun_1=initial_pulse_degree,b_fun_2=stochastic_pulsing_degree,b_name_1="Initial Pulsing",b_name_2="Stochastic Pulsing")
    ev = EvaluationVector(evs,:pProd,[0.,η2,scale]);
    v_ip = b_fun_1.(ev.evaluations)
    v_sp = b_fun_2.(ev.evaluations)

    itp_ip = interpolate(v_ip, BSpline(Linear()))(range(1,stop=length(v_ip),length=n))
    itp_sp = interpolate(v_sp, BSpline(Linear()))(range(1,stop=length(v_sp),length=n))
    itp_min = min.(itp_ip,itp_sp)

    ipa = (sum(itp_ip.-itp_min))*(ev.grid[end]-ev.grid[1])/n
    spa = (sum(itp_ip.-itp_min))*(ev.grid[end]-ev.grid[1])/n
    ca = sum(itp_min)*(ev.grid[end]-ev.grid[1])/n
    if display_data
        println("$(b_name_1) Area: $(ipa)")
        println("$(b_name_2) Area: $(spa)")
        println("Combined Area: $(ca)")
        println("Distinctness Measure = $(sqrt(ipa*spa)/(ipa+spa+ca))")
    end
    title = set_title ? "η2 = $(short_dec(η2)), scale = $(short_dec(scale))       Distinctness = $(short_dec(sqrt(ipa*spa)/(ipa+spa+ca)))" : ""

    plot(range(ev.grid[1],stop=ev.grid[end],length=n),itp_ip; color=:blue,lw=4,fillrange=itp_min,fillalpha=0.5,label="")
    plot!(range(ev.grid[1],stop=ev.grid[end],length=n),itp_sp; color=:red,lw=4,fillrange=itp_min,fillalpha=0.5,label="")
    plot!(range(ev.grid[1],stop=ev.grid[end],length=n),itp_min; color=:purple,lw=0,fillrange=zeros(n),fillalpha=0.5,label="")
    plot!(xlimit=(ev.grid[1],ev.grid[end]),ylimit=(0,1.1*max(v_ip...,v_sp...)),xguide="pProd")
    plot!([[-1] [-1]],color=[:blue :red],l2=[4 4],label=["Degree of $(b_name_1)" "Degree of $(b_name_2)"])
    return plot!([[-1] [-1] [-1]],title=title,color=[:blue :red :purple],fillrange=[[-1] [-1] [-1]],fillalpha=0.5,la=0.5,label=["Area $(b_name_1)" "Area $(b_name_2)" "Area Combined"])
end


### Bifurcation Diagram Plotting ###

# Loads bifurcation diagram(s).
load_bifurcation_diagrams(p_idx) = deserialize("Data/Bifurcation_diagrams/bifs_$(igoshin_model.parameter_syms[p_idx])_$(igoshin_model.parameters[p_idx]/10.0)_$(igoshin_model.parameters[p_idx]*10.0).jls") 
load_bifurcation_diagram(p_idx,p_span)= deserialize("Data/Bifurcation_diagrams/bif_$(igoshin_model.parameter_syms[p_idx])_$(p_span[1])_$(p_span[2]).jls") 

# Plots a bifurcation diagram.
plot_bifurcation_diagram(args...;kwargs...) = (plot(); plot_bifurcation_diagram!(args...;kwargs...);)
function plot_bifurcation_diagram!(bif;color_stable=:blue,color_unstable=:red,ls_stable=:solid,ls_unstable=:solid,lw=4,lw_stable=lw,lw_unstable=lw,xguide="$(bif.sym)",yguide="σᴮ concentration",markercolor=:lightgreen,markersize=10,markershape=:star4,kwargs...)
    bps = [1,(findall(xor.(bif.stabs[1:end-1],bif.stabs[2:end])).+1)...,length(bif.stabs)]
    for i = 1:length(bps)-1
        plot!(bif.ps[bps[i]:bps[i+1]],bif.xs[bps[i]:bps[i+1]], xaxis=:log,color=(bif.stabs[bps[i]] ? color_unstable : color_stable),linestyle=(bif.stabs[bps[i]] ? ls_unstable : ls_stable),lw=(bif.stabs[bps[i]] ? lw_unstable : lw_stable),label="")
    end
    ymax = (maximum(bif.xs) > 2*bif.eq0[2]) ? Inf : 2*bif.eq0[2]
    scatter!(bif.eq0,color=markercolor,markersize=markersize,markershape=markershape,label="",xlimit=bif.p_span,xguide=xguide,yguide=yguide,ylimit=(0.,ymax),xticks=[])
end

# Plots several bifurcation diagrams.
function plot_bifurcation_diagrams(bifs;colors_stable=fill(:blue,length(bifs)),colors_unstable=fill(:red,length(bifs)))
    plot()
    foreach(i -> plot_bifurcation_diagram!(bifs[i],color_stable=colors_stable[i],color_unstable=colors_unstable[i]), 1:length(bifs))
    plot!()
end


### Simulation Plots ###

# Plots system simulations (SigB activity).
plot_simulations(args...;kwargs...) = (plot(); plot_simulations!(args...;kwargs...);)
function plot_simulations!(sims;idxs=1:length(sims),la=0.7,lw=2,yguide="σᴮ concentration",xguide="Time",label_stress="",label="",kwargs...)
    plot!(sims,vars=[7],la=la,lw=lw,idxs=idxs,xguide=xguide,yguide=yguide,label=label)
    plot!([0.,0.],[-0.05,1.05*maximum(vcat(map(sim -> getindex.(sim.u,7), sims)...))];lw=3,color=:red,la=0.8,linestyle=:dash,label=label_stress,kwargs...)
end

# Plots information of the phosphatase activity in a simulation.
plot_simulations_phos(args...;kwargs...) = (plot(); plot_simulations_phos!(args...;kwargs...);)
function plot_simulations_phos!(sims;labels=["phos (active)" "vP-phos" "phos (free)"],lws=[4 2 2], las = [0.5 0.7 0.7],include=[true false false],xticks=[],yticks=[],kwargs...)
    include[1] && plot!(sims[1].t,getindex.(sims[1].u,9)+getindex.(sims[1].u,10),la=las[1],lw=lws[1],label=labels[1])
    include[2] && plot!(sims,vars=9,idxs=1:1,la=las[2],lw=lws[2],label=labels[2])
    include[3] && plot!(sims,vars=10,idxs=1:1,la=las[3],lw=lws[3],label=labels[3])
    max_val = 1.1*maximum(vcat(map(sim -> getindex.(sim.u,9)+getindex.(sim.u,10),sims)...))
    plot!([0.,0.],[-0.05,max_val];lw=3,color=:red,la=0.8,ylimit=(-0.05,max_val),linestyle=:dash,xlimit=(sims[1].t[1],sims[1].t[end]),label="",legend=:bottomright,xticks=xticks,yticks=yticks,yguide="Concentration",xguide="Time",left_margin=5mm,kwargs...)
end

# Prepares plots of a ssa simulation.
function prepare_ssa_plots(l,n,p_size)
    sols = ssa_simulate_model(igoshin_model,l,1;stress_step=(10,10.0,p_size))[1]
    sols_several = ssa_simulate_model(igoshin_model,l,n;stress_step=(10,10.0,p_size),save_positions=(false,false),saveat=.25)
    max_val = maximum(vcat(getindex.(sols.u,7),map(i -> getindex.(sols_several[i].u,7), 1:n)...))
    
    plot(sols,vars=[7],xguide="Time",yguide="σᴮ copy number",title="Phosphatase molecules = $(p_size)",label="",xlimit=(-10.0,50.0),ylimit=(-0.25,1.1*max_val))
    plot_single = plot!([0.,0.],[-0.25,1.1*max_val], lw=2, linestyle=:dash, color=:red, label="")
    
    plot(sols_several,vars=[7],xguide="Time",yguide="σᴮ copy number",title="",label="",xlimit=(-10.0,50.0),ylimit=(-0.25,1.1*max_val),la=0.8)
    plot_several = plot!([0.,0.],[-0.25,1.1*max_val], lw=2, linestyle=:dash, color=:red, label="")
    
    return((plot_single,plot_several))
end;