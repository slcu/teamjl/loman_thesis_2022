### Fetch Packages ###
using Catalyst

### Locke Model ###

# Declares the network.
locke_system = @reaction_network begin
    k_d, (A, Ap, K) ⟶ ∅
    t_a*A+t_i, ∅ ⟶ (A, K)
    (b_p*K)/(k_p+A), A → Ap
    (b_dp*P)/(k_dp+Ap), Ap → A
end t_i t_a k_dp k_p k_d b_p b_dp P

# Declares the paraemters.
t_i = 0.005; t_a = 0.025;
k_dp = 0.1; k_p = 0.1;
b_p = 0.065; b_dp = 0.125;
k_d = 0.005; 
P = 0.9;
η = 0.1;
locke_parameters = [t_i, t_a, k_dp, k_p, k_d, b_p, b_dp, P]

# Compiles everything into a model.
locke_model = Model(locke_system, locke_parameters);


### Declare Expanded Model Combined Information Structure ###
mutable struct ModelExpanded
    model::Model
    u0_func::Function
    stress_cb::Function
    sigB_idx::Int64
end
get_u0_base(me::ModelExpanded) = me.u0_func(me.model.p_vals)


### Base Narula Model ###

# Declares the network.
narula_system = @reaction_network begin
    kDeg,       (w,w2,w2v,v,w2v2,vP,σB,w2σB) ⟶ ∅
    kDeg,       vPp ⟶ phos
    (kBw,kDw),  2w ⟷ w2
    (kB1,kD1),  w2 + v ⟷ w2v
    (kB2,kD2),  w2v + v ⟷ w2v2
    kK1,        w2v ⟶ w2 + vP
    kK2,        w2v2 ⟶ w2v + vP
    (kB3,kD3),  w2 + σB ⟷ w2σB
    (kB4,kD4),  w2σB + v ⟷ w2v + σB
    (kB5,kD5),  vP + phos ⟷ vPp
    kP,         vPp ⟶ v + phos
    v0*((1+F*σB)/(K+σB)),     ∅ ⟶ σB
    λW*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ w
    λV*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ v
end kBw kDw kD kB1 kB2 kB3 kB4 kB5 kD1 kD2 kD3 kD4 kD5 kK1 kK2 kP kDeg v0 F K λW λV pInit pStress η;

# Declares the paraemters.
kBw = 3600; kDw = 18; kD = 18;
kB1 = 3600; kB2 = 3600; kB3 = 3600; kB4 = 1800; kB5 = 3600;
kD1 = 18; kD2 = 18; kD3 = 18; kD4 = 1800; kD5 = 18;
kK1 = 36; kK2 = 36;
kP = 180; kDeg = 0.7;
v0 = 0.4; F = 30; K = 0.2;
λW = 4; λV = 4.5;

η_val = 0.05
pInit = 0.001
pStress = 0.4
narula_parameters = [kBw, kDw, kD, kB1, kB2, kB3, kB4, kB5, kD1, kD2, kD3, kD4, kD5, kK1, kK2, kP, kDeg, v0, F, K, λW, λV, pInit, pStress, η_val]

# Declares auxiliary model structures and information.
narula_u0_func(params) = [1.,1.,1.,1.,1.,1.,1.,1.,0.,params[23]]
narula_noises = fill((@parameters η)[1],length(narula_system.eqs))
narula_stress_cb(st) = DiscreteCallback((u,t,integrator)->t==st,integrator->(integrator.u[10]+=(integrator.p[24]-integrator.p[23])),save_positions=(false,false))

# Compiles everything into a model.
narula_model = ModelExpanded(Model(narula_system, narula_parameters; noise_scaling=narula_noises), narula_u0_func, narula_stress_cb, 7)


### Transformed Phosphatase Parameters Narula Model ###

# Declares the network.
narula_new_params_system = @reaction_network begin
    kDeg,       (w,w2,w2v,v,w2v2,vP,σB,w2σB) ⟶ ∅
    kDeg,       vPp ⟶ phos
    (kBw,kDw),  2w ⟷ w2
    (kB1,kD1),  w2 + v ⟷ w2v
    (kB2,kD2),  w2v + v ⟷ w2v2
    kK1,        w2v ⟶ w2 + vP
    kK2,        w2v2 ⟶ w2v + vP
    (kB3,kD3),  w2 + σB ⟷ w2σB
    (kB4,kD4),  w2σB + v ⟷ w2v + σB
    (kB5,kD5),  vP + phos ⟷ vPp
    sqrt(pProd*pFrac),        vPp ⟶ v + phos
    v0*((1+F*σB)/(K+σB)),     ∅ ⟶ σB
    λW*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ w
    λV*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ v
end kBw kDw kD kB1 kB2 kB3 kB4 kB5 kD1 kD2 kD3 kD4 kD5 kK1 kK2 pProd kDeg v0 F K λW λV pInit pFrac η;

# Declares the paraemters.
kBw = 3600; kDw = 18; kD = 18;
kB1 = 3600; kB2 = 3600; kB3 = 3600; kB4 = 1800; kB5 = 3600;
kD1 = 18; kD2 = 18; kD3 = 18; kD4 = 1800; kD5 = 18;
kK1 = 36; kK2 = 12; #kK2 reduced to a third to achive arcitability. kK2 = 36 in the original model.
pProd = 60; kDeg = 0.7;
v0 = 0.4; F = 30; K = 0.2;
λW = 4; λV = 4.5;

η = 0.05
pInit = 0.001
pFrac = 100
narula_new_params_parameters = [kBw, kDw, kD, kB1, kB2, kB3, kB4, kB5, kD1, kD2, kD3, kD4, kD5, kK1, kK2, pProd, kDeg, v0, F, K, λW, λV, pInit, pFrac, η]

# Declares auxiliary model structures and information.
narula_new_params_u0_func(params) = [1.,1.,1.,1.,1.,1.,1.,1.,0.,params[23]]
narula_new_params_noises = fill((@parameters η)[1],length(narula_new_params_system.eqs))
narula_new_params_stress_cb(st) = DiscreteCallback((u,t,integrator)->t==st,integrator->(integrator.u[10]+=(sqrt(integrator.p[16]/integrator.p[24])-integrator.p[23])),save_positions=(false,false))

# Compiles everything into a model.
narula_new_params_model = ModelExpanded(Model(narula_new_params_system, narula_new_params_parameters; noise_scaling=narula_new_params_noises), narula_new_params_u0_func, narula_new_params_stress_cb, 7)


### Noise Modulation Model ###

noise_modulation_system = @reaction_network begin
    kDeg,       (w,w2,w2v,v,w2v2,vP,σB,w2σB) ⟶ ∅
    kDeg,       vPp ⟶ phos
    (kBw,kDw),  2w ⟷ w2
    (kB1,kD1),  w2 + v ⟷ w2v
    (kB2,kD2),  w2v + v ⟷ w2v2
    kK1,        w2v ⟶ w2 + vP
    kK2,        w2v2 ⟶ w2v + vP
    (kB3,kD3),  w2 + σB ⟷ w2σB
    (kB4,kD4),  w2σB + v ⟷ w2v + σB
    (kB5,kD5),  vP + phos ⟷ vPp
    sqrt(pProd*pFrac),        vPp ⟶ v + phos
    v0*((1+F*σB)/(K+σB)),     ∅ ⟶ σB
    λW*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ w
    λV*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ v
    
    (ηFreq,ηFreq), phosI ⟷ phos
    ηFreq, vPp ⟶ vP + phosI
end kBw kDw kD kB1 kB2 kB3 kB4 kB5 kD1 kD2 kD3 kD4 kD5 kK1 kK2 pProd kDeg v0 F K λW λV pInit pFrac ηFreq ηAmp ηCore;

# Declares the paraemters.
kBw = 3600; kDw = 18; kD = 18;
kB1 = 3600; kB2 = 3600; kB3 = 3600; kB4 = 1800; kB5 = 3600;
kD1 = 18; kD2 = 18; kD3 = 18; kD4 = 1800; kD5 = 18;
kK1 = 36; kK2 = 12;
pProd = 60; kDeg = 0.7;
v0 = 0.4; F = 30; K = 0.2;
λW = 4; λV = 4.5;

pInit = 0.001; pFrac = 100;
ηFreq = 1.0; ηAmp_val = 0.05
ηCore_val = 0.05;

noise_modulation_parameters = [kBw, kDw, kD, kB1, kB2, kB3, kB4, kB5, kD1, kD2, kD3, kD4, kD5, kK1, kK2, pProd, kDeg, v0, F, K, λW, λV, pInit, pFrac, ηFreq, ηAmp_val, ηCore_val]

# Declares auxiliary model structures and information.
noise_modulation_u0_func(params) = [1.,1.,1.,1.,1.,1.,1.,1.,0.,params[23],params[23]]
noise_modulation_noises = [fill((@parameters ηCore)[1],27)..., fill((@parameters ηAmp)[1],3)...]
noise_modulation_stress_cb(st) = DiscreteCallback((u,t,integrator)->t==st,integrator->(integrator.u[10]+=(sqrt(integrator.p[16]/integrator.p[24])-integrator.p[23]); integrator.u[11]+=(sqrt(integrator.p[16]/integrator.p[24])-integrator.p[23]);),save_positions=(false,false))

# Compiles everything into a model.
noise_modulation_model = ModelExpanded(Model(noise_modulation_system, noise_modulation_parameters; noise_scaling=noise_modulation_noises), noise_modulation_u0_func, noise_modulation_stress_cb, 7);


### Base Narula Steady State Calculation Model ###
narula_bif_system = @reaction_network begin
    kDeg,       (w,w2,w2v,v,w2v2,vP,σB,w2σB) ⟶ ∅
    kDeg,       vPp ⟶ phos
    (kBw,kDw),  2w ⟷ w2
    (kB1,kD1),  w2 + v ⟷ w2v
    (kB2,kD2),  w2v + v ⟷ w2v2
    kK1,        w2v ⟶ w2 + vP
    kK2,        w2v2 ⟶ w2v + vP
    (kB3,kD3),  w2 + σB ⟷ w2σB
    (kB4,kD4),  w2σB + v ⟷ w2v + σB
    (kB5,kD5),  vP + phos ⟷ vPp
    kP,         vPp ⟶ v + phos
    v0*((1+F*σB)/(K+σB)),     ∅ ⟶ σB
    λW*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ w
    λV*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ v
    
    (pStress,1), 0 <--> phos
    1,  vPp ⟶ vP 
end kBw kDw kD kB1 kB2 kB3 kB4 kB5 kD1 kD2 kD3 kD4 kD5 kK1 kK2 kP kDeg v0 F K λW λV pInit pStress η;
narula_bif_model = Model(narula_bif_system,narula_parameters);

### Parameter transformed Narula Steady State Calculation Model ###
narula_new_params_bif_system = @reaction_network begin
    kDeg,       (w,w2,w2v,v,w2v2,vP,σB,w2σB) ⟶ ∅
    kDeg,       vPp ⟶ phos
    (kBw,kDw),  2w ⟷ w2
    (kB1,kD1),  w2 + v ⟷ w2v
    (kB2,kD2),  w2v + v ⟷ w2v2
    kK1,        w2v ⟶ w2 + vP
    kK2,        w2v2 ⟶ w2v + vP
    (kB3,kD3),  w2 + σB ⟷ w2σB
    (kB4,kD4),  w2σB + v ⟷ w2v + σB
    (kB5,kD5),  vP + phos ⟷ vPp
    sqrt(pProd*pFrac),        vPp ⟶ v + phos
    v0*((1+F*σB)/(K+σB)),     ∅ ⟶ σB
    λW*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ w
    λV*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ v
    
    (sqrt(pProd/pFrac),1), 0 <--> phos
    1,  vPp ⟶ vP 
end kBw kDw kD kB1 kB2 kB3 kB4 kB5 kD1 kD2 kD3 kD4 kD5 kK1 kK2 pProd kDeg v0 F K λW λV pInit pFrac η;
narula_new_params_bif_model = Model(narula_new_params_bif_system,narula_new_params_parameters);