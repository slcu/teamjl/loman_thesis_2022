### Fetch Packages ###
using DifferentialEquations

# System Activation Simulations ###

# Single activation using the RRE interpretation
function detsim_activation(model::ModelExpanded, pre_stress_t::Float64, post_stress_t::Float64; p_changes=[], kwargs...)
    foreach(pc -> model.model[pc[1]] = pc[2], p_changes)
    sol = detsim(model.model, (-pre_stress_t,post_stress_t); u0=get_u0_rre(model.model;u0_base=get_u0_base(model),tleng=post_stress_t), p_steps=(), v_steps=(model.model.v_syms[1],(0.0,0.0)), callbacks=(model.stress_cb(0.0),), kwargs...)
    !isempty(p_changes) && reset!(model.model)
    return sol
end
# Single activation using the CLE interpretation
function stochsim_activation(model::ModelExpanded, pre_stress_t::Float64, post_stress_t::Float64; p_changes=[], saveat=0.1, maxiters=1e6, adaptive=true, dt=0.00001, kwargs...)
    foreach(pc -> model.model[pc[1]] = pc[2], p_changes)
    sol = stochsim(model.model, (-pre_stress_t,post_stress_t); u0=get_u0s_cle(model.model;u0_base=get_u0_base(model),tleng=pre_stress_t,dt=dt,adaptive=adaptive)[1], p_steps=(), v_steps=(model.model.v_syms[1],(0.0,0.0)), callbacks=(model.stress_cb(0.0),), dt=dt, adaptive=adaptive, saveat=saveat, kwargs...)
    !isempty(p_changes) && reset!(model.model)
    return sol
end
# Monte carlo activation using the CLE interpretation
function monte_activation(model::ModelExpanded, pre_stress_t::Float64, post_stress_t::Float64, n::Int64; p_changes=[], saveat=0.1, maxiters=1e6, adaptive=true, dt=0.00001, kwargs...)
    foreach(pc -> model.model[pc[1]] = pc[2], p_changes)
    sols = monte(model.model, (-pre_stress_t,post_stress_t), n; u0=nothing,u0s=get_u0s_cle(model.model,n;u0_base=get_u0_base(model),tleng=pre_stress_t,dt=dt,adaptive=adaptive), p_steps=(), v_steps=(model.model.v_syms[1],(0.0,0.0)), callbacks=(model.stress_cb(0.0),), dt=dt, adaptive=adaptive, saveat=saveat, kwargs...)
    !isempty(p_changes) && reset!(model.model)
    return sols
end

# Single activation using the Gillespie interpretation
function ssasim_activation(model::ModelExpanded, pre_stress_t::Float64, post_stress_t::Float64, u0_base::Vector{Int64}, step_var::Symbol,step_val::Int64; p_changes=[], saveat=0.1, kwargs...)
    reset!(model.model); foreach(pc -> model.model[pc[1]] = pc[2], p_changes)
    sol = ssasim(model.model, (-pre_stress_t,post_stress_t); u0=get_u0s_gillespie(model.model;tleng=post_stress_t,u0_base=u0_base)[1], p_steps=(), v_steps=(step_var,(0.0,step_val)), saveat=saveat, kwargs...)
    !isempty(p_changes) && reset!(model.model)
    return sol
end
# Monte carlo activation using the Gillespie interpretation
function ssamonte_activation(model::ModelExpanded, pre_stress_t::Float64, post_stress_t::Float64, u0_base::Vector{Int64}, step_var::Symbol,step_val::Int64, n::Int64; p_changes=[], saveat=0.1, kwargs...)
    reset!(model.model); foreach(pc -> model.model[pc[1]] = pc[2], p_changes)
    sols = ssamonte(model.model, (-pre_stress_t,post_stress_t), n; u0=nothing,u0s=get_u0s_gillespie(model.model,n;u0_base=u0_base,tleng=pre_stress_t), p_steps=(), v_steps=(step_var,(0.0,step_val)), saveat=saveat, kwargs...)
    !isempty(p_changes) && reset!(model.model)
    return sols
end;














### Old Stuff - Delete Eventually ###


### Simulation Functions ###

# Monte Carlo simulation of the model.
function simulate_model(modelE::ModelExpanded,t_stress,l,n;p_changes=[],dt=0.00001,saveat=0.1,t_ss=100.0)
    p = modified_parameters(modelE,p_changes)
    u0s = solve(SDEProblem(modelE.model.system,modelE.u0_func(p),(0.,t_ss),p,noise_scaling=modelE.model.noise_scaling),ImplicitEM(),callback=positive_domain_cb,adaptive=false,saveat=1.,dt=dt).u[floor(Int64,t_ss/2):end]
    sprob = SDEProblem(modelE.model.system,u0s[end],(-t_stress,l),p,noise_scaling=modelE.model.noise_scaling)
    eprob = EnsembleProblem(sprob,prob_func=(p,i,r)->remake(p,u0=rand(u0s)))
    return solve(eprob,ImplicitEM(),trajectories=n,callback=CallbackSet(positive_domain_cb,modelE.stress_cb(0.)),adaptive=false,saveat=saveat,dt=dt,tstops=[0.])
end

# Monte Carlo ssa simulation of the model (igoshin).
function ssa_simulate_model(modelE::ModelExpanded,l,n;u0=zeros(length(modelE.model.system.states)),stress_step=(10,0.0,0),saveat=0.1,save_positions=(true,true),p_changes=[],t_ss=50.0)
    p = modified_parameters(modelE,p_changes)[1:end-1]
    JumpProblem(modelE.model.system,DiscreteProblem(modelE.model.system,u0,(0.,t_ss),p),Direct())
    u0s = solve(JumpProblem(modelE.model.system,DiscreteProblem(modelE.model.system,u0,(0.,t_ss),p),Direct()),SSAStepper()).u[floor(Int64,t_ss/2):end]
    
    dprob = DiscreteProblem(modelE.model.system,u0s[end],(-stress_step[2],l),p)
    jprob = JumpProblem(modelE.model.system,dprob,Direct(),save_positions=save_positions)
    eprob = EnsembleProblem(jprob,prob_func=(p,i,r)->remake(p,u0=rand(u0s)))
    cb = DiscreteCallback((u,t,integrator)->t==0.0,integrator->integrator.u[stress_step[1]]+=stress_step[3],save_positions=(false,false))
    return solve(eprob,SSAStepper(),trajectories=n,callback=cb,tstops=[0.0],saveat=saveat)
end


### Callbacks ###

# Positive domain callback.
positive_domain_cb = DiscreteCallback((u,t,integrator) -> minimum(u) < 0,integrator -> integrator.u .= integrator.uprev,save_positions = (false,false))


### Utility functions ###

# Generates a modified parameter set.
function modified_parameters(modelE::ModelExpanded,p_changes)
    p = deepcopy(modelE.model.p_vals)
    foreach(pc -> p[get_p_idx(modelE,pc[1])] = pc[2], p_changes)
    return p
end

# Finds the index of a parameter in a model.
get_p_idx(modelE::ModelExpanded,par::Symbol) = findfirst(map(i -> Symbol(modelE.model.p_syms[i]) == par, 1:length(modelE.model.p_syms)))
