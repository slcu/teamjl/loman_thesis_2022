### Fetch Packages ###
using Catalyst

### Declare Model Combined Information Structure ###
struct Model
    system::ReactionSystem
    parameters::Vector{Float64}
    parameter_syms::Vector{Symbol}
    u0_func::Function
    noises::Vector{Num}
    stress_cb::Function
    sigB_idx::Int64
    phos_idxs::Vector{Int64}
end

### Base Igoshin Model ###

# Declares the network.
igoshin_system = @reaction_network begin
    kDeg,       (w,w2,w2v,v,w2v2,vP,σB,w2σB) ⟶ ∅
    kDeg,       vPp ⟶ phos
    (kBw,kDw),  2w ⟷ w2
    (kB1,kD1),  w2 + v ⟷ w2v
    (kB2,kD2),  w2v + v ⟷ w2v2
    kK1,        w2v ⟶ w2 + vP
    kK2,        w2v2 ⟶ w2v + vP
    (kB3,kD3),  w2 + σB ⟷ w2σB
    (kB4,kD4),  w2σB + v ⟷ w2v + σB
    (kB5,kD5),  vP + phos ⟷ vPp
    kP,         vPp ⟶ v + phos
    v0*((1+F*σB)/(K+σB)),     ∅ ⟶ σB
    λW*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ w
    λV*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ v
end kBw kDw kD kB1 kB2 kB3 kB4 kB5 kD1 kD2 kD3 kD4 kD5 kK1 kK2 kP kDeg v0 F K λW λV pInit pStress;

# Declares the paraemters.
kBw = 3600; kDw = 18; kD = 18;
kB1 = 3600; kB2 = 3600; kB3 = 3600; kB4 = 1800; kB5 = 3600;
kD1 = 18; kD2 = 18; kD3 = 18; kD4 = 1800; kD5 = 18;
kK1 = 36; kK2 = 12; #kK2 reduced to a third to achive arcitability. kK2 = 36 in the original model.
kP = 180; kDeg = 0.7;
v0 = 0.4; F = 30; K = 0.2;
λW = 4; λV = 4.5;

η = 0.05
pInit = 0.001
pStress = 0.4
igoshin_parameters = [kBw, kDw, kD, kB1, kB2, kB3, kB4, kB5, kD1, kD2, kD3, kD4, kD5, kK1, kK2, kP, kDeg, v0, F, K, λW, λV, pInit, pStress, η]

# Declares auxiliary model structures and information.
igoshin_u0_func(params) = [1.,1.,1.,1.,1.,1.,1.,1.,0.,params[23]]
igoshin_parameter_syms = [:kBw, :kDw, :kD, :kB1, :kB2, :kB3, :kB4, :kB5, :kD1, :kD2, :kD3, :kD4, :kD5, :kK1, :kK2, :kP, :kDeg, :v0, :F, :K, :λW, :λV, :pInit, :pStress, :η]
igoshin_noises = fill((@parameters H)[1],length(igoshin_system.eqs))
igoshin_stress_cb(st) = DiscreteCallback((u,t,integrator)->t==st,integrator->(integrator.u[10]+=(integrator.p[24]-integrator.p[23])),save_positions=(false,false))
igoshin_phos_idxs = [9,10]

# Compiles everything into a model.
igoshin_model = Model(igoshin_system, igoshin_parameters, igoshin_parameter_syms, igoshin_u0_func, igoshin_noises, igoshin_stress_cb, 7, igoshin_phos_idxs)


### Transformed Phosphatase Parameters Model ###

# Declares the network.
igoshin_new_params_system = @reaction_network begin
    kDeg,       (w,w2,w2v,v,w2v2,vP,σB,w2σB) ⟶ ∅
    kDeg,       vPp ⟶ phos
    (kBw,kDw),  2w ⟷ w2
    (kB1,kD1),  w2 + v ⟷ w2v
    (kB2,kD2),  w2v + v ⟷ w2v2
    kK1,        w2v ⟶ w2 + vP
    kK2,        w2v2 ⟶ w2v + vP
    (kB3,kD3),  w2 + σB ⟷ w2σB
    (kB4,kD4),  w2σB + v ⟷ w2v + σB
    (kB5,kD5),  vP + phos ⟷ vPp
    sqrt(pProd*pFrac),        vPp ⟶ v + phos
    v0*((1+F*σB)/(K+σB)),     ∅ ⟶ σB
    λW*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ w
    λV*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ v
end kBw kDw kD kB1 kB2 kB3 kB4 kB5 kD1 kD2 kD3 kD4 kD5 kK1 kK2 pProd kDeg v0 F K λW λV pInit pFrac;

# Declares the paraemters.
kBw = 3600; kDw = 18; kD = 18;
kB1 = 3600; kB2 = 3600; kB3 = 3600; kB4 = 1800; kB5 = 3600;
kD1 = 18; kD2 = 18; kD3 = 18; kD4 = 1800; kD5 = 18;
kK1 = 36; kK2 = 12; #kK2 reduced to a third to achive arcitability. kK2 = 36 in the original model.
pProd = 60; kDeg = 0.7;
v0 = 0.4; F = 30; K = 0.2;
λW = 4; λV = 4.5;

η = 0.05
pInit = 0.001
pFrac = 100
igoshin_new_params_parameters = [kBw, kDw, kD, kB1, kB2, kB3, kB4, kB5, kD1, kD2, kD3, kD4, kD5, kK1, kK2, pProd, kDeg, v0, F, K, λW, λV, pInit, pFrac, η]

# Declares auxiliary model structures and information.
igoshin_new_params_u0_func(params) = [1.,1.,1.,1.,1.,1.,1.,1.,0.,params[23]]
igoshin_new_params_parameter_syms = [:kBw, :kDw, :kD, :kB1, :kB2, :kB3, :kB4, :kB5, :kD1, :kD2, :kD3, :kD4, :kD5, :kK1, :kK2, :pProd, :kDeg, :v0, :F, :K, :λW, :λV, :pInit, :pFrac, :η]
igoshin_new_params_noises = fill((@parameters H)[1],length(igoshin_new_params_system.eqs))
igoshin_new_params_stress_cb(st) = DiscreteCallback((u,t,integrator)->t==st,integrator->(integrator.u[10]+=(sqrt(integrator.p[16]/integrator.p[24])-integrator.p[23])),save_positions=(false,false))
igoshin_new_params_phos_idxs = [9,10]

# Compiles everything into a model.
igoshin_new_params_model = Model(igoshin_new_params_system, igoshin_new_params_parameters, igoshin_new_params_parameter_syms, igoshin_new_params_u0_func, igoshin_new_params_noises, igoshin_new_params_stress_cb, 7, igoshin_new_params_phos_idxs)


### Old Noise Modulation Model ###

old_noise_modulation_system = @reaction_network begin
    kDeg,       (w,w2,w2v,v,w2v2,vP,σB,w2σB) ⟶ ∅
    kDeg,       vPp ⟶ phos
    (kBw,kDw),  2w ⟷ w2
    (kB1,kD1),  w2 + v ⟷ w2v
    (kB2,kD2),  w2v + v ⟷ w2v2
    kK1,        w2v ⟶ w2 + vP
    kK2,        w2v2 ⟶ w2v + vP
    (kB3,kD3),  w2 + σB ⟷ w2σB
    (kB4,kD4),  w2σB + v ⟷ w2v + σB
    (kB5,kD5),  vP + phos ⟷ vPp
    sqrt(pProd*pFrac),        vPp ⟶ v + phos
    v0*((1+F*σB)/(K+σB)),     ∅ ⟶ σB
    λW*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ w
    λV*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ v
    
    (S*ηFreq*kAct,ηFreq*kIn), phosI ⟷ phos
    ηFreq*kIn, vPp ⟶ vP + phosI
end kBw kDw kD kB1 kB2 kB3 kB4 kB5 kD1 kD2 kD3 kD4 kD5 kK1 kK2 pProd kDeg v0 F K λW λV pFrac kAct kIn ηFreq S;

# Declares the paraemters.
kBw = 3600; kDw = 18; kD = 18
kB1 = 3600; kB2 = 3600; kB3 = 3600; kB4 = 1800; kB5 = 3600;
kD1 = 18; kD2 = 18; kD3 = 18; kD4 = 1800; kD5 = 18;
kK1 = 36; kK2 = 12; #kK2 reduced to a third to achive arcitability. kK2 = 36 in the original model.
pProd = 60; kDeg = 0.7;
v0 = 0.4; F = 30; K = 0.2;
λW = 4; λV = 4.5;

ηCore = 0.05; ηAmp = 0.05
pFrac = 100
S = 1.0; kIn = 100; kAct = 1; ηFreq = 1.0;

old_noise_modulation_parameters = [kBw, kDw, kD, kB1, kB2, kB3, kB4, kB5, kD1, kD2, kD3, kD4, kD5, kK1, kK2, pProd, kDeg, v0, F, K, λW, λV, pFrac, kAct, kIn, ηFreq, S, ηCore, ηAmp]

# Declares auxiliary model structures and information.
old_noise_modulation_u0_func(params) = [1.,1.,1.,1.,1.,1.,1.,1.,0.,0.,2*sqrt(params[16]/params[23])]
old_noise_modulation_parameter_syms = [:kBw, :kDw, :kD, :kB1, :kB2, :kB3, :kB4, :kB5, :kD1, :kD2, :kD3, :kD4, :kD5, :kK1, :kK2, :pProd, :kDeg, :v0, :F, :K, :λW, :λV, :pFrac, :kAct, :kIn, :ηFreq, :S, :ηCore, :ηAmp]
old_noise_modulation_noises = [fill((@parameters H1)[1],27)..., fill((@parameters H2)[1],3)...]
old_noise_modulation_stress_cb(st) = DiscreteCallback((u,t,integrator)->t==st,integrator->integrator.p[27]=integrator.p[25]/integrator.p[24],save_positions=(false,false))
old_noise_modulation_phos_idxs = [9,10,11]

# Compiles everything into a model.
old_noise_modulation_model = Model(old_noise_modulation_system, old_noise_modulation_parameters, old_noise_modulation_parameter_syms, old_noise_modulation_u0_func, old_noise_modulation_noises, old_noise_modulation_stress_cb, 7, old_noise_modulation_phos_idxs)

### Old Noise Modulation Model ###

noise_modulation_system = @reaction_network begin
    kDeg,       (w,w2,w2v,v,w2v2,vP,σB,w2σB) ⟶ ∅
    kDeg,       vPp ⟶ phos
    (kBw,kDw),  2w ⟷ w2
    (kB1,kD1),  w2 + v ⟷ w2v
    (kB2,kD2),  w2v + v ⟷ w2v2
    kK1,        w2v ⟶ w2 + vP
    kK2,        w2v2 ⟶ w2v + vP
    (kB3,kD3),  w2 + σB ⟷ w2σB
    (kB4,kD4),  w2σB + v ⟷ w2v + σB
    (kB5,kD5),  vP + phos ⟷ vPp
    sqrt(pProd*pFrac),        vPp ⟶ v + phos
    v0*((1+F*σB)/(K+σB)),     ∅ ⟶ σB
    λW*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ w
    λV*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ v
    
    (ηFreq,ηFreq), phosI ⟷ phos
    ηFreq, vPp ⟶ vP + phosI
end kBw kDw kD kB1 kB2 kB3 kB4 kB5 kD1 kD2 kD3 kD4 kD5 kK1 kK2 pProd kDeg v0 F K λW λV pInit pFrac ηFreq ηAmp ηCore;

# Declares the paraemters.
kBw = 3600; kDw = 18; kD = 18;
kB1 = 3600; kB2 = 3600; kB3 = 3600; kB4 = 1800; kB5 = 3600;
kD1 = 18; kD2 = 18; kD3 = 18; kD4 = 1800; kD5 = 18;
kK1 = 36; kK2 = 12;
pProd = 60; kDeg = 0.7;
v0 = 0.4; F = 30; K = 0.2;
λW = 4; λV = 4.5;

pInit = 0.001; pFrac = 100;
ηFreq = 1.0; ηAmp_val = 0.05
ηCore_val = 0.05;

noise_modulation_parameters = [kBw, kDw, kD, kB1, kB2, kB3, kB4, kB5, kD1, kD2, kD3, kD4, kD5, kK1, kK2, pProd, kDeg, v0, F, K, λW, λV, pInit, pFrac, ηFreq, ηAmp_val, ηCore_val]

# Declares auxiliary model structures and information.
noise_modulation_u0_func(params) = [1.,1.,1.,1.,1.,1.,1.,1.,0.,params[23],params[23]]
noise_modulation_parameter_syms = [:kBw, :kDw, :kD, :kB1, :kB2, :kB3, :kB4, :kB5, :kD1, :kD2, :kD3, :kD4, :kD5, :kK1, :kK2, :pProd, :kDeg, :v0, :F, :K, :λW, :λV, :pInit, :pFrac, :ηFreq, :ηAmp, :ηCore]
noise_modulation_noises = [fill((@parameters ηCore)[1],27)..., fill((@parameters ηAmp)[1],3)...]
noise_modulation_stress_cb(st) = DiscreteCallback((u,t,integrator)->t==st,integrator->(integrator.u[10]+=(sqrt(integrator.p[16]/integrator.p[24])-integrator.p[23]); integrator.u[11]+=(sqrt(integrator.p[16]/integrator.p[24])-integrator.p[23]);),save_positions=(false,false))
noise_modulation_phos_idxs = [9,10,11]

# Compiles everything into a model.
noise_modulation_model = Model(noise_modulation_system, noise_modulation_parameters, noise_modulation_parameter_syms, noise_modulation_u0_func, noise_modulation_noises, noise_modulation_stress_cb, 7, noise_modulation_phos_idxs)

