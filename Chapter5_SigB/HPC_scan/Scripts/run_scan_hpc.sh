#!/bin/bash

#SBATCH -A LOCKE-SL2-CPU
#SBATCH -J igoshin_model_parameter_scan
#SBATCH -D /home/tel30/rds/hpc-work/projects/scan_igoshin_parameters_final/Scripts
#SBATCH -o ../Logs/rescans/parameter_scan_%a.log
#SBATCH -p cclake  ### or cclake-himem
#SBATCH -c 1                   # max 32 CPUs
#SBATCH --mem-per-cpu=5980MB   # max 5980MB or 12030MB for skilake-himem
#SBATCH -t 36:00:00            # HH:MM:SS with maximum 36:00:00 for SL3 or 36:00:00 for SL2
#SBATCH -a 1-500

module load julia/1.6.2
julia scan_parameters.jl $SLURM_ARRAY_TASK_ID 500 core_parameters_scan
