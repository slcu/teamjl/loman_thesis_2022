### Fetch Required Packages ###
using Catalyst


### List of models for all number of intermediaries ###

model1 = @reaction_network begin
    (v0 + (S*σ)^n / ((S*σ)^n + (D*A1)^n + 1),1.), ∅ ↔ σ
    (σ/τ,1/τ), ∅ ↔ A1
end S D τ v0 n η

model2 = @reaction_network begin
    (v0 + (S*σ)^n / ((S*σ)^n + (D*A2)^n + 1),1.), ∅ ↔ σ
    (σ/τ,1/τ), ∅ ↔ A1
    (A1/τ,1/τ), ∅ ↔ A2
end S D τ v0 n η

model3 = @reaction_network begin
    (v0 + (S*σ)^n / ((S*σ)^n + (D*A3)^n + 1),1.), ∅ ↔ σ
    (σ/τ,1/τ), ∅ ↔ A1
    (A1/τ,1/τ), ∅ ↔ A2
    (A2/τ,1/τ), ∅ ↔ A3
end S D τ v0 n η

model4 = @reaction_network begin
    (v0 + (S*σ)^n / ((S*σ)^n + (D*A4)^n + 1),1.), ∅ ↔ σ
    (σ/τ,1/τ), ∅ ↔ A1
    (A1/τ,1/τ), ∅ ↔ A2
    (A2/τ,1/τ), ∅ ↔ A3
    (A3/τ,1/τ), ∅ ↔ A4
end S D τ v0 n η

model5 = @reaction_network begin
    (v0 + (S*σ)^n / ((S*σ)^n + (D*A5)^n + 1),1.), ∅ ↔ σ
    (σ/τ,1/τ), ∅ ↔ A1
    (A1/τ,1/τ), ∅ ↔ A2
    (A2/τ,1/τ), ∅ ↔ A3
    (A3/τ,1/τ), ∅ ↔ A4
    (A4/τ,1/τ), ∅ ↔ A5
end S D τ v0 n η

model6 = @reaction_network begin
    (v0 + (S*σ)^n / ((S*σ)^n + (D*A6)^n + 1),1.), ∅ ↔ σ
    (σ/τ,1/τ), ∅ ↔ A1
    (A1/τ,1/τ), ∅ ↔ A2
    (A2/τ,1/τ), ∅ ↔ A3
    (A3/τ,1/τ), ∅ ↔ A4
    (A4/τ,1/τ), ∅ ↔ A5
    (A5/τ,1/τ), ∅ ↔ A6
end S D τ v0 n η

model7 = @reaction_network begin
    (v0 + (S*σ)^n / ((S*σ)^n + (D*A7)^n + 1),1.), ∅ ↔ σ
    (σ/τ,1/τ), ∅ ↔ A1
    (A1/τ,1/τ), ∅ ↔ A2
    (A2/τ,1/τ), ∅ ↔ A3
    (A3/τ,1/τ), ∅ ↔ A4
    (A4/τ,1/τ), ∅ ↔ A5
    (A5/τ,1/τ), ∅ ↔ A6
    (A6/τ,1/τ), ∅ ↔ A7
end S D τ v0 n η

model8 = @reaction_network begin
    (v0 + (S*σ)^n / ((S*σ)^n + (D*A8)^n + 1),1.), ∅ ↔ σ
    (σ/τ,1/τ), ∅ ↔ A1
    (A1/τ,1/τ), ∅ ↔ A2
    (A2/τ,1/τ), ∅ ↔ A3
    (A3/τ,1/τ), ∅ ↔ A4
    (A4/τ,1/τ), ∅ ↔ A5
    (A5/τ,1/τ), ∅ ↔ A6
    (A6/τ,1/τ), ∅ ↔ A7
    (A7/τ,1/τ), ∅ ↔ A8
end S D τ v0 n η

model9 = @reaction_network begin
    (v0 + (S*σ)^n / ((S*σ)^n + (D*A9)^n + 1),1.), ∅ ↔ σ
    (σ/τ,1/τ), ∅ ↔ A1
    (A1/τ,1/τ), ∅ ↔ A2
    (A2/τ,1/τ), ∅ ↔ A3
    (A3/τ,1/τ), ∅ ↔ A4
    (A4/τ,1/τ), ∅ ↔ A5
    (A5/τ,1/τ), ∅ ↔ A6
    (A6/τ,1/τ), ∅ ↔ A7
    (A7/τ,1/τ), ∅ ↔ A8
    (A8/τ,1/τ), ∅ ↔ A9
end S D τ v0 n η

model10 = @reaction_network begin
    (v0 + (S*σ)^n / ((S*σ)^n + (D*A10)^n + 1),1.), ∅ ↔ σ
    (σ/τ,1/τ), ∅ ↔ A1
    (A1/τ,1/τ), ∅ ↔ A2
    (A2/τ,1/τ), ∅ ↔ A3
    (A3/τ,1/τ), ∅ ↔ A4
    (A4/τ,1/τ), ∅ ↔ A5
    (A5/τ,1/τ), ∅ ↔ A6
    (A6/τ,1/τ), ∅ ↔ A7
    (A7/τ,1/τ), ∅ ↔ A8
    (A8/τ,1/τ), ∅ ↔ A9
    (A9/τ,1/τ), ∅ ↔ A10
end S D τ v0 n η

# An array of all the models.
models = [model1,model2,model3,model4,model5,model6,model7,model8,model9,model10]


### A model for Gillespie Simulations ###
model_gillespie = @reaction_network begin
    (v0 + v*(I*S*σ)^n / ((I*S*σ)^n + (D*A3)^n + K^n),d), ∅ ↔ σ
    (σ/τ,1/τ), ∅ ↔ A1
    (A1/τ,1/τ), ∅ ↔ A2
    (A2/τ,1/τ), ∅ ↔ A3
end v0 v I S D K n d τ