# For a given behaviour, returns the appropriate color.
function get_color(behaviour_or_classification::Symbol)
    return color_codes[known_behaviour_list_short[behaviour_or_classification]]
    #return color_codes[behaviour_or_classification]
end

# List of possible behaviours, and their corresponding color.
color_codes = Dict([
    :no_activation =>                           RGB{Float64}(1,1,1),            # White
    :single_response_pulse =>                   RGB{Float64}(1.0, 0.77, 0.77),  # Pink
    :stochastic_pulsing =>                      RGB{Float64}(1,1,0),            # Yellow
    :oscillation =>                             RGB{Float64}(1,0,0),            # Red
    :stochastic_anti_pulsing =>                 RGB{Float64}(0.5,0,0.5),        # Purple
    :homogeneous_activation =>                  RGB{Float64}(0,0,0.5),          # Dark Blue
    :heterogeneous_activation =>                RGB{Float64}(0,0.75,1),         # Light Blue
    :stochastic_switching =>                    RGB{Float64}(0,1,0),            # Light Green
    :stable_bistability =>                      RGB{Float64}(0,0.25,0),         # Dark Green
    :intermediate_activation =>                 RGB{Float64}(0.25,0,0),         # Dark Red

    # Special cases.
    :tri_unstable =>                            RGB{Float64}(0.5,0.5,0.5),      # Grey
    :empty_behaviour =>                         RGB{Float64}(1,1,1),            # White
    :unknown_behaviour =>                       RGB{Float64}(0.5,0.5,0.5),      # Grey
    :convergence_failure =>                     RGB{Float64}(0.5,0.5,0.5),      # Grey
    :max_iters =>                               RGB{Float64}(0,1,0.75),         # Weird Green
    :unclassified_behaviour =>                  RGB{Float64}(0,0,0)])           # Black



# Hopefully we can delete this.
known_behaviour_list_short = Dict([ 
    :no_activation =>                                                       :no_activation,
    :single_response_pulse =>                                               :single_response_pulse,
    :stochastic_pulsing =>                                                  :stochastic_pulsing,
    :stochastic_pulsing_initial_pulse =>                                    :stochastic_pulsing,
    :stochastic_pulsing_no_initial_pulse =>                                 :stochastic_pulsing,
    :stochastic_pulsing_bistable_initial_pulse =>                           :stochastic_pulsing,
    :stochastic_pulsing_bistable_no_initial_pulse =>                        :stochastic_pulsing,
    :stochastic_pulsing_bistable_oscillation_initial_pulse =>               :stochastic_pulsing,
    :stochastic_pulsing_bistable_oscillation_no_initial_pulse =>            :stochastic_pulsing,
    :stochastic_pulsing_bistable_intermediate_activation_initial_pulse =>   :stochastic_pulsing,
    :stochastic_pulsing_bistable_intermediate_activation_no_initial_pulse =>:stochastic_pulsing,
    :regular_pulsing =>                                                     :oscillation,      
    :oscillation =>                                                         :oscillation,
    :oscillation_bistable =>                                                :oscillation,
    :regular_anti_pulsing =>                                                :oscillation,      
    :regular_anti_pulsing_no_initial_activation =>                          :oscillation,       
    :tri_unstable =>                                                        :oscillation,      
    :stochastic_anti_pulsing =>                                             :stochastic_anti_pulsing,
    :stochastic_anti_pulsing_no_initial_activation =>                       :stochastic_anti_pulsing,
    :stochastic_anti_pulsing_bistable =>                                    :stochastic_anti_pulsing,
    :stochastic_anti_pulsing_bistable_no_initial_activation =>              :stochastic_anti_pulsing,
    :stochastic_anti_pulsing_bistable_oscillation =>                        :stochastic_anti_pulsing,
    :stochastic_anti_pulsing_bistable_intermediate_activation =>            :stochastic_anti_pulsing,
    :homogeneous_activation =>                                              :homogeneous_activation,
    :heterogeneous_activation =>                                            :heterogeneous_activation,
    :heterogeneous_activation_monostable =>                                 :homogeneous_activation,    # Empiric, n=1.
    :stochastic_switching =>                                                :stochastic_switching,
    :stochastic_switching_instant_initial_activation =>                     :stochastic_switching,
    :stable_bistability =>                                                  :stable_bistability,
    :homogeneous_intermediate_activation =>                                 :intermediate_activation,
    :homogeneous_intermediate_activation_bistable =>                        :intermediate_activation,
    :heterogeneous_intermediate_activation =>                               :intermediate_activation,   # Very few cases exists (and also quite homogeneous). Or maybe not actually...
    :heterogeneous_intermediate_activation_bistable =>                      :intermediate_activation,   # Very few cases exists (and also quite homogeneous). Or maybe not actually...
    :tri_unstable =>                                                        :oscillation,               # Empiric, low count. a guess. Check.
    :unclassified_behaviour =>                                              :unclassified_behaviour,
    :no_default_behaviour =>                                                :unclassified_behaviour,
    :unknown_behaviour =>                                                   :unknown_behaviour,
    :max_iters =>                                                           :max_iters,
    :empty_behaviour =>                                                     :empty_behaviour,
    :convergence_failure =>                                                 :convergence_failure])