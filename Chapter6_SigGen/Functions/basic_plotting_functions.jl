### Fetch Required Packages ###
using Plots, Plots.Measures
using BifurcationKit
using LinearAlgebra, Setfield


### Empty Plots ###

# Creates an empty plot (for creating spaces in sub plots).
p0 = plot(framestyle=:none); 
p01 = plot(framestyle=:none); p02 = plot(framestyle=:none); p03 = plot(framestyle=:none); p04 = plot(framestyle=:none); p05 = plot(framestyle=:none); p06 = plot(framestyle=:none); p07 = plot(framestyle=:none); p08 = plot(framestyle=:none); p09 = plot(framestyle=:none);
p001 = plot(framestyle=:none); p002 = plot(framestyle=:none); p003 = plot(framestyle=:none); p004 = plot(framestyle=:none); p005 = plot(framestyle=:none); p006 = plot(framestyle=:none); p007 = plot(framestyle=:none); p008 = plot(framestyle=:none); p009 = plot(framestyle=:none);


### Basic Simulation Plots ###

# Plots the activation of a single trajectory.
plot_activation(args...;kwargs...) = (plot(); plot_activation!(args...;kwargs...);)
function plot_activation!(p_vals::Vector{Float64}, pre_stress_t::Float64, post_stress_t::Float64;model=model3,vars=[1,4],color_prestress=[:darkblue :darkorange3],color_poststress=[:darkblue :darkorange3],ymax=Inf,la=[0.6 0.7],lw=[4 3],activation_lw=7,activation_la=0.8,xticks=[],yticks=[],legendfontsize=11,plot_labels=false,xguide="Time",yguide="Concentration",
                            gillespie_interpretation=false,saveat=0.05,adaptive=false,dt=0.0025,maxiters=1e6,
                            sol = gillespie_interpretation ? ssasim_activation(p_vals,pre_stress_t,post_stress_t;saveat=saveat) : stochsim_activation(p_vals,pre_stress_t,post_stress_t;model=model,saveat=saveat,adaptive=adaptive,dt=dt,maxiters=maxiters),
                            kwargs...)
    if isequal(color_prestress,color_poststress)
        colors = color_poststress
    else
        colors = [[fill(color_prestress[i],Int64(pre_stress_t/saveat)+1)...,fill(color_poststress[i],length(sol.t)-Int64(pre_stress_t/saveat)-1)...] for ignore in 1:1, i in 1:length(vars)]
        gillespie_interpretation && (colors = [[fill(color_prestress[i],Int64(pre_stress_t/saveat)+1)...,fill(color_poststress[i],1000000-Int64(pre_stress_t/saveat)-1)...] for ignore in 1:1, i in 1:length(vars)]) # Probably a small bug in the plot recepie causes this to be required
    end
    plot!(sol;vars=vars,color=colors,la=la,lw=lw,xticks=xticks,yticks=yticks,xguide=xguide,yguide=yguide,label="",ylimit=(0.,ymax),kwargs...)
    if plot_labels
        for (i,var) in enumerate(vars)
            plot!([],[],label="$(model.states[var].val.f), before stress",color=color_prestress[i],lw=lw[i],la=la[i]); 
            plot!([],[],label="$(model.states[var].val.f), after stress",color=color_poststress[i],lw=lw[i],la=la[i]); 
        end
        plot!(legendfontsize=legendfontsize,legend=:topleft)
    end
    plot_vertical_line!(0,(ymax==Inf ? 1.05*maximum(first.(sol.u)) : max(ymax,1.05*maximum(first.(sol.u)))),lw=activation_lw,la=activation_la,left_margin=5mm,xlimit=(-pre_stress_t,post_stress_t))
end

# Plots the activations of a monte carlo assemble of trajectories.
plot_activations(args...;kwargs...) = (plot(); plot_activations!(args...;kwargs...);)
function plot_activations!(p_vals::Vector{Float64}, pre_stress_t::Float64, post_stress_t::Float64, n::Int64;model=model3,ymax=Inf,color=:darkblue,lw=4,la=0.6,colors=fill(color,1,n),lws=fill(lw,1,n),las=fill(la,1,n),activation_lw=7,activation_la=0.8,xticks=[],yticks=[],saveat=0.05,adaptive=false,dt=0.0025,maxiters=1e6,xguide="Time",yguide="Concentration",gillespie_interpretation=false,kwargs...)
    sols = gillespie_interpretation ? ssamonte_activation(p_vals,pre_stress_t,post_stress_t,n;saveat=saveat) : monte_activation(p_vals,pre_stress_t,post_stress_t,n;model=model,saveat=saveat,adaptive=adaptive,dt=dt,maxiters=maxiters)
    plot!(sols;vars=1,color=colors,la=las,lw=lws,xticks=xticks,yticks=yticks,xguide=xguide,yguide=yguide,label="",ylimit=(0.,ymax),kwargs...)
    plot_vertical_line!(0,(ymax==Inf ? 1.05*maximum(first.(vcat(getfield.(sols.u,:u)...))) : max(ymax,1.05*maximum(first.(vcat(getfield.(sols.u,:u)...))))),lw=activation_lw,la=activation_la,left_margin=5mm,xlimit=(-pre_stress_t,post_stress_t))
end


# Plots a single activation trajectory.
plot_simulation(args...;kwargs...) = (plot(); plot_simulation!(args...;kwargs...);)
function plot_simulation!(p_vals::Vector{Float64}, tspan::Tuple{Float64,Float64};model=model3,saveat=0.1,maxiters=1e6,adaptive=false,dt=0.0025,ymax=Inf,vars=[1],color=:darkblue,lw=4,la=0.6,colors=fill(color,1,length(vars)),lws=fill(lw,1,length(vars)),las=fill(la,1,length(vars)),xticks=[],yticks=[],legendfontsize=11,plot_labels=false,xguide="Time",yguide="Concentration",gillespie_interpretation=false,kwargs...)
    sol = gillespie_interpretation ? ssasim(p_vals,tspan;saveat=saveat,kwargs...) : stochsim(p_vals,tspan;model=model,saveat=saveat,maxiters=maxiters,adaptive=adaptive,dt=dt,kwargs...)
    plot!(sol,vars=vars,color=colors,la=las,lw=lws,xticks=xticks,yticks=yticks,xguide=xguide,yguide=yguide,label="",ylimit=(0.,ymax))
    if plot_labels
        for (i,var) in enumerate(vars)
            plot!([],[],label="$(model.states[var].val.f)",color=colors[i],lw=lws[i],la=las[i]); 
        end
        plot!(legendfontsize=legendfontsize,legend=:topleft)
    end
    plot!(left_margin=5mm,xlimit=tspan)
end

# Plots a monte carlo assemble of trajectories
plot_simulations(args...;kwargs...) = (plot(); plot_simulations!(args...;kwargs...);)
function plot_simulations!(p_vals::Vector{Float64}, tspan::Tuple{Float64,Float64},n;model=model3,saveat=0.1,maxiters=1e6,adaptive=false,dt=0.0025,ymax=Inf,color=:darkblue,lw=4,la=0.6,colors=fill(color,1,n),lws=fill(lw,1,n),las=fill(la,1,n),xticks=[],yticks=[],xguide="Time",yguide="Concentration",gillespie_interpretation=false,kwargs...)
    sols = gillespie_interpretation ? ssamonte(p_vals,tspan,n;saveat=saveat) : monte(p_vals,tspan,n;model=model,saveat=saveat,maxiters=maxiters,adaptive=adaptive,dt=dt,kwargs...)
    plot!(sols,vars=1,color=colors,la=las,lw=lws,xticks=xticks,yticks=yticks,xguide=xguide,yguide=yguide,label="",ylimit=(0.,ymax),left_margin=5mm,xlimit=tspan)
end


### Basic Nullcline Plots ###

# Plots a single (non-trivial) nullcline.
plot_nullcline(args...;kwargs...) = (plot(); plot_nullcline!(args...;kwargs...);)
function plot_nullcline!(p;lw=4,color=:red,xmin=0.,xmax=Inf,ymin=0.,ymax=Inf,label="",linealpha=1.0,linestyle=:solid,kwargs...)
    zeros = nullcline_zeros(p)
    (length(zeros)==3) && plot!(nullcline(p),zeros[2],zeros[3];lw=lw,color=color,label="",kwargs...)
    plot!(nullcline(p),p[4],zeros[1];lw=lw,xguide="σ",yguide="A",xlimit=(xmin,xmax),ylimit=(ymin,ymax),label=label,color=color,linealpha=linealpha,linestyle=linestyle,kwargs...)
end

plot_nullclines(args...;kwargs...) = (plot(); plot_nullclines!(args...;kwargs...);)
function plot_nullclines!(p,target_par,vals;set_label=true,lw=4,colors=fill(:red,length(vals)),xmin=0.,xmax=Inf,ymin=0.,ymax=Inf,linealpha=1.0,linestyle=:solid,xguide="σ",yguide="A",kwargs...)
    labels = set_label ? map(val -> string(target_par)*" = "*string(val), vals) : fill("",length(vals))
    foreach(i->plot_nullcline!(setindex!(copy(p),vals[i],par2idx(target_par));lw=lw,label=labels[i],color=colors[i],xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,linealpha=linealpha,linestyle=linestyle,kwargs...), 1:length(vals))
    return plot!(xguide=xguide,yguide=yguide)
end

# Plots full nullcline set for single parameter value
make_nullcline(args...;kwargs...) = (plot(); make_nullcline!(args...;kwargs...);)
function make_nullcline!(p;lw=4,color1=:green,color2=:red,label1="",label2="",xmin=0.,xmax=1.3,ymin=0.,ymax=Inf,linealpha=1.0,linestyle=:solid,kwargs...)
    plot!(x->x;lw=lw,xlimit=(0.,max(xmax,5.)),label=label1,color=color1,kwargs...)
    plot_nullcline!(p;lw=lw,xguide="σ",yguide="A",color=color2,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,label=label2,linealpha=linealpha,linestyle=linestyle,kwargs...)
end

# Plots full nullcline set, while varrying the value of a single parameter.
make_nullclines(args...;kwargs...) = (plot(); make_nullclines!(args...;kwargs...);)
function make_nullclines!(p,target_par,vals;lw=4,color1=:green,colors2=fill(:red,length(vals)),label1="",set_label=true,xmin=0.,xmax=1.,ymin=0.,ymax=Inf,kwargs...)
    plot!(x->x;lw=lw,xlimit=(0.,max(xmax,5)),label=label1,color=color1,kwargs...)
    plot_nullclines!(p,target_par,vals;lw=lw,xguide="σ",yguide="A",set_label=set_label,colors=colors2,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,kwargs...)
end


### Plot NC Min Maxes ###
plot_nc_min_maxes(args...;kwargs...) = (plot(); plot_nc_min_maxes!(args...;kwargs...);)
function plot_nc_min_maxes!(p_vals::Vector{Float64}, tspan::Tuple{Float64,Float64};model=model3,lw=1,la=0.9)
    ncmm = nc_local_min_max(p_vals); 
    (length(ncmm) == 0) && push!(ncmm,large_v0_inactive_active_threshold(p_vals[5]))
    foreach(val -> plot!([tspan...],[val,val],color=:black,linestyle=:dot,lw=lw,la=la,label=""), ncmm)
    plot!()
end
  

### Composite Simulation-Nullcline Plots ###

# Creates an activation simulation, and plots it in phase-space.
plot_activation_pspace(args...;kwargs...) = (plot(); plot_activation_pspace!(args...;kwargs...);)
function plot_activation_pspace!(p_vals::Vector{Float64}, pre_stress_t::Float64, post_stress_t::Float64;model=model3,color_prestress=:lightblue,color_poststress=:darkblue,la=0.6,lw=4,xticks=[],yticks=[],legendfontsize=11,plot_labels=false,nc_lw=5,nc_color1=:green,nc_colors2=[:pink, :red],xmax=1.3,ymax=1.3,title="",
                                    gillespie_interpretation=false,saveat=0.05,adaptive=false,dt=0.0025,maxiters=1e6,
                                    sol = gillespie_interpretation ? ssasim_activation(p_vals,pre_stress_t,post_stress_t;saveat=saveat) : stochsim_activation(p_vals,pre_stress_t,post_stress_t;model=model,saveat=saveat,adaptive=adaptive,dt=dt,maxiters=maxiters),                                    
                                    kwargs...)
    colors = [fill(color_prestress,Int64(pre_stress_t/saveat)+1)...,fill(color_poststress,length(sol.t)-Int64(pre_stress_t/saveat)-1)...]
    gillespie_interpretation && (colors = [[fill(color_prestress[1],Int64(pre_stress_t/saveat)+1)...,fill(color_poststress[1],100000-Int64(pre_stress_t/saveat)-1)...] [fill(color_prestress[2],Int64(pre_stress_t/saveat)+1)...,fill(color_poststress[2],100000-Int64(pre_stress_t/saveat)-1)...]]) # Probably a small bug in the plot recepie causes this to be required 
    plot(sol,vars=(1,length(model.states)),color=colors,la=la,lw=lw,xticks=xticks,yticks=yticks,xguide="σ",yguide="$(model.states[end].val.f)",label="",kwargs...)
    if plot_labels
        plot!([],[],label="Before stress",color=color_prestress,lw=lw,la=la); 
        plot!([],[],label="After stress",color=color_poststress,lw=lw,la=la); 
        plot!(legendfontsize=legendfontsize,legend=:topleft)
    end
    gillespie_interpretation && return plot!(xlimit=(0.,xmax),ylimit=(0.,ymax),title=title,left_margin=5mm)
    return make_nullclines!(p_vals,:S,[p_vals[4],p_vals[1]],color1=nc_color1,colors2=nc_colors2,lw=nc_lw,set_label=false,xmax=xmax,ymax=ymax,title=title,left_margin=5mm)
end

# Creates a simulation, and plots it in phase-space.
plot_simulation_pspace(args...;kwargs...) = (plot(); plot_simulation_pspace!(args...;kwargs...);)
function plot_simulation_pspace!(p_vals::Vector{Float64}, tspan::Tuple{Float64,Float64};model=model3,color_prestress=:lightblue,color=:darkblue,lw=4,la=0.6,xticks=[],yticks=[],saveat=0.05,adaptive=false,dt=0.0025,maxiters=1e6,legendfontsize=11,nc_lw=5,nc_color1=:green,nc_colors2=[:pink, :red],xmax=1.3,ymax=1.3,title="",gillespie_interpretation=false,kwargs...)
    sol = gillespie_interpretation ? ssasim(p_vals,tspan;saveat=saveat,kwargs...) : stochsim(p_vals,tspan;model=model,saveat=saveat,maxiters=maxiters,adaptive=adaptive,dt=dt,kwargs...)
    plot(sol,vars=(1,length(model.states)),color=color,la=la,lw=lw,xticks=xticks,yticks=yticks,xguide="σ",yguide="$(model.states[end].val.f)",label="",kwargs...)
    gillespie_interpretation && return plot!(xlimit=(0.,xmax),ylimit=(0.,ymax),title=title,left_margin=5mm)
    return make_nullclines!(p_vals,:S,[p_vals[4],p_vals[1]],color1=nc_color1,colors2=nc_colors2,lw=nc_lw,set_label=false,xmax=xmax,ymax=ymax,title=title,left_margin=5mm)
end

# Creates an activation simulation, and plots it both over time and in phase-space.
function plot_activation_time_n_pspace(p_vals::Vector{Float64}, pre_stress_t::Float64, post_stress_t::Float64;model=model3,gillespie_interpretation=false,saveat=0.05,adaptive=false,dt=0.0025,maxiters=1e6,
                                        color_prestress_t=[:lightblue :darkgoldenrod1],color_poststress_t=[:darkblue :darkorange3],ymax_t=Inf,la_t=[0.9 0.7],lw_t=[4 3],activation_line_width_t=5,xticks_t=[],yticks_t=[],legendfontsize_t=11,plot_labels_t=false,xguide_t="Time",yguide_t="Concentration",
                                        color_prestress_ps=:lightblue,color_poststress_ps=:darkblue,la_ps=0.6,lw_ps=4,xticks_ps=[],yticks_ps=[],legendfontsize_ps=11,plot_labels_ps=false,nc_lw_ps=5,nc_color1_ps=:green,nc_colors2_ps=[:pink, :red],xmax_ps=1.3,ymax_ps=1.3)
    sol = gillespie_interpretation ? ssasim_activation(p_vals,pre_stress_t,post_stress_t;saveat=saveat) : stochsim_activation(p_vals,pre_stress_t,post_stress_t;model=model,saveat=saveat,adaptive=adaptive,dt=dt,maxiters=maxiters)
    p_time = plot_activation(p_vals,pre_stress_t,post_stress_t;model=model,vars=[1,length(model.states)],color_prestress=color_prestress_t,color_poststress=color_poststress_t,ymax=ymax_t,la=la_t,lw=lw_t,activation_line_width=activation_line_width_t,xticks=xticks_t,yticks=yticks_t,legendfontsize=legendfontsize_t,plot_labels=plot_labels_t,xguide=xguide_t,yguide=yguide_t,sol=sol)
    p_pspace = plot_activation_pspace(p_vals,pre_stress_t,post_stress_t;model=model,color_prestress=color_prestress_ps,color_poststress=color_poststress_ps,la=la_ps,lw=lw_ps,xticks=xticks_ps,yticks=yticks_ps,legendfontsize=legendfontsize_ps,plot_labels=plot_labels_ps,nc_lw=nc_lw_ps,nc_color1=nc_color1_ps,nc_colors2=nc_colors2_ps,xmax=xmax_ps,ymax=ymax_ps,sol=sol)
    return (p_time,p_pspace)
end;
