### Fetch Required Packages ###
using Polynomials


### System Functions ###

# An array of all ODEFunctions
odefuns = map(model -> ODEFunction(convert(ODESystem,model),jac=true), models)
# An array of all system functions.
sys_Fs = map(odefun -> (u,p) -> odefun(u,p,0), odefuns)
# An array of all jacobians.
sys_Js = map(odefun -> (u,p) -> odefun.jac(u,p,0), odefuns)

# Functions, taking a model and returning the appropriate function.
get_F(model) = sys_Fs[length(model.states)-1]
get_J(model) = sys_Js[length(model.states)-1]

# Function for the (non-trivial) nullcline.
function nullcline(p)
    S,D,τ,v0,n = p
    return σ -> (1/D)*max(((1/(σ-v0)-1)*(S*σ)^n-1),0.)^(1/n)
end

### System Properties ###

# Gets the value for which the system has a roots.
function sys_zeros(p)
    S,D,τ,v0,n = p
    coefficients = zeros(Int64(n)+2)
    coefficients[Int64(n)+2] = -S^n-D^n
    coefficients[Int64(n)+1] = v0*(S^n+D^n)+S^n
    coefficients[2] = -1
    coefficients[1] = v0
    return sort(real.(filter(x->(imag(x)==0)&&real(x)>0.,roots(Polynomial(coefficients)))))
end

# Get system roots.
function sys_roots(model,p)
    return map(z -> fill(z,length(model.states)), sys_zeros(p))
end

# Gets the (local) min and max values of the nullcline.
function nc_local_min_max(p)
    S,D,τ,v0,n = p
    pol = Polynomial([v0+v0^2,1/n-2*v0-1,1])
    return sort(real.(filter(x->(imag(x)==0)&&real(x)>0.,roots(pol))))
end

# For a given parameter set, given v0>nc_2_max_v0(p), finds the threshold between an active/inactive system.
function large_v0_inactive_active_threshold(n)
    return (n/4)+1/(4n)-0.5+(n-1)/(2*n)
end

# For a given n, there is a threshold value of v0 so that the nullcline don't have local minimum/maximums.
function nc_2_max_v0(n)
    return 0.25*n+0.25/n-0.5 
 end

# Finds the places where a nullcines intersects with zero.
function nullcline_zeros(p)
    S,D,τ,v0,n = p
    coefficients = zeros(Int64(n)+2)
    coefficients[Int64(n)+2] = -S^(n)
    coefficients[Int64(n)+1] = S^n * (1+v0)
    coefficients[2] = -1
    coefficients[1] = v0
    return sort(real.(filter(x->(imag(x)==0)&&real(x)>0.,roots(Polynomial(coefficients)))))
end

### Miscellaneous Utility Functions ###

# Finds the index corresponding to the target parameter (as a symbol) in the parameter array.
function par2idx(target_par)
    return findfirst([:S, :D, :τ, :v0, :n, :η] .== target_par)
end

# Finds the index corresponding to the target variable (as a symbol) in the variable array.
function var2idx(target_par)
    return findfirst([:σ, :A1, :A2, :A3, :A4, :A5] .== target_par)
end

# For a parameter set, returns the 4 paraemters τ, v0, n, η.
function get_τ_v0_n_η(parameters)
    (length(parameters)==4) && return parameters
    (length(parameters)==6) && return parameters[3:6]
    error("Paraemter entry set have weird length (nether 4 nor 6).")
end