### Fetch Required Packages ###

using Serialization
using StatsBase


### Sets Path to Files ###

if isdir("Data/BehaviourMap")
    behaviour_path_1I = ""
    behaviour_path_3I = ""
elseif isdir("../../loman_thesis_2022_uncommited/Chapter6_SigGen/Data/BehaviourMap")
    behaviour_path_1I = "../../loman_thesis_2022_uncommited/Chapter6_SigGen/"
    behaviour_path_3I = "../../loman_thesis_2022_uncommited/Chapter6_SigGen/"
else
    error("Data directory not found.")
end

### Dataset structure ###

# Contains the information regarding a dataset.
struct DataSet
    dataset_tag::String
    S_grid::Vector{Float64}
    D_grid ::Vector{Float64}
    τ_grid::Vector{Float64}
    v0_grid::Vector{Float64}
    n_grid::Vector{Float64}
    η_grid::Vector{Float64}
    p_grids::Vector{Vector{Float64}}

    function DataSet(dataset_tag,S_grid,D_grid,τ_grid,v0_grid,n_grid,η_grid)
        new(dataset_tag,S_grid,D_grid,τ_grid,v0_grid,n_grid,η_grid,[S_grid,D_grid,τ_grid,v0_grid,n_grid,η_grid])
    end
end


### Behaviour Grid Structures ###

# Contains a single grid of behaviours.
struct BehaviourGrid
    behaviours::Matrix{Symbol}
    S_grid::Array{Float64,1}
    D_grid::Array{Float64,1}
    params::Array{Float64,1}
    file_existed::Bool

    function BehaviourGrid(parameters,dataset; bp=behaviour_path_3I)
        τ,v0,n,η = get_τ_v0_n_η(parameters)
        filename = bp*"Data/$(dataset.dataset_tag)/DeterminedBehaviour/behaviours_v0_$(v0)_n_$(n)_η_$(η)/grid_τ_$(τ)_v0_$(v0)_n_$(n)_η_$(η).jls"
        behaviours = (isfile(filename) ? deserialize(filename) : fill(:empty_behaviour,length(dataset.S_grid),length(dataset.D_grid)))
        new(behaviours,S_grid,D_grid,parameters,isfile(filename))
    end
end

# Contains a grid of behaviour grids.
struct BehaviourGrids
    behaviour_grids::Matrix{BehaviourGrid}
    parameters::Vector{Float64}
    par1::Symbol
    par2::Symbol
    vals1::Vector{Float64}
    vals2::Vector{Float64}

    function BehaviourGrids(parameters,dataset;bp=behaviour_path_3I,τ_values=[parameters[1]],v0_values=[parameters[2]],n_values=[parameters[3]],η_values=[parameters[4]])
        lengths = length.([τ_values,v0_values,n_values,η_values])
        (count(lengths .> 1) != 2) && error("Weird input vectors given")
        idx1 = findfirst(lengths .> 1)
        idx2 = findlast(lengths .> 1)
        
        behaviour_grids = Matrix{BehaviourGrid}(undef,lengths[idx1],lengths[idx2])
        for (τi,τ) in enumerate(τ_values), (v0i,v0) in enumerate(v0_values), (ni,n) in enumerate(n_values), (ηi,η) in enumerate(η_values)
            behaviour_grids[[τi,v0i,ni,ηi][idx1],[τi,v0i,ni,ηi][idx2]] = BehaviourGrid([τ,v0,n,η],dataset,bp=bp)
        end
        return new(behaviour_grids,parameters,[:τ,:v0,:n,:η][idx1],[:τ,:v0,:n,:η][idx2],[τ_values,v0_values,n_values,η_values][idx1],[τ_values,v0_values,n_values,η_values][idx2])
    end
end
