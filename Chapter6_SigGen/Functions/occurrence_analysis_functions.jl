### Generates Occurence Counts ####

# Lists all behaviours.
list_of_behaviours = [:no_activation, :single_response_pulse, :stochastic_pulsing, :oscillation, :stochastic_anti_pulsing, :homogeneous_activation, :heterogeneous_activation, :stochastic_switching, :stable_bistability, :homogeneous_intermediate_activation]

# Function which counts the occurences.
function count_occurences(dataset)
    counts = Dict{Symbol,Dict{Symbol,Dict{Float64,Int64}}}()
    for behaviour in list_of_behaviours
        counts[behaviour] = Dict{Symbol,Dict{Float64,Int64}}()
        for (param,grid) in zip([:τ, :v0, :n, :η],dataset.p_grids[3:end])
            counts[behaviour][param] = Dict{Float64,Int64}()
            for val in grid
                counts[behaviour][param][val] = 0
            end
        end
    end

    ### Counts Occurences ###
    for τ in dataset.τ_grid, η in dataset.η_grid, v0 in dataset.v0_grid, n in dataset.n_grid
        bg = BehaviourGrid([τ,v0,n,η],dataset);
        !bg.file_existed && error("Try to read faulty file, this might scew the results")
        for behaviour in list_of_behaviours
            counts[behaviour][:τ][τ] += count(bg.behaviours .== behaviour)
            (n>2) && (counts[behaviour][:v0][v0] += count(bg.behaviours .== behaviour))
            counts[behaviour][:n][n] += count(bg.behaviours .== behaviour)
            counts[behaviour][:η][η] += count(bg.behaviours .== behaviour)
        end
    end
    return counts
end

### Prepares Plotting Functions ###
function plot_occurences(behaviour,counts,dataset;label=["τ" "v₀" "n" "η"],xguide=["" "" "" ""],yguide=["" "" "" ""],lw=5,ms=6,la=0.9)
    x_axises = dataset.p_grids[3:end];
    y_axises = map((param,grid) -> map(val->counts[behaviour][param][val], grid) ./ maximum(map(val->counts[behaviour][param][val], grid)), [:τ, :v0, :n, :η], dataset.p_grids[3:end])
    scales = [:log,:log,:none,:log]
    return map((xs,ys,i) -> (plot(xs,ys,xaxis=scales[i],label=label[i],xguide=xguide[i],yguide=yguide[i],lw=lw,xticks=[],yticks=[],xlimit=(0.98*xs[1],1.02*xs[end]),ylimit=(-0.07,1.07),la=la,color=i); scatter!(xs,ys,label="",ms=ms,linealpha=0.9,color=i);), x_axises, y_axises,1:4)
end