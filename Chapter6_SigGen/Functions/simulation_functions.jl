### Fetch Required Packages ###
using DiffEqJump, OrdinaryDiffEq, StochasticDiffEq


### System Simulations ###

# Simplified calls to the previous simuation functions (model 3 is assumed).
detsim(p_vals::Vector{Float64}, tspan::Tuple{Float64,Float64}; model=model3, init_fp=1, u0=sys_roots(model,p_vals)[init_fp], kwargs...) = detsim(model,p_vals,tspan;u0=u0,kwargs...)
stochsim(p_vals::Vector{Float64}, tspan::Tuple{Float64,Float64}; model=model3, init_fp=1, u0=sys_roots(model,p_vals)[init_fp], saveat=0.1, maxiters=1e6, kwargs...) = stochsim(model,p_vals,tspan;u0=u0,saveat=saveat,maxiters=maxiters,kwargs...)
monte(p_vals::Vector{Float64}, tspan::Tuple{Float64,Float64}, n::Int64; model=model3, init_fp=1, u0=sys_roots(model,p_vals)[init_fp], saveat=0.1, maxiters=1e6, kwargs...) = monte(model,p_vals,tspan,n;u0=u0,saveat=saveat,maxiters=maxiters,kwargs...)
ssasim(p_vals::Vector{Float64}, tspan::Tuple{Float64,Float64}; saveat=0.1, kwargs...) = ssasim(model_gillespie,p_vals,tspan;saveat=saveat,kwargs...)
ssamonte(p_vals::Vector{Float64}, tspan::Tuple{Float64,Float64}, n::Int64; saveat=0.1, kwargs...) = ssamonte(model_gillespie,p_vals,tspan,n;saveat=saveat,kwargs...)


# System Activation Simulations ###

# Single activation using the RRE interpretation
function detsim_activation(p_vals::Vector{Float64}, pre_stress_t::Float64, post_stress_t::Float64; model=model3, kwargs...)
    detsim(model,setindex!(copy(p_vals),0.,1),(-pre_stress_t,post_stress_t);u0=fill(p_vals[4],length(model.states)),p_steps=(:S,(0,p_vals[1])),kwargs...)
end
# Single activation using the CLE interpretation
function stochsim_activation(p_vals::Vector{Float64}, pre_stress_t::Float64, post_stress_t::Float64; model=model3, saveat=0.1, maxiters=1e6, adaptive=true, kwargs...)
    init_model = Model(model,setindex!(copy(p_vals),0.,1))
    u0 = stochsim(init_model,(0.,500.),u0=fill(p_vals[4],length(init_model.v_syms))).u[end] 
    stochsim(model,setindex!(copy(p_vals),0.,1),(-pre_stress_t,post_stress_t);u0=u0,p_steps=(:S,(0,p_vals[1])),saveat=saveat,maxiters=maxiters,adaptive=adaptive,kwargs...)
end
# Monte carlo activation using the CLE interpretation
function monte_activation(p_vals::Vector{Float64}, pre_stress_t::Float64, post_stress_t::Float64, n::Int64; model=model3, saveat=0.1, maxiters=1e6, adaptive=true, kwargs...)
    init_model = Model(model,setindex!(copy(p_vals),0.,1))
    u0s = map(sol -> sol.u[end], monte(init_model,(0.,500.),n,u0=fill(p_vals[4],length(init_model.v_syms))))  
    monte(init_model,(-pre_stress_t,post_stress_t),n;u0s=u0s,p_steps=(:S,(0,p_vals[1])),saveat=saveat,maxiters=maxiters,adaptive=adaptive,kwargs...)
end
# Single activation using the Gillespie interpretation
function ssasim_activation(p_vals::Vector{Float64}, pre_stress_t::Float64, post_stress_t::Float64; saveat=0.1, kwargs...)
    init_model = Model(model_gillespie,setindex!(copy(p_vals),0.,3))
    u0 = ssasim(init_model,(0.,500.),u0=fill(p_vals[1]/p_vals[end],length(init_model.v_syms))).u[end] 
    ssasim(init_model,(-pre_stress_t,post_stress_t);u0=u0,p_steps=(:I,(0,p_vals[3])),saveat=saveat,kwargs...)
end
# Monte carlo activation using the Gillespie interpretation
function ssamonte_activation(p_vals::Vector{Float64}, pre_stress_t::Float64, post_stress_t::Float64, n::Int64; saveat=0.1, kwargs...)
    init_model = Model(model_gillespie,setindex!(copy(p_vals),0.,3))
    u0s = map(sol -> sol.u[end], ssamonte(init_model,(0.,500.),n,u0=fill(p_vals[1]/p_vals[end],length(init_model.v_syms))))  
    ssamonte(init_model,(-pre_stress_t,post_stress_t),n;u0s=u0s,p_steps=(:I,(0,p_vals[3])),saveat=saveat,kwargs...)
end