### HPC Scan

To scan model behaviour across large parameter space we used an HPC. This is a self-contained folder for performing such scans. The output is stored in the "Data" folder, and can then be moved where the figure generating scripts can find it. This wasrequired since this folder, when we performed the scan, was located elsewhere. The "Logs" folder contain the logs. The "Scripts" folder contains the scripts for running the scan.

The thesis repsoitory contain the output for the scans that we used, so unless you want to run a modified scan, you do not actually need this folder.