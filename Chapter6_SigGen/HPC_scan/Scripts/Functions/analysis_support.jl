### Fetch Required Packages ###
using Plots
using Plots.Measures
using StatsBase


### Feteches Required Files ###
include("analysis_support_base_functions.jl")


###  Loads and Manipulates Behaviour Grids (Base) ###
function load_behaviour_grid(parameters;dataset_tag="main",check_unknown_behaviours=true,n_2_int=false)
    τ,v0,n,η = get_τ_v0_n_η(parameters)
    (n_2_int ? Int64(n) : n)
    behaviour_grid = deserialize("../Data/$(dataset_tag)/behaviours_v0_$(v0)_n_$(n_2_int ? Int64(n) : n)_η_$(η)/grid_τ_$(τ)_v0_$(v0)_n_$(n_2_int ? Int64(n) : n)_η_$(η).jls")
    check_unknown_behaviours && any(:unclassified_behaviour  .== get_behaviour.(behaviour_grid)) && @warn "The behaviour grid contains classifications corresponding to unknown behaviour."
    return behaviour_grid
end

# Gets all of the behaviour classifications whithout corresponding behaviour class.
function find_unregistered_classes(behaviour_grid)
    sort(collect(countmap(filter(behaviour -> !in(behaviour,keys(known_behaviour_list)),behaviour_grid))),by=x->-x[2])
end

# Gets the parameters corresponding to certain index.
function get_param(i::Int64,j::Int64,parameters,S_grid,D_grid)
    return [S_grid[i],D_grid[j],get_τ_v0_n_η(parameters)...]
end
# Gets the parameters corresponding to certain index. Also plots the position of the parameter in the parameter set.
function get_param(i::Int64,j::Int64,parameters,behaviour_grid,S_grid,D_grid;display_info=true,smooth=false,idx_axis=true,marker_color=:black)
    plot_behaviour_grid(behaviour_grid,S_grid,D_grid,smooth=smooth,idx_axis=idx_axis)
    display(scatter!((j,i),color=marker_color,label=""))
    display_info && display_behaviour_summary(behaviour_grid[i,j])
    return [S_grid[i],D_grid[j],get_τ_v0_n_η(parameters)...]
end
# Gets the parameters corresponding to certain file name (only 4).
function get_param(filename::String)
    τ = parse(Float64,filename[findfirst("_τ_",filename)[end]+1:findfirst("_v0_",filename)[1]-1])
    v0 = parse(Float64,filename[findfirst("_v0_",filename)[end]+1:findfirst("_n_",filename)[1]-1])
    n = parse(Float64,filename[findfirst("_n_",filename)[end]+1:findfirst("_η_",filename)[1]-1])
    η = parse(Float64,filename[findfirst("_η_",filename)[end]+1:findfirst(".jls",filename)[1]-1])
    return [τ,v0,n,η]
end

# Search all files (corresponding to the given parameter inputs). Returns all parameters corresponding to the target behaviour (as a classification => parameter vector).
function search_file_for_behaviour(target_behaviour::Symbol,S_grid,D_grid,τ_grid,v0_grid,n_grid,η_grid;dataset_tag="main",behaviour_type_long=false)
    output_parameters = Dict{BehaviourClass,Vector{Vector{Float64}}}()
    for τ in τ_grid, v0 in v0_grid, n in n_grid, η in η_grid
        !file_exists(dataset_tag,τ,v0,n,η) && continue
        behaviour_grid = load_behaviour_grid([τ,v0,n,η];dataset_tag=dataset_tag,check_unknown_behaviours=false)
        for (Si,S) in enumerate(S_grid), (Dj,D) in enumerate(D_grid)
            if target_behaviour == (behaviour_type_long ? get_behaviour_long(behaviour_grid[Si,Dj]) : get_behaviour(behaviour_grid[Si,Dj]))
                if !haskey(output_parameters,behaviour_grid[Si,Dj])
                    output_parameters[behaviour_grid[Si,Dj]] = [[S,D,τ,v0,n,η]]
                else
                    push!(output_parameters[behaviour_grid[Si,Dj]],[S,D,τ,v0,n,η])
                end
            end
        end
    end
    return sort(collect(output_parameters),by=x->length(x[2]))
end
# Search all files (corresponding to the given parameter inputs). Returns all parameters corresponding to the target classification (as a vector).
function search_file_for_behaviour(target_classification::BehaviourClass,S_grid,D_grid,τ_grid,v0_grid,n_grid,η_grid;dataset_tag="main")
    output_parameters = Vector{Vector{Float64}}()
    for τ in τ_grid, v0 in v0_grid, n in n_grid,η in η_grid
        !file_exists(dataset_tag,τ,v0,n,η) && continue
        behaviour_grid = load_behaviour_grid([τ,v0,n,η];dataset_tag=dataset_tag,check_unknown_behaviours=false)
        origin_parameters = get_origin_params(behaviour_grid,S_grid,D_grid,[τ,v0,n,η],target_classification)
        append!(output_parameters,origin_parameters)
    end
    return output_parameters
end

# Search all files in dataset folder. Returns all parameters corresponding to the target behaviour (as a classification => parameter vector).
function search_dataset_for_behaviour(target_behaviour::Symbol,S_grid,D_grid;dataset_tag="main",behaviour_type_long=false)
    output_parameters = Dict{BehaviourClass,Vector{Vector{Float64}}}()
    for folder in readdir("../Data/$(dataset_tag)"), filename in readdir("../Data/$(dataset_tag)/$(folder)")
        behaviour_grid = deserialize("../Data/$(dataset_tag)/$(folder)/$(filename)")
        for (Si,S) in enumerate(S_grid), (Dj,D) in enumerate(D_grid)
            if target_behaviour == (behaviour_type_long ? get_behaviour_long(behaviour_grid[Si,Dj]) : get_behaviour(behaviour_grid[Si,Dj]))
                if !haskey(output_parameters,behaviour_grid[Si,Dj])
                    output_parameters[behaviour_grid[Si,Dj]] = [[S,D,get_param(filename)...]]
                else
                    push!(output_parameters[behaviour_grid[Si,Dj]],[S,D,get_param(filename)...])
                end
            end
        end
    end
    return sort(collect(output_parameters),by=x->length(x[2]))
end
# Search all files in dataset folder. Returns all parameters corresponding to the target classification (as a vector).
function search_dataset_for_behaviour(target_classification::BehaviourClass,S_grid,D_grid;dataset_tag="main")
    output_parameters = Vector{Vector{Float64}}()
    for folder in readdir("../Data/$(dataset_tag)"), filename in readdir("../Data/$(dataset_tag)/$(folder)")
        behaviour_grid = deserialize("../Data/$(dataset_tag)/$(folder)/$(filename)")
        origin_parameters = get_origin_params(behaviour_grid,S_grid,D_grid,get_param(filename),target_classification)
        append!(output_parameters,origin_parameters)
    end
    return output_parameters
end


### Extracts Information from Behaviour Grid ###

# Counts how many time each behaviour type ocurs in a grid.
function get_behaviour_counts(behaviour,long=false)
    long && (return countmap(get_behaviour_long.(behaviour_grid)))
    return countmap(get_behaviour.(behaviour_grid))    
end

# Counts how many time each classification type ocurs in a grid.
function get_classification_counts(behaviour)
    return countmap(behaviour_grid)    
end

# Gets all parameter sets which produce the corresponding behaviour or classification
function get_origin_params(behaviour_grid,S_grid,D_grid,parameters,behaviour_or_classification;behaviour_type_long=false)
    parameter_values = []
    for (Si,S) in enumerate(S_grid), (Dj,D) in enumerate(D_grid)
        (behaviour_or_classification isa BehaviourClass) && (behaviour_grid[Si,Dj]==behaviour_or_classification) && push!(parameter_values,get_param(Si,Dj,parameters,S_grid,D_grid))
        (behaviour_or_classification isa Symbol) && behaviour_type_long && (get_behaviour_long(behaviour_grid[Si,Dj])==behaviour_or_classification) && push!(parameter_values,get_param(Si,Dj,parameters,S_grid,D_grid))
        (behaviour_or_classification isa Symbol) && !behaviour_type_long && (get_behaviour(behaviour_grid[Si,Dj])==behaviour_or_classification) && push!(parameter_values,get_param(Si,Dj,parameters,S_grid,D_grid))
    end
    return parameter_values
end

# Gets the indexes of all parameter sets which produce the corresponding behaviour or classification
function get_origin_params_idxs(behaviour_grid,behaviour_or_classification;behaviour_type_long=false)
    parameter_idxs = []
    for Si in 1:size(behaviour_grid)[1], Dj in 1:size(behaviour_grid)[2]
        (behaviour_or_classification isa BehaviourClass) && (behaviour_grid[Si,Dj]==behaviour_or_classification) && push!(parameter_idxs,(Si,Dj))
        (behaviour_or_classification isa Symbol) && behaviour_type_long && (get_behaviour_long(behaviour_grid[Si,Dj])==behaviour_or_classification) && push!(parameter_idxs,(Si,Dj))
        (behaviour_or_classification isa Symbol) && !behaviour_type_long && (get_behaviour(behaviour_grid[Si,Dj])==behaviour_or_classification) && push!(parameter_idxs,(Si,Dj))
    end
    return parameter_idxs
end


### Plots Behaviour Grids (Base) ###

# Plots a behaviour grid.
function plot_behaviour_grid(behaviour_grid, S_grid, D_grid; smooth=true, idx_axis=false, size=(600,400), title="", xguide="", yguide="", kwargs...)
    color_grid = (smooth ? smooth_colors(get_color_grid(behaviour_grid)) : get_color_grid(behaviour_grid))
    if idx_axis 
        xticks = 10:20:length(D_grid)
        yticks = 10:20:length(S_grid)
    else
        xticks = (range(1, length(D_grid), length = 9), map(i->i[1:3],string.(10 .^(range(-1,stop=2,length=9)))))
        yticks = (range(1, length(S_grid), length = 6), map(i->i[1:3],string.(10 .^(range(0,stop=2,length=6)))))
    end
    return plot(color_grid,yflip=false,aspect_ratio=:none,framestyle=:box,title=title,xguide=xguide,yguide=yguide,bottom_margin=3mm,left_margin=4mm,xticks=xticks,yticks=yticks,size=size,kwargs...)
end

# Smooths the colors, filtering out noise. 
function smooth_colors(color_grid)
    return [get_major_color(get_colors_from_color_grid(color_grid,i,j)) for i in 1:size(color_grid)[1], j in 1:size(color_grid)[2]]
end    

# For a given color grid and set of index, returns a (weigthed) list of the target, and it neighbours, colors.
function get_colors_from_color_grid(color_grid,i,j)
    colors = fill(color_grid[i,j],3)
    for I = max(i-1,1):1:min(i+1,size(color_grid)[1]), J = max(j-1,1):1:min(j+1,size(color_grid)[2])
        (I==i) && (J==j) && continue
        push!(colors,color_grid[I,J])
        (I==i || J==j) && push!(colors,color_grid[I,J])
    end
    return colors
end


### Plots Behaviour Grids (Extended) ###
function plot_behaviour_grid_compare_filter(behaviour_grid, S_grid, D_grid; idx_axis=false, xguide="", yguide="", size=(1200,400), kwargs...)
    p1 = plot_behaviour_grid(behaviour_grid, S_grid, D_grid,idx_axis=idx_axis,smooth=false,title="Unfiltered",xguide=xguide,yguide=yguide,kwargs...)
    p2 = plot_behaviour_grid(behaviour_grid, S_grid, D_grid,idx_axis=idx_axis,title="Filtered",xguide=xguide,kwargs...)
    return plot(p1,p2,size=size,left_margin=20mm)
end

# Gets the parameters corresponding to certain index. Also plots the position of the parameter in the parameter set.
function display_param(S::Float64,D::Float64,behaviour_grid,S_grid,D_grid;display_info=false,smooth=false)
    display_info && display_behaviour_summary(behaviour_grid[findfirst(S_grid .> S),findfirst(D_grid .> D)])
    plot_behaviour_grid(behaviour_grid,S_grid,D_grid,smooth=smooth,idx_axis=false)
    scatter!((findfirst(D_grid .> D),findfirst(S_grid .> S)),color=:black,label="")
end
# Gets the parameters corresponding to certain index. Also plots the position of the parameter in the parameter set.
function display_param(parameters::Vector{Float64},behaviour_grid,S_grid,D_grid;display_info=false,smooth=false)
    display_info && display_behaviour_summary(behaviour_grid[findfirst(S_grid .> parameters[1]),findfirst(D_grid .> parameters[2])])
    plot_behaviour_grid(behaviour_grid,S_grid,D_grid,smooth=smooth,idx_axis=false)
    scatter!((findfirst(D_grid .> parameters[2]),findfirst(S_grid .> parameters[1])),color=:black,label="")
end

# Marks region in parameter space where the designated behaviour exists.
function mark_behaviour(behaviour_or_classification,behaviour_grid, S_grid, D_grid; mark_color=RGB{Float64}(0.,0.,0.), behaviour_type_long=false, idx_axis=false, size=(600,400), title="", xguide="", yguide="",kwargs...)
    color_grid = get_color_grid(behaviour_grid)
    foreach(idx -> color_grid[idx...]=mark_color, get_origin_params_idxs(behaviour_grid,behaviour_or_classification;behaviour_type_long=behaviour_type_long))
    if idx_axis 
        xticks = 10:20:length(D_grid)
        yticks = 10:20:length(S_grid)
    else
        xticks = (range(1, length(D_grid), length = 9), map(i->i[1:3],string.(10 .^(range(-1,stop=2,length=9)))))
        yticks = (range(1, length(S_grid), length = 6), map(i->i[1:3],string.(10 .^(range(0,stop=2,length=6)))))
    end
    return plot(color_grid,yflip=false,aspect_ratio=:none,framestyle=:box,title=title,xguide=xguide,yguide=yguide,bottom_margin=3mm,left_margin=4mm,xticks=xticks,yticks=yticks,size=size,kwargs...)
end

# Marks the color, but also compares to the original plot
function mark_behaviour_compare(behaviour_or_classification,behaviour_grid, S_grid, D_grid; mark_color=RGB{Float64}(0.,0.,0.), behaviour_type_long=false, smooth=false, idx_axis=false, size=(1200,400), xguide="", yguide="",kwargs...)
    p1 = plot_behaviour_grid(behaviour_grid, S_grid, D_grid; smooth=smooth, idx_axis=idx_axis, xguide=xguide, yguide=yguide, kwargs...)
    p2 = mark_behaviour(behaviour_or_classification,behaviour_grid, S_grid, D_grid; mark_color=mark_color, behaviour_type_long=behaviour_type_long, idx_axis=idx_axis, xguide=xguide,kwargs...)
    return plot(p1,p2,size=size)
end

# Marks region in parameter space where the designated function returns true.
function mark_region(filter::Function,behaviour_grid, S_grid, D_grid; mark_color=RGB{Float64}(0.,0.,0.), idx_axis=false, size=(600,400), title="", xguide="", yguide="",kwargs...)
    color_grid = get_color_grid(behaviour_grid)
    for i = 1:length(S_grid), j = 1:length(D_grid)
        filter(behaviour_grid[i,j]) && (color_grid[i,j]=mark_color)
    end
    if idx_axis 
        xticks = 10:20:length(D_grid)
        yticks = 10:20:length(S_grid)
    else
        xticks = (range(1, length(D_grid), length = 9), map(i->i[1:3],string.(10 .^(range(-1,stop=2,length=9)))))
        yticks = (range(1, length(S_grid), length = 6), map(i->i[1:3],string.(10 .^(range(0,stop=2,length=6)))))
    end
    return plot(color_grid,yflip=false,aspect_ratio=:none,framestyle=:box,title=title,xguide=xguide,yguide=yguide,bottom_margin=3mm,left_margin=4mm,xticks=xticks,yticks=yticks,size=size,kwargs...)
end

# For a array of parameter values, plto all the corresponding behaviour grids.
function plot_behaviour_with_parameter(parameters,S_grid, D_grid; τ_grid=[parameters[1]], v0_grid=[parameters[2]], n_grid=[parameters[3]], η_grid=[parameters[4]], idx_axis=false, size=(600,400), dataset_tag="main", title="", xguide="", yguide="")
    (sum(length.([τ_grid, v0_grid, n_grid, η_grid]).==1) != 3) && error("Wrong number of parameter grids given in input")
    (length(parameters)!=4) && error("Parameter array should be of length 4")
    target_param = ["τ","v0","n","τ"][findfirst(length.([τ_grid, v0_grid, n_grid, η_grid]) .!= 1)]
    target_array = [τ_grid, v0_grid, n_grid, η_grid][findfirst(length.([τ_grid, v0_grid, n_grid, η_grid]) .!= 1)]
    plots = []; iteration = 0;
    for τ in τ_grid, v0 in v0_grid, n in n_grid, η in η_grid
        iteration += 1
        !file_exists(dataset_tag,τ,v0,n,η) && continue
        push!(plots,plot_behaviour_grid_compare_filter(load_behaviour_grid([τ,v0,n,η],dataset_tag=dataset_tag),S_grid, D_grid,idx_axis=idx_axis,yguide="$(target_param)=$(target_array[iteration])"))
    end
    (length(plots)==0) && error("No plots found, plot array is empty.")
    return plot(plots...,layout=(length(plots),1),size=(size[1]*2,size[2]*length(plots)))
end


### Plotting utility Functions ###

# For a small array of colors, find the dominating one (or pick the original one if none is dominating it enough).
function get_major_color(colors;color_original=colors[1])
    color_count_dict = countmap(colors)
    color_counts = sort(collect(color_count_dict),by=x->x[2])
    (color_counts[end][2] > color_count_dict[color_original]) && (return color_counts[end][1])
    return color_original
end

### Miscellaneous Utility Functions ###
function display_behaviour_summary(behaviour)
    display(behaviour)
    display(get_behaviour_long(behaviour))
    display(get_behaviour(behaviour))
end
# Checks if there eixsts a file for the corresponding parameters.
function file_exists(dataset_tag,τ,v0,n,η,n_2_int=false)
    return isfile("../Data/$(dataset_tag)/behaviours_v0_$(v0)_n_$(n_2_int ? Int64(n) : n)_η_$(η)/grid_τ_$(τ)_v0_$(v0)_n_$(n_2_int ? Int64(n) : n)_η_$(η).jls")
end

### Investigate Parameter Function ###

# Displays an investigation of what classification a parameter set have, and why that classification  was given.
function investigate_parameter(p;m=8,l=6000,display_investigation=true)
    # Gets behaviour classifications.
    #behaviour_classification = determine_behaviour(p)
    #behaviour_long = get_behaviour_long(behaviour_classification)
    #behaviour = get_behaviour(behaviour_long)

    # Calulcate system properties.
    u0_perturb = (p[6] < 0.025) ? ((p[6] < 0.0025) ? (0.85,1.15) : (0.95,1.05)) : (1.,1.) 
    rts = system_zeros(p)
    stabs = map(r -> maximum(real.(eigen(system_jac([r[1],r[1],r[1],r[1]],p)).values))<0, rts)
    nc_min_max = nc_local_min_max(p)
    nc_zeros = nullcline_zeros(p)
    fps,fp_type = find_fp_type(p,rts,nc_min_max,stabs,u0_perturb)
    fps = map(fp->[fp[1],fp[2],fp[2],fp[2]], fps)

    # Make standard behaviour plots
    p_start = setindex!(copy(p),0.,1); S_step = p[1];
    sols = monte(p_start,(0.,2000.),5,u0=[p[4],p[4],p[4],p[4]],callbacks=(stress_step_cb(500.,S_step),),saveat=1.,tstops=[500.]);
    plot(sols,vars=[1],linealpha=0.4,framestyle=:box,grid=false,left_margin=3mm,right_margin=3mm,bottom_margin=3mm,tickfontsize=8,guidefontsize=10,xguide="Time",yguide="[σ]",label="")
    p_behaviour_1 = plot_vertical_line!(500,1.35)
    plot(sols[1],vars=[1],framestyle=:box,grid=false,linealpha=0.8,left_margin=3mm,right_margin=3mm,bottom_margin=3mm,tickfontsize=8,guidefontsize=10,xguide="Time",yguide="[σ]",label="",color=:green)
    p_behaviour_2 = plot_vertical_line!(500,1.35)
    plot(sols[1],vars=(1,2),framestyle=:box,grid=false,color=[fill(:lightgreen,501)...,fill(:darkgreen,1500)...],linealpha=0.1,left_margin=3mm,right_margin=3mm,bottom_margin=3mm,tickfontsize=8,guidefontsize=10,label="")
    p_behaviour_3 = make_nullcline!(p)
    behaviour_plot = plot(p_behaviour_1,p_behaviour_2,p_behaviour_3,bottom_margin=5mm,size=(1300,300),layout=(1,3))

    
    # Check initial activation.
    p_init_1 = plot(framestyle=:box,grid=false,xticks=[],yticks=[]); p_init_2 = plot(framestyle=:box,grid=false,xticks=[],yticks=[]);
    if !in(fp_type,[:tri_unstable_inactive_ss,:tri_unstable_intermediary_ss,:tri_unstable_active_ss,:tri_unstable_bistable_ss,:unknown_fp_type])
        u0_init = [p[4],p[4],p[4],p[4]]; threshold_init = nc_min_max[2];
        sim_length = ((fps[end][1] < nc_min_max[1]) || ((fp_type==:biunstability) && (fps[1][1]<nc_min_max[1])) ? 200. : l)
        sols_init = monte(p,(0.,sim_length),m,u0=u0_init,eSolver=EnsembleThreads(),callbacks=(terminate_sim(u0_init,threshold_init),),saveat=.1)
        vals_init = vcat(map(i -> first.(sols_init[i].u), 1:length(sols_init))...)
        ends_init = maximum(vcat(map(i -> sols_init[i].t[end], 1:length(sols_init))...))
        ends_vals_init = map(i -> [sols_init[i].t[end],sols_init[i].u[end][1]], 1:length(sols_init))
        plot([0.,sim_length],[threshold_init,threshold_init],linewidth=3.,linestyle=:dash,color=:black,label="")
        (nc_min_max[1] < fps[end][1] < nc_min_max[2]) || ((fp_type==:biunstability) && (fps[1][1]>nc_min_max[1])) && plot!([0.,sim_length],[nc_min_max[1],nc_min_max[1]],linewidth=3.,linestyle=:dash,color=:black,label="")
        plot!(sols_init,vars=[1],xlimit=(0.,max(10.,ends_init)),ylimit=(min(0.,minimum(vals_init)),max(1.35,maximum(vals_init))),linealpha=0.4,framestyle=:box,grid=false,left_margin=3mm,right_margin=3mm,bottom_margin=3mm,tickfontsize=8,guidefontsize=10,xguide="Time",yguide="[σ]",label="")
        p_init_1 = scatter!((first.(ends_vals_init),last.(ends_vals_init)),color=:yellow,label="")
            
        plot(sols_init[1],vars=(1,2),framestyle=:box,grid=false,color=:green,linealpha=0.4,left_margin=3mm,right_margin=3mm,bottom_margin=3mm,tickfontsize=8,guidefontsize=10,label="")
        make_nullcline!(p)
        plot!([threshold_init,threshold_init],[0.,1.35],linewidth=3.,linestyle=:dash,color=:black,label="")
        p_init_2 = scatter!(([u0_init[1],sols_init[1].u[end][1]],[u0_init[2],sols_init[1].u[end][2]]),color=[:palevioletred2,:yellow],label="")
    end

    # Check deactivation.
    p_de_1 = plot(framestyle=:box,grid=false,xticks=[],yticks=[]); p_de_2 = plot(framestyle=:box,grid=false,xticks=[],yticks=[]);
    if !in(fp_type,[:biunstability,:inactive,:intermediate_activation_inactive_ss,:oscillation_inactive_ss,:intermediate_activation_intermediary_ss,:tri_unstable_inactive_ss,:tri_unstable_intermediary_ss,:tri_unstable_active_ss,:tri_unstable_bistable_ss,:unknown_fp_type])
        u0_de = u0_perturb[2]*fps[end]; threshold_de = nc_min_max[1]
        sols_de = monte(p,(0.,l),m,u0=u0_de,eSolver=EnsembleThreads(),callbacks=(terminate_sim(u0_de,threshold_de),),saveat=.1)
        vals_de = vcat(map(i -> first.(sols_de[i].u), 1:length(sols_de))...)
        ends_de = maximum(vcat(map(i -> sols_de[i].t[end], 1:length(sols_de))...))
        ends_vals_de = map(i -> [sols_de[i].t[end],sols_de[i].u[end][1]], 1:length(sols_de))
        plot([0.,l],[threshold_de,threshold_de],linewidth=3.,linestyle=:dash,color=:black,label="")
        plot!(sols_de,vars=[1],xlimit=(0.,max(10.,ends_de)),ylimit=(min(0.,minimum(vals_de)),max(1.35,maximum(vals_de))),linealpha=0.4,framestyle=:box,grid=false,left_margin=3mm,right_margin=3mm,bottom_margin=3mm,tickfontsize=8,guidefontsize=10,xguide="Time",yguide="[σ]",label="")
        p_de_1 = scatter!((first.(ends_vals_de),last.(ends_vals_de)),color=:yellow,label="")
        
        plot(sols_de[1],vars=(1,2),framestyle=:box,grid=false,color=:green,linealpha=0.4,left_margin=3mm,right_margin=3mm,bottom_margin=3mm,tickfontsize=8,guidefontsize=10,label="")
        make_nullcline!(p)
        plot!([threshold_de,threshold_de],[0.,1.35],linewidth=3.,linestyle=:dash,color=:black,label="")
        p_de_2 = scatter!(([u0_de[1],sols_de[1].u[end][1]],[u0_de[2],sols_de[1].u[end][2]]),color=[:palevioletred2,:yellow],label="")
    end
    
    # Check reactivation.
    p_re_1 = plot(framestyle=:box,grid=false,xticks=[],yticks=[]); p_re_2 = plot(framestyle=:box,grid=false,xticks=[],yticks=[]);
    if !in(fp_type,[:active,:intermediate_activation_active_ss,:oscillation_active_ss,:intermediate_activation_intermediary_ss,:tri_unstable_inactive_ss,:tri_unstable_intermediary_ss,:tri_unstable_active_ss,:tri_unstable_bistable_ss,:unknown_fp_type])
        u0_re = u0_perturb[1]*fps[1]; threshold_re = nc_min_max[2]
        sols_re = monte(p,(0.,l),m,u0=u0_re,eSolver=EnsembleThreads(),callbacks=(terminate_sim(u0_re,threshold_re),),saveat=.1)
        vals_re = vcat(map(i -> first.(sols_re[i].u), 1:length(sols_re))...)
        ends_re = maximum(vcat(map(i -> sols_re[i].t[end], 1:length(sols_re))...))
        ends_vals_re = map(i -> [sols_re[i].t[end],sols_re[i].u[end][1]], 1:length(sols_re))
        plot([0.,l],[threshold_re,threshold_re],linewidth=3.,linestyle=:dash,color=:black,label="")
        plot!(sols_re,vars=[1],xlimit=(0.,max(10.,ends_re)),ylimit=(min(0.,minimum(vals_re)),max(1.35,maximum(vals_re))),linealpha=0.4,framestyle=:box,grid=false,left_margin=3mm,right_margin=3mm,bottom_margin=3mm,tickfontsize=8,guidefontsize=10,xguide="Time",yguide="[σ]",label="")
        p_re_1 = scatter!((first.(ends_vals_re),last.(ends_vals_re)),color=:yellow,label="")
        
        plot(sols_re[1],vars=(1,2),framestyle=:box,grid=false,color=:green,linealpha=0.4,left_margin=3mm,right_margin=3mm,bottom_margin=3mm,tickfontsize=8,guidefontsize=10,label="")
        make_nullcline!(p)
        plot!([threshold_re,threshold_re],[0.,1.35],linewidth=3.,linestyle=:dash,color=:black,label="")
        p_re_2 = scatter!(([u0_re[1],sols_re[1].u[end][1]],[u0_re[2],sols_re[1].u[end][2]]),color=[:palevioletred2,:yellow],label="")
    end

    passage_simulations_plot = plot(p_init_1,p_de_1,p_re_1,p_init_2,p_de_2,p_re_2,bottom_margin=5mm,size=(1300,600),layout=(2,3))
    

    # Check steady state distribution (for fps from distribution).
    ss_distribution_plot = plot(framestyle=:box,grid=false,xticks=[],yticks=[])
    rt = rts[1]
    sols = monte(p,(0.,5000.),4,u0=u0_perturb[2]*[rt,rt,rt,rt],saveat=.1)
    vals = vcat(map(i->sols[i].u,1:length(sols))...)[10000:end]
    if true || (stabs==[false] || (stabs==[true] && (nc_min_max[1]<rts[1]<nc_min_max[2])))
        dist = kde((first.(vals), last.(vals)))
        plot([nc_min_max[1],nc_min_max[1]],[-0.1,1.35],linewidth=3.,linestyle=:dash,color=:red,label="")
        plot!([nc_min_max[2],nc_min_max[2]],[-0.1,1.35],linewidth=3.,linestyle=:dash,color=:red,label="")
        foreach(fp -> scatter!((fp[1],fp[2]),color=:green,label=""), fps)
        ss_distribution_plot = plot!(dist.x,dist.y,dist.density',framestyle=:box,grid=false,ylimit=(-0.1,1.35),xlabel="σ",ylabel="A")
    end
    hist = kde(first.(vals)); hist_max = 1.1*maximum(hist.density);
    mean_val_1 = mean(first.(vals))
    mean_val_2 = mean(sort(first.(vals))[floor(Int64,length(vals)*0.1):ceil(Int64,length(vals)*0.9)])
    plot(hist.x,hist.density,label="",lw=3.,title="Histogram",xlimit=(-0.1,1.35),xlabel="σ",ylabel="Density")
    plot_vertical_line!(nc_min_max[1],hist_max,lw=2)
    plot_vertical_line!(nc_min_max[2],hist_max,lw=2)
    plot_vertical_line!(mean_val_1,hist_max,lw=2,color=:black)
    p_hist = plot_vertical_line!(mean_val_2,hist_max,lw=2,color=:black)
    perm = welch_pgram(first.(vals) .- mean_val_1)
    p_freq_1 = plot(perm.freq,perm.power,title="Frequency Domain (full)",label="",xlabel="Frequency")
    p_freq_2 = plot(perm.freq[1:findfirst(perm.freq.>0.05)],perm.power[1:findfirst(perm.freq.>0.05)],label="",title="Frequency Domain (limited)",xlabel="Frequency")
    oscillation_characterisation_plot = plot(p_hist,p_freq_1,p_freq_2,framestyle=:box,size=(1300,300),layout=(1,3),bottom_margin=5mm,left_margin=5mm)
    
    sols1 = monte(p,(0.,4500.),4,u0=u0_perturb[2]*[rts[1],rts[1],rts[1],rts[1]])
    sols2 = monte(p,(0.,16500.),1,u0=u0_perturb[2]*[rts[1],rts[1],rts[1],rts[1]])
    vals1 = first.(vcat(map(i->sols1[i].u[min(5000,length(sols1[i].t)):end],1:length(sols1))...))
    vals2 = first.(vcat(map(i->sols2[i].u[min(5000,length(sols2[i].t)):end],1:length(sols2))...))
    perm1 = welch_pgram(vals1 .- mean(vals1))
    perm2 = welch_pgram(vals2 .- mean(vals2))
    p_freq_tmp_1 = plot(perm1.freq[1:findfirst(perm1.freq.>0.05)],perm1.power[1:findfirst(perm1.freq.>0.05)],label="",title="Frequency Domain (limited)",xlabel="Frequency")
    p_freq_tmp_2 = plot(perm2.freq[1:findfirst(perm2.freq.>0.05)],perm2.power[1:findfirst(perm2.freq.>0.05)],label="",title="Frequency Domain (limited)",xlabel="Frequency")

    tmp_debug_plot = plot(p_freq_tmp_1,p_freq_tmp_2,framestyle=:box,size=(650,300),layout=(1,2),bottom_margin=5mm,left_margin=5mm)
    
    # Displays output.
    if display_investigation
        display("[$(p[1]), $(p[2]), $(p[3]), $(p[4]), $(p[5]), $(p[6])]")
        (length(stabs)==1) ? display("Stability type: [$(stabs[1])]") : display("Stability type: [$(stabs[1])    $(stabs[2])    $(stabs[3])]")
        display(fp_type)    
        #display(behaviour_classification)
        #display(behaviour_long)
        #display(behaviour)
        display(behaviour_plot)
        display(passage_simulations_plot)
        display(ss_distribution_plot)
        display(oscillation_characterisation_plot)
        display(tmp_debug_plot)
    end
    return (fp_type=fp_type,behaviour_classification=behaviour_classification,behaviour_long=behaviour_long,behaviour=behaviour,p_behaviour_1=p_behaviour_1,p_behaviour_2=p_behaviour_2,p_behaviour_3=p_behaviour_3,behaviour_plot=behaviour_plot,passage_simulations_plot=passage_simulations_plot,ss_distribution_plot=ss_distribution_plot)
end



### Color Mapping ###

# Gets the color corresponding to a behaviour or classification.
function get_color(behaviour_or_classification::Union{BehaviourClass,Symbol})
    (behaviour_or_classification isa BehaviourClass) && return behaviour_2_color_dict[get_behaviour(behaviour_or_classification)]
    return behaviour_2_color_dict[known_behaviour_list_short[behaviour_or_classification]]
end

# Gets a color grid for a grid of classifications or behaviours.
function get_color_grid(behaviour_or_classification_grid::Union{Matrix{BehaviourClass},Matrix{Symbol}})
    (behaviour_or_classification_grid isa Matrix{BehaviourClass}) && return map(classification -> behaviour_2_color_dict[classification],get_behaviour.(behaviour_or_classification_grid))
    return map(behaviour -> behaviour_2_color_dict[known_behaviour_list_short[behaviour]], behaviour_or_classification_grid)
end


# Maps behaviours to colors for plotting.
const behaviour_2_color_dict = Dict([
    :no_activation =>                           RGB{Float64}(1,1,1),            # White
    :single_response_pulse =>                   RGB{Float64}(1,1,0.75),         # Light Yellow
    :stochastic_pulsing =>                      RGB{Float64}(1,1,0),            # Yellow
    :oscillation =>                             RGB{Float64}(1,0,0),            # Red
    :stochastic_anti_pulsing =>                 RGB{Float64}(0.5,0,0.5),        # Purple
    :homogeneous_activation =>                  RGB{Float64}(0,0,0.5),          # Dark Blue
    :heterogeneous_activation =>                RGB{Float64}(0,0.75,1),         # Light Blue
    :stochastic_switching =>                    RGB{Float64}(0,1,0),            # Light Green
    :stable_bistability =>                      RGB{Float64}(0,0.25,0),         # Dark Green
    :intermediate_activation =>                 RGB{Float64}(0.25,0,0),         # Dark Red
    :tri_unstable =>                            RGB{Float64}(0.5,0.5,0.5),      # Grey
    :empty_behaviour =>                         RGB{Float64}(1,1,1),            # White
    :unknown_behaviour =>                       RGB{Float64}(0.5,0.5,0.5),      # Grey
    :convergence_failure =>                     RGB{Float64}(0.5,0.5,0.5),      # Grey
    :max_iters =>                               RGB{Float64}(0,1,0.75),         # Weird Green
    :unclassified_behaviour =>                  RGB{Float64}(0,0,0)])           # Black