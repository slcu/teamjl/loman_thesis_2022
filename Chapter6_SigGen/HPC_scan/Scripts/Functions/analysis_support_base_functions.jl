### Simulation Functions ###

# Callback, produces a step increase in stress.
function stress_step_cb(step_time,step_amplitude)
    condition(u,t,integrator) = (t===step_time)
    affect!(integrator)= integrator.p[1] += step_amplitude
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end

### Plotting Utility Functions ###

# Plots a vertical line (to e.g. mark stress addition).
plot_vertical_line(args...;kwargs...) = (plot(); plot_vertical_line!(args...;kwargs...))
plot_vertical_line!(x_value,heigth;linestyle=:dash,lw=3,color=:red,label="") = plot!([x_value,x_value],[0,heigth],lw=lw,color=color,linestyle=linestyle,label=label)
plot_vertical_lines(args...;kwargs...) = (plot(); plot_vertical_lines!(args...;kwargs...))
function plot_vertical_lines!(x_values,heigth;linestyle=:dash,lw=3,colors=fill(:red,length(x_values)),label="")
    for (i,x_value) in enumerate(x_values)
        plot_vertical_line!(x_value,heigth;linestyle=linestyle,lw=lw,color=colors[i],label=label)
    end
    plot!()
end

### Nullcline Plotting ###

# Plots a single nullcline (of the complicated one).
function plot_nullcline(args...;kwargs...)
    plot(); plot_nullcline!(args...;kwargs...)
end
function plot_nullcline!(p,args...;lw=4,color=:red,xmin=0.,xmax=1.,ymin=0.,ymax=1.,xlimit=(xmax>xmin)&&(xmin,xmax),ylimit=(ymax>ymin)&&(ymin,ymax),label="",kwargs...)
    rts = nullcline_zeros(p)
    (length(rts)==3) && plot!(nullcline(p),rts[2],rts[3],args...;lw=lw,color=color,label="",kwargs...)
    plot!(nullcline(p),p[4],rts[1],args...;lw=lw,xguide="σ",yguide="A",xlimit=xlimit,ylimit=ylimit,label=label,color=color,kwargs...)
end

# Used for ploting a nullcline
function nullcline(p)
    S,D,τ,v0,n = p
    σ -> (1/D)*max(((1/(σ-v0)-1)*(S*σ)^n-1),0.)^(1/n)
end
# Finds the places where a nullcines intersects with zero.
function nullcline_zeros(p)
    S,D,τ,v0,n = p
    coefficients = zeros(Int64(n)+2)
    coefficients[Int64(n)+2] = -S^(n)
    coefficients[Int64(n)+1] = S^n * (1+v0)
    coefficients[2] = -1
    coefficients[1] = v0
    return sort(real.(filter(x->(imag(x)==0)&&real(x)>0.,roots(Polynomial(coefficients)))))
end

# Plots a set nullclines (of the complicated one), as a parameter is varries.
function plot_nullclines(args...;kwargs...)
    plot(); plot_nullclines!(args...;kwargs...)
end
function plot_nullclines!(p,target_par,vals,args...;set_label=true,colors=fill(:red,length(vals)),lw=4,xmin=0.,xmax=1.,ymin=0.,ymax=1.,kwargs...)
    labels = set_label ? map(val -> string(target_par)*" = "*string(val), vals) : fill("",length(vals))
    for (i,val) in enumerate(vals)
        plot_nullcline!(setindex!(copy(p),val,par2idx[target_par]),args...;lw=lw,label=labels[i],color=colors[i],xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,kwargs...)
    end
    return plot!(xguide="σ",yguide="A")
end

# Plots full nullcline set for single parameter value
function make_nullcline(args...;kwargs...)
    plot(); make_nullcline!(args...;kwargs...)
end
function make_nullcline!(p,args...;lw=4,color1=:blue,color2=:red,label1="",label2="",xmin=0.,xmax=1.,ymin=0.,ymax=1.,kwargs...)
    plot!(x->x,args...;lw=lw,xlimit=(0.,max(xmax,5)),label=label1,color=color1,kwargs...)
    plot_nullcline!(p,args...;lw=lw,xguide="σ",yguide="A",color=color2,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,label=label2,kwargs...)
end

# Plots full nullcline set, while varrying the value of a single parameter.
function make_nullclines(args...;kwargs...)
    plot(); make_nullclines!(args...;kwargs...)
end
function make_nullclines!(p,target_par,vals,args...;lw=4,color1=:blue,colors2=fill(:red,length(vals)),label1="",set_label=true,xmin=0.,xmax=1.,ymin=0.,ymax=1.,kwargs...)
    plot!(x->x,args...;lw=lw,xlimit=(0.,max(xmax,5)),label=label1,color=color1,kwargs...)
    plot_nullclines!(p,target_par,vals,args...;lw=lw,xguide="σ",yguide="A",set_label=set_label,colors=colors2,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,kwargs...)
end