function ccf1(ev::ParameterEvaluation;instant_activation_limit=15.,perm_max_power_thres=10.)
    p = ev.p
    nc_min_max = ev.nc_min_max
    l = ev.l
    ssd = ev.steady_state_distribution_data
    fpd = ev.fp_data
    td = ev.transition_data
    is_default = ev.is_default
    default = ev.default
    

    is_default && return default
    any(isnothing.([ssd,fpd,td])) && return :unknown_behaviour
    (length(nc_min_max)!=2) && return (fpd.fps_type==:inactive ? :no_activation : :homogeneous_activation)

    initial_pulse_activation_type = passage_times_evaluation(td.initial_pulse_activation_times,200.,instant_activation_limit=instant_activation_limit)
    initial_activation_type = passage_times_evaluation(td.initial_activation_times,l,instant_activation_limit=instant_activation_limit)
    deactivation_type = passage_times_evaluation(td.deactivation_times,l,instant_activation_limit=instant_activation_limit)
    reactivation_type = passage_times_evaluation(td.reactivation_times,l,instant_activation_limit=instant_activation_limit)

    if fpd.fps_type == :bistability_intermediary_active
        (ssd.median_activity > nc_min_max[2]) && return :unknown_behaviour
        (ssd.median_activity > nc_min_max[1]) && return :unknown_behaviour
        if any(reactivation_type .== [:instantaneous,:instantaneous_delayed])
            return :oscillation
        elseif reactivation_type == :heterogeneous
            return :stochastic_pulsing
        elseif reactivation_type == :infinite 
            return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
        end    
    end

    if fpd.fps_type == :inactive
        if any(reactivation_type .== [:instantaneous,:instantaneous_delayed])
            return :oscillation
        elseif reactivation_type == :heterogeneous
            return :stochastic_pulsing
        elseif reactivation_type == :infinite 
            return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
        end
    elseif fpd.fps_type == :intermediary
        return ((ssd.perm_max_power < perm_max_power_thres) ? :homogeneous_intermediate_activation : :oscillation)
    elseif fpd.fps_type == :active
        if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
            return :oscillation
        elseif deactivation_type == :heterogeneous
            return :stochastic_anti_pulsing
        elseif deactivation_type == :infinite 
            return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :homogeneous_activation : :heterogeneous_activation)
        end
    elseif any(fpd.fps_type .== [:bistability,:bistability_intermediary_active])
        if any(reactivation_type .== [:instantaneous,:instantaneous_delayed])
            if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
                return :oscillation
            elseif deactivation_type == :heterogeneous
                return :stochastic_anti_pulsing
            elseif deactivation_type == :infinite 
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :homogeneous_activation : :heterogeneous_activation)
            end
        elseif reactivation_type == :heterogeneous
            if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
                return :stochastic_pulsing
            elseif deactivation_type == :heterogeneous
                return :stochastic_switching
            elseif deactivation_type == :infinite 
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :homogeneous_activation : :heterogeneous_activation)
            end
        elseif reactivation_type == :infinite 
            if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
            elseif deactivation_type == :heterogeneous
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
            elseif deactivation_type == :infinite 
                if any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed])
                    return :homogeneous_activation 
                elseif initial_pulse_activation_type == :heterogeneous
                    return :heterogeneous_activation
                elseif initial_pulse_activation_type == :infinite 
                    return :stable_bistability                     
                end
            end
        end
    end 
    @warn "The behaviour evaluator could not classify it as a known behaviour for p=$(p)."
    return :unknown_behaviour
end

function ccf2(ev::ParameterEvaluation;new_l=2000.,instant_activation_limit=15.,perm_max_power_thres=10.)
    p = ev.p
    nc_min_max = ev.nc_min_max
    l = new_l
    ssd = ev.steady_state_distribution_data
    fpd = ev.fp_data
    td = ev.transition_data
    is_default = ev.is_default
    default = ev.default
    

    is_default && return default
    any(isnothing.([ssd,fpd,td])) && return :unknown_behaviour
    (length(nc_min_max)!=2) && return (fpd.fps_type==:inactive ? :no_activation : :homogeneous_activation)

    initial_pulse_activation_type = passage_times_evaluation(td.initial_pulse_activation_times,200.,instant_activation_limit=instant_activation_limit)
    initial_activation_type = passage_times_evaluation(min.(td.initial_activation_times,l),l,instant_activation_limit=instant_activation_limit)
    deactivation_type = passage_times_evaluation(min.(td.deactivation_times,l),l,instant_activation_limit=instant_activation_limit)
    reactivation_type = passage_times_evaluation(min.(td.reactivation_times,l),l,instant_activation_limit=instant_activation_limit)

    if fpd.fps_type == :bistability_intermediary_active
        (ssd.median_activity > nc_min_max[2]) && return :unknown_behaviour
        (ssd.median_activity > nc_min_max[1]) && return :unknown_behaviour
        if any(reactivation_type .== [:instantaneous,:instantaneous_delayed])
            return :oscillation
        elseif reactivation_type == :heterogeneous
            return :stochastic_pulsing
        elseif reactivation_type == :infinite 
            return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
        end    
    end

    if (fpd.fps_type == :bistability) && (reactivation_type == :heterogeneous) && any(deactivation_type .== [:instantaneous,:instantaneous_delayed]) && (ssd.median_activity > nc_min_max[1])
        return ((ssd.perm_max_power < perm_max_power_thres) ? :homogeneous_intermediate_activation : :oscillation)
    end

    if fpd.fps_type == :inactive
        if any(reactivation_type .== [:instantaneous,:instantaneous_delayed])
            return :oscillation
        elseif reactivation_type == :heterogeneous
            return :stochastic_pulsing
        elseif reactivation_type == :infinite 
            return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
        end
    elseif fpd.fps_type == :intermediary
        return ((ssd.perm_max_power < perm_max_power_thres) ? :homogeneous_intermediate_activation : :oscillation)
    elseif fpd.fps_type == :active
        if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
            return :oscillation
        elseif deactivation_type == :heterogeneous
            return :stochastic_anti_pulsing
        elseif deactivation_type == :infinite 
            return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :homogeneous_activation : :heterogeneous_activation)
        end
    elseif any(fpd.fps_type .== [:bistability,:bistability_intermediary_active])
        if any(reactivation_type .== [:instantaneous,:instantaneous_delayed])
            if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
                return :oscillation
            elseif deactivation_type == :heterogeneous
                return :stochastic_anti_pulsing
            elseif deactivation_type == :infinite 
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :homogeneous_activation : :heterogeneous_activation)
            end
        elseif reactivation_type == :heterogeneous
            if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
                return :stochastic_pulsing
            elseif deactivation_type == :heterogeneous
                return :stochastic_switching
            elseif deactivation_type == :infinite 
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :homogeneous_activation : :heterogeneous_activation)
            end
        elseif reactivation_type == :infinite 
            if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
            elseif deactivation_type == :heterogeneous
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
            elseif deactivation_type == :infinite 
                if any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed])
                    return :homogeneous_activation 
                elseif initial_pulse_activation_type == :heterogeneous
                    return :heterogeneous_activation
                elseif initial_pulse_activation_type == :infinite 
                    return :stable_bistability                     
                end
            end
        end
    end 
    @warn "The behaviour evaluator could not classify it as a known behaviour for p=$(p)."
    return :unknown_behaviour
end

# Finds the behaviour for the given input data.
function find_behaviour(p::Vector{Float64},b::Symbol,ec::CondensedEvaluation;instant_activation_limit=15.,perm_max_power_thres=10.,passage_fraction=0.85)
    any(map(field -> isnothing(getproperty(ev.condensed,field)), fieldnames(typeof(ev.condensed)))) && return b

    if fpd.fps_type == :inactive
        if any(reactivation_type .== [:instantaneous,:instantaneous_delayed])
            return ((ssd.perm_max_power < perm_max_power_thres) ? :homogeneous_intermediate_activation : :oscillation)
        elseif reactivation_type == :heterogeneous
            return :stochastic_pulsing
        elseif reactivation_type == :infinite 
            return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
        end
    elseif fpd.fps_type == :intermediary
        return ((ssd.perm_max_power < perm_max_power_thres) ? :homogeneous_intermediate_activation : :oscillation)
    elseif fpd.fps_type == :active
        if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
            return ((ssd.perm_max_power < perm_max_power_thres) ? :homogeneous_intermediate_activation : :oscillation)
        elseif deactivation_type == :heterogeneous
            return :stochastic_anti_pulsing
        elseif deactivation_type == :infinite 
            return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :homogeneous_activation : :heterogeneous_activation)
        end
    elseif fpd.fps_type == :bistability
        if any(reactivation_type .== [:instantaneous,:instantaneous_delayed])
            if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
                return :oscillation
            elseif deactivation_type == :heterogeneous
                (ssd.median_activity < nc_min_max[2]) && return ((ssd.perm_max_power < perm_max_power_thres) ? :homogeneous_intermediate_activation : :oscillation)
                return :stochastic_anti_pulsing
            elseif deactivation_type == :infinite 
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :homogeneous_activation : :heterogeneous_activation)
            end
        elseif reactivation_type == :heterogeneous
            if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
                (ssd.median_activity > nc_min_max[1]) && return ((ssd.perm_max_power < perm_max_power_thres) ? :homogeneous_intermediate_activation : :oscillation)
                return :stochastic_pulsing
            elseif deactivation_type == :heterogeneous
                return :stochastic_switching
            elseif deactivation_type == :infinite 
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :homogeneous_activation : :heterogeneous_activation)
            end
        elseif reactivation_type == :infinite 
            if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
            elseif deactivation_type == :heterogeneous
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
            elseif deactivation_type == :infinite 
                if any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed])
                    return :homogeneous_activation 
                elseif initial_pulse_activation_type == :heterogeneous
                    return :heterogeneous_activation
                elseif initial_pulse_activation_type == :infinite 
                    return :stable_bistability                     
                end
            end
        end
    end 
    @warn "The behaviour evaluator could not classify it as a known behaviour for p=$(p)."
    return :unknown_behaviour
end