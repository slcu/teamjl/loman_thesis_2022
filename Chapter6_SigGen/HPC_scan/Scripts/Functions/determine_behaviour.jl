using Statistics, KernelDensity, DSP, LinearAlgebra

### Main Function ###

# Determiens the system behaviour type for a specific parameter set.
function determine_behaviour(p;m=20,l=4000.)
    u0_perturb = (p[6] < 0.025) ? ((p[6] < 0.0025) ? (0.85,1.15) : (0.95,1.05)) : (1.,1.) 
    rts = system_zeros(p)
    stabs = map(r -> maximum(real.(eigen(system_jac([r[1],r[1]],p)).values))<0, rts)
    nc_min_max = nc_local_min_max(p)
    (length(nc_min_max) != 2) && return nc_min_max_exception(p,rts)
    fps,fp_type = find_fp_type(p,rts,nc_min_max,stabs,u0_perturb)
    output =   (fp_type = fp_type,
                initial_activation_type = initial_activation_type(p,fps,fp_type,nc_min_max,m,l),
                deactivation_type = deactivation_type(p,fps,fp_type,nc_min_max,u0_perturb,m,l),
                reactivation_type = reactivation_type(p,fps,fp_type,nc_min_max,u0_perturb,m,l))
    return !any(map(i -> output[i]==:MaxIters,1:length(output))) ? output : (fp_type=:MaxIters,initial_activation_type=:NA,deactivation_type=:NA,reactivation_type=:NA)
end


### Determines Fixedpoint Type ###

# Finds the fixed point type, for given stabilities and fixedpoint positions.
function find_fp_type(p,rts,nc_min_max,stabs,u0_perturb)
    #if stabs==[true]
    #    (rts[1]<nc_min_max[1]) && return (map(rt ->[rt,rt],rts),:inactive)
    #    (rts[1]>nc_min_max[2]) && return (map(rt ->[rt,rt],rts),:active)
    #end
    if length(stabs)==1
        sols = monte(p,(0.,4500.),4,u0=u0_perturb[2]*[rts[1],rts[1],rts[1],rts[1]],saveat=.1)
        vals = vcat(map(i->sols[i].u[min(5000,length(sols[i].t)):end],1:length(sols))...)
        if length(vals) < 20
            mean_val = mean(first.(vals))
        else
            mean_val = mean(sort(first.(vals))[floor(Int64,length(vals)*0.1):ceil(Int64,length(vals)*0.9)])
        end
        perm_power_max = ((length(vals)>10) ? maximum(welch_pgram(first.(vals) .- mean(first.(vals))).power) : 12)
        if false && (perm_power_max < 5.)
            if mean_val < nc_min_max[1]
                (perm_power_max < 2.) && return ([[mean_val,mean(last.(vals))]],:low_freq_inactive_ss)
            elseif mean_val < nc_min_max[2]
                (perm_power_max < 2.) && return ([[mean_val,mean(last.(vals))]],:low_freq_intermediary_ss)
            else
                return ([[mean_val,mean(last.(vals))]],:low_freq_active_ss)
            end
        end

        fps = map(fp -> max.(fp,0.), fps_from_distribution(p,nc_min_max,kde((first.(vals), last.(vals)))))

        if (perm_power_max>15) || ((perm_power_max>10)&&(stabs==[false]))
            if (length(fps)==3)
                (mean_val < nc_min_max[1]) && return ([fps[1]],:oscillation_inactive_ss)
                (nc_min_max[2] < mean_val) && return ([fps[end]],:oscillation_active_ss)
                return (fps,:oscillation_bistable_ss)
            end
            (nc_min_max[1] < mean_val < nc_min_max[2]) && return ([[mean_val,mean(last.(vals))]],:oscillation_intermediary_ss)
            (fps[1][1]<nc_min_max[1]) && return (fps,:oscillation_inactive_ss)
            (fps[1][1]>nc_min_max[2]) && return (fps,:oscillation_active_ss)
            return (fps,:oscillation_intermediary_ss)
        else
            if (length(fps)==3)
                (mean_val < nc_min_max[1]) && return ([fps[1]],:intermediate_activation_inactive_ss)
                (nc_min_max[2] < mean_val) && return ([fps[end]],:intermediate_activation_active_ss)
                return (fps,:intermediate_activation_bistable_ss)
            end
            (nc_min_max[1] < mean_val < nc_min_max[2]) && return ([[mean_val,mean(last.(vals))]],:intermediate_activation_intermediary_ss)
            (fps[1][1]<nc_min_max[1]) && return (fps,:intermediate_activation_inactive_ss)
            (fps[1][1]>nc_min_max[2]) && return (fps,:intermediate_activation_active_ss)
            return (fps,:intermediate_activation_intermediary_ss)
        end
    end
    if stabs==[false,false,false]
        sols = monte(p,(0.,4500.),4,u0=u0_perturb[2]*[rts[2],rts[2]],saveat=.1)
        vals = vcat(map(i->sols[i].u[min(5000,length(sols[i].t)):end],1:length(sols))...)
        fps = map(fp -> max.(fp,0.), fps_from_distribution(p,nc_min_max,kde((first.(vals), last.(vals)))))

        (length(fps)==3) && return (fps,:tri_unstable_bistable_ss)
        (fps[1][1]<nc_min_max[1]) && return (fps,:tri_unstable_inactive_ss)
        (fps[1][1]>nc_min_max[2]) && return (fps,:tri_unstable_active_ss)
        return (fps,:tri_unstable_intermediary_ss)
    end
    if (stabs==[true,false,true]) 
        if rts[3] < nc_min_max[2]
            return (map(rt ->[rt,rt],rts),:bistability_intermediary_active)
        end
        return (map(rt ->[rt,rt],rts),:bistability)
    end
    (stabs==[true,false,false]) && return (map(rt ->[rt,rt],rts),:biunstability)
    return [[],:unknown_fp_type]
end

# For two fixed point types (oscillation and intermediary activation), the stochastic fixed points might be different. This function determines that from the solution distribution.
function fps_from_distribution(p,nc_min_max,dist)
    if (nc_min_max[2] < dist.x[1])
        max3 = maximum(dist.density')
        coord3 = findfirst(dist.density'.==max3)
        pos3 = [dist.x[coord3[2]],dist.y[coord3[1]]];
        return [pos3]
    end
    if (nc_min_max[1] > dist.x[end])
        max1 = maximum(dist.density')
        coord1 = findfirst(dist.density'.==max1)
        pos1 = [dist.x[coord1[2]],dist.y[coord1[1]]];
        return [pos1]
    end
    if (nc_min_max[1] < dist.x[1]) && (nc_min_max[2] > dist.x[end])
        max2 = maximum(dist.density')
        coord2 = findfirst(dist.density'.==max2)
        pos2 = [dist.x[coord2[2]],dist.y[coord2[1]]];
        return [pos2]
    end
    if (nc_min_max[1] > dist.x[1]) && (nc_min_max[2] > dist.x[end])
        xLim1 = findfirst(dist.x .> nc_min_max[1]) - 1
        max1,max2 = maximum.([dist.density'[:,1:xLim1],dist.density'[:,xLim1+1:end]])
        coord1,coord2 = findfirst.([dist.density'[:,1:xLim1].==max1,dist.density'[:,xLim1+1:end].==max2])
        pos1 = [dist.x[coord1[2]],dist.y[coord1[1]]]; pos2 = [dist.x[xLim1+coord2[2]],dist.y[coord2[1]]];
        (max1>max2) && return [pos1]
        return [pos2]
    end
    if (nc_min_max[1] < dist.x[1]) && (nc_min_max[2] < dist.x[end])
        xLim2 = findfirst(dist.x .> nc_min_max[2]) - 1
        max2,max3 = maximum.([dist.density'[:,1:xLim2],dist.density'[:,xLim2+1:end]])
        coord2,coord3 = findfirst.([dist.density'[:,1:xLim2].==max2,dist.density'[:,xLim2+1:end].==max3])
        pos2 = [dist.x[coord2[2]],dist.y[coord2[1]]]; pos3 = [dist.x[xLim2+coord3[2]],dist.y[coord3[1]]];
        (max3>max2) && return [pos3]
        return [pos2]
    end
    xLim1,xLim2 = findfirst.([dist.x .> nc_min_max[1], dist.x .> nc_min_max[2]]) .- 1
    if xLim1==xLim2
        @warn "xLim1 = xLim2 for p=$p"
        max2 = maximum(dist.density')
        coord2 = findfirst(dist.density'.==max2)
        pos2 = [dist.x[coord2[2]],dist.y[coord2[1]]];
        return [pos2]
    end
    max1,max2,max3 = maximum.([dist.density'[:,1:xLim1],dist.density'[:,xLim1+1:xLim2],dist.density'[:,xLim2+1:end]])
    max1edge2,max2edge3 = maximum.([dist.density'[:,xLim1],dist.density'[:,xLim2+1]])
    coord1,coord2,coord3 = findfirst.([dist.density'[:,1:xLim1].==max1,dist.density'[:,xLim1+1:xLim2].==max2,dist.density'[:,xLim2+1:end].==max3])
    pos1 = [dist.x[coord1[2]],dist.y[coord1[1]]]; pos2 = [dist.x[xLim1+coord2[2]],dist.y[coord2[1]]]; pos3 = [dist.x[xLim2+coord3[2]],dist.y[coord3[1]]];
    (max2 > max(max1,max3)) && return [pos2]
    (max2 < min(max1,max3)) && return [pos1,pos2,pos3]
    if max1 > max2
        (max3 > 2*max2edge3) && return [pos1,pos2,pos3]
        return [pos1]
    end
    if max3 > max2
        (max1 > 2*max1edge2) && return [pos1,pos2,pos3]
        return [pos3]
    end
    @warn "Did not find a fixed point from distribution (should not happen), retruns a default value."
    [pos2]
end


### Determines (De)Activation Types ###

# Finds the initial activation type, for a given parameter set.
function initial_activation_type(p,fps,fp_type,nc_min_max,m,l)
    in(fp_type,[:low_freq_inactive_ss,:low_freq_intermediary_ss,:low_freq_active_ss,:tri_unstable_inactive_ss,:tri_unstable_intermediary_ss,:tri_unstable_active_ss,:tri_unstable_bistable_ss,:unknown_fp_type]) && return :NA
    pulse_l = 200. # 8*instant_activation_limit
    initial_activation_times_pulse = get_passage_times(p,[p[4],p[4]],nc_min_max[2],m,pulse_l)
    (classify_passage_times(initial_activation_times_pulse,pulse_l,m) == :instantaneous) && (return :pulse)
    
    (fps[end][1] < nc_min_max[1] || fp_type==:biunstability) && fps[1][1]<nc_min_max[1] && (return :NA)

    threshold = (fps[end][1] < nc_min_max[2] ? nc_min_max[1] : nc_min_max[2])
    (fp_type == :bistability_intermediary_active) && (threshold = fps[end][1])
    initial_activation_times = get_passage_times(p,[p[4],p[4]],threshold,m,l)
    return classify_passage_times(initial_activation_times,l,m)
end

# Finds the deactivation type, for a given parameter set.
function deactivation_type(p,fps,fp_type,nc_min_max,u0_perturb,m,l)
    in(fp_type,[:biunstability,:inactive,:intermediate_activation_inactive_ss,:oscillation_inactive_ss,:intermediate_activation_intermediary_ss,:low_freq_inactive_ss,:low_freq_intermediary_ss,:low_freq_active_ss,:tri_unstable_inactive_ss,:tri_unstable_intermediary_ss,:tri_unstable_active_ss,:tri_unstable_bistable_ss,:unknown_fp_type]) && return :NA
    deactivation_times = get_passage_times(p,fps[end],nc_min_max[1],m,l)
    return classify_passage_times(deactivation_times,l,m)
end

# Finds the reactivation type, for a given parameter set.
function reactivation_type(p,fps,fp_type,nc_min_max,u0_perturb,m,l)
    in(fp_type,[:active,:intermediate_activation_active_ss,:oscillation_active_ss,:intermediate_activation_intermediary_ss,:low_freq_inactive_ss,:low_freq_intermediary_ss,:low_freq_active_ss,:tri_unstable_inactive_ss,:tri_unstable_intermediary_ss,:tri_unstable_active_ss,:tri_unstable_bistable_ss,:unknown_fp_type]) && return :NA
    reactivation_times = get_passage_times(p,fps[1],nc_min_max[2],m,l)
    return classify_passage_times(reactivation_times,l,m)
end


### NC Min Max  Excetion ###

# If the nullcline do not have length 2, provides an alternative output. 
# It is hard to define whenever these systems are (in)active, requires an alternative approach.
function nc_min_max_exception(p,rts)
    if length(rts) != 1
        @warn "The length of the rts vector have a strength_length: $(length(rts))"
        return (fp_type = :unknown_fp_type, initial_activation_type = :NA, deactivation_type = :NA, reactivation_type = :NA)
    end
    (rts[1] < p[4]+0.25) && return (fp_type = :inactive_leng_1_ncminmax, initial_activation_type = :NA, deactivation_type = :NA, reactivation_type = :NA)
    (rts[1] > p[4]+0.75) && return (fp_type = :active_leng_1_ncminmax, initial_activation_type = :NA, deactivation_type = :NA, reactivation_type = :NA)
    return (fp_type = :intermediary_leng_1_ncminmax, initial_activation_type = :NA, deactivation_type = :NA, reactivation_type = :NA)
end

### Passage Time Classification ###

# Gets the times for the system, from a give initial condition, pass a threshold.
function get_passage_times(p,u0,thres,m,l)
    sols = monte(p,(0.,l),m,u0=u0,eSolver=EnsembleThreads(),callbacks=(terminate_sim(u0,thres),),saveat=.1)
    return any(getfield.(sols[:],:retcode) .== :MaxIters) ? :MaxIters : last.(getfield.(sols[:],:t))
end

# Classifies a set of activation times as Instantaneous, Delayed Constant, Heterogeneous, or Infinite (N/A is also a type, but not assigned here).
function classify_passage_times(passage_times,l,m)
    (passage_times == :MaxIters) && return :MaxIters
    instant_activation_limit = 25.
    (count(passage_times .== l) >= m/2.) && return :infinite
    (mean(passage_times) < instant_activation_limit) && return :instantaneous
    (std(passage_times)<(instant_activation_limit/2)) && (mean(passage_times) < 4*instant_activation_limit) && return :instantaneous
    return :heterogeneous
end

# Callback to termiante a simulation when it passes a specific threshold.
function terminate_sim(u0,thres);
    condition = (u0[1]<thres) ? (u,t,integrator)->(u[1]>thres) : (u,t,integrator)->(u[1]<thres)
    affect!(integrator) = terminate!(integrator)
    return DiscreteCallback(condition,affect!,save_positions = (true,true))
end

