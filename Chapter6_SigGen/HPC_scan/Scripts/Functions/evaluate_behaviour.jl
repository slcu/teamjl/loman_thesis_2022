### Fetches Required Packages ###
using Statistics, KernelDensity, DSP, LinearAlgebra


### Parameter Evaluation Structures ###


# Contains the information regarding the steady state distribution.
struct SteadyStateDistributionData
    mean_activity::Float64
    std_activity::Float64
    median_activity::Float64
    mean_activity_A::Float64
    std_activity_A::Float64
    median_activity_A::Float64
    truncated_distribution_means::Vector{Float64}
    truncated_distribution_stds::Vector{Float64}
    truncated_distribution_means_A::Vector{Float64}
    truncated_distribution_stds_A::Vector{Float64}
    percentile_activities::Vector{Float64}
    percentile_activities_A::Vector{Float64}

    passage_counts::Union{Nothing,Vector{Int64}}

    perm_max_power::Float64
    perm_max_power_freq::Float64
    perm_max_power_A::Float64
    perm_max_power_freq_A::Float64

    function SteadyStateDistributionData(vals1,vals2,nc_min_max)
        sorted_vals1 = sort(vals1)
        sorted_vals2 = sort(vals2)
        n_vals = length(vals1)
    
        mean_activity = mean(vals1)
        std_activity = std(vals1)
        median_activity = sorted_vals1[floor(Int64,n_vals/2.)]
        mean_activity_A = mean(vals2)
        std_activity_A = std(vals2)
        median_activity_A = sorted_vals2[floor(Int64,n_vals/2.)]
        
        truncation_values = [0.05,0.10,0.15,0.20,0.25,0.40,0.45,0.46,0.47,0.48,0.49]
        truncated_distribution_means = map(tv -> mean(sorted_vals1[max(1,floor(Int64,tv*n_vals)):min(n_vals,floor(Int64,(1. -tv)*n_vals))]), truncation_values)
        truncated_distribution_stds = map(tv -> std(sorted_vals1[max(1,floor(Int64,tv*n_vals)):min(n_vals,floor(Int64,(1. -tv)*n_vals))]), truncation_values)
        truncated_distribution_means_A = map(tv -> mean(sorted_vals2[max(1,floor(Int64,tv*n_vals)):min(n_vals,floor(Int64,(1. -tv)*n_vals))]), truncation_values)
        truncated_distribution_stds_A = map(tv -> std(sorted_vals2[max(1,floor(Int64,tv*n_vals)):min(n_vals,floor(Int64,(1. -tv)*n_vals))]), truncation_values)
        
        percentiles = [0.0,0.05,0.10,0.15,0.20,0.25,0.33,0.40,0.41,0.42,0.43,0.44,0.45,0.46,0.47,0.48,0.49,0.5,0.51,0.52,0.53,0.54,0.55,0.56,0.57,0.58,0.59,0.6,0.66,0.75,0.8,0.85,0.9,0.95,1.0]
        percentile_activities = map(perc -> sorted_vals1[max(1,floor(Int64,perc*n_vals))], percentiles)
        percentile_activities_A = map(perc -> sorted_vals2[max(1,floor(Int64,perc*n_vals))], percentiles)
    
        wpgram1 = welch_pgram(vals1 .- mean_activity)
        wpgram2 = welch_pgram(vals2 .- mean_activity_A)
        perm_max_power = maximum(wpgram1.power)
        perm_max_power_A = maximum(wpgram2.power)
        perm_max_power_freq = wpgram1.freq[findfirst(wpgram1.power .>= perm_max_power)]
        perm_max_power_freq_A = wpgram2.freq[findfirst(wpgram2.power .>= perm_max_power_A)]

        if length(nc_min_max) == 2
            passage_values = [nc_min_max[1],(nc_min_max[1]+nc_min_max[2])/4,(nc_min_max[1]+nc_min_max[2])/2,3*(nc_min_max[1]+nc_min_max[2])/4,nc_min_max[2]]
            passage_counts = map(pv -> count(map(i -> (vals1[i]<pv)&&(vals1[i+1]>pv),1:n_vals-1)), passage_values)
        else
            passage_counts = nothing
        end
        
        return new(mean_activity,std_activity,median_activity,mean_activity_A,std_activity_A,median_activity_A,truncated_distribution_means,truncated_distribution_stds,truncated_distribution_means_A,truncated_distribution_stds_A,percentile_activities,percentile_activities_A,passage_counts,perm_max_power,perm_max_power_freq,perm_max_power_A,perm_max_power_freq_A)
    end
end

# Contains the information regarding the fixed point.
struct FPData
    fps_dist_type::Union{Nothing,Symbol}
    fps_dist::Union{Nothing,Vector{Vector{Float64}}}
    fps_m_type::Union{Nothing,Symbol}
    fps_m::Union{Nothing,Vector{Vector{Float64}}}
    fps_type::Union{Nothing,Symbol}
    fps::Union{Nothing,Vector{Vector{Float64}}}

    function FPData(vals1,vals2,p,nc_min_max,ssd,det_fps_type,rts)
        dist = kde((vals1, vals2))
        nc_min_max = ((length(nc_min_max)==2) ? nc_min_max : [large_v0_inactive_active_threshold(p),large_v0_inactive_active_threshold(p)])

        coord = findfirst(dist.density'.==maximum(dist.density'))        
        fps_dist = [[dist.x[coord[2]],dist.y[coord[1]],dist.y[coord[1]],dist.y[coord[1]]]]
        fps_dist_type = ((fps_dist[1][1] < nc_min_max[1]) ? :inactive : ((fps_dist[1][1] < nc_min_max[2]) ? :intermediary : :active)) 
        
        percentiles = [0.0,0.05,0.10,0.15,0.20,0.25,0.33,0.40,0.41,0.42,0.43,0.44,0.45,0.46,0.47,0.48,0.49,0.5,0.51,0.52,0.53,0.54,0.55,0.56,0.57,0.58,0.59,0.6,0.66,0.75,0.8,0.85,0.9,0.95,1.0]
        target_percentile = 0.5
        perc_idx1 = findfirst(percentiles .== target_percentile)
        perc_idx2 = length(percentiles)-perc_idx1+1
        fps_m = [[ssd.median_activity,ssd.median_activity,ssd.median_activity,ssd.median_activity]]
        if ssd.median_activity < nc_min_max[1]
            fps_m_type = :inactive 
        elseif ssd.median_activity > nc_min_max[2]
            fps_m_type = :active 
        else
            fps_m_type = :intermediary
        end

        if det_fps_type == :bistability
            fps_type = det_fps_type
            fps = map(rt ->[rt,rt,rt,rt],rts)
        else
            fps_type = fps_m_type
            fps = fps_m
        end
        return new(fps_dist_type,fps_dist,fps_m_type,fps_m,fps_type,fps)
    end
end

# Contains the information regarding system transitions.
struct TransitionData
    initial_activation_type::Symbol
    initial_activation_times::Vector{Float64}
    initial_activation_MaxIters::Bool
    initial_pulse_activation_type::Symbol
    initial_pulse_activation_times::Vector{Float64}
    initial_pulse_activation_MaxIters::Bool
    deactivation_type::Symbol
    deactivation_times::Vector{Float64}
    deactivation_MaxIters::Bool
    reactivation_type::Symbol
    reactivation_times::Vector{Float64}
    reactivation_MaxIters::Bool

    function TransitionData(p,nc_min_max,m,l,fpd)
        (length(nc_min_max)!=2) && return new(:NA,fill(0.,m),false,:NA,fill(0.,m),false,:NA,fill(0.,m),false,:NA,fill(0.,m),false)
        
        pulse_l = 200. # 8*instant_activation_limit
        initial_pulse_activation_times = generate_passage_times(p,[p[4],p[4],p[4],p[4]],nc_min_max[2],m,pulse_l)
        initial_pulse_activation_type = passage_times_evaluation(initial_pulse_activation_times,pulse_l,p)
        initial_pulse_activation_MaxIters = (length(initial_pulse_activation_times)==m)
        
        if in(fpd.fps_type,[:intermediary, :active])
            initial_activation_times = fill(0.,m)
        else
            threshold = ((fpd.fps_type==:bistability_intermediary_active) ? fpd.fps[end][1] : nc_min_max[2])
            initial_activation_times = generate_passage_times(p,fpd.fps[1],threshold,m,l)
        end
        initial_activation_type = passage_times_evaluation(initial_activation_times,l,p)
        initial_activation_MaxIters = (length(initial_activation_times)<m)
        
        deactivation_times = (in(fpd.fps_type,[:inactive, :intermediary]) ? fill(0.,m) : generate_passage_times(p,1.05*fpd.fps[end],nc_min_max[1],m,l))
        deactivation_type = passage_times_evaluation(deactivation_times,l,p)
        deactivation_MaxIters = (length(deactivation_times)<m)
        
        reactivation_times = (in(fpd.fps_type,[:intermediary, :active]) ? fill(0.,m) : generate_passage_times(p,fpd.fps[1],nc_min_max[2],m,l))
        reactivation_type = passage_times_evaluation(reactivation_times,l,p)
        reactivation_MaxIters = (length(reactivation_times)<m)
        
        new(initial_activation_type,initial_activation_times,initial_activation_MaxIters,initial_pulse_activation_type,initial_pulse_activation_times,initial_pulse_activation_MaxIters,deactivation_type,deactivation_times,deactivation_MaxIters,reactivation_type,reactivation_times,reactivation_MaxIters)
    end
end

# Contains the information from the evaluation of a single parameter set.
mutable struct ParameterEvaluation
    p::Vector{Float64}
    rts::Vector{Float64}
    max_real_eigen_vals::Vector{Float64}
    stabs::Vector{Bool}
    nc_min_max::Vector{Float64}
    det_fps_type::Symbol

    sols_data::Vector{NamedTuple{(:seed, :maxval, :retcode, :end_val),Tuple{UInt64,Float64,Symbol,Float64}}}
    steady_state_distribution_data::Union{Nothing,SteadyStateDistributionData}
    fp_data::Union{Nothing,FPData}
    transition_data::Union{Nothing,TransitionData}
    steady_state_distribution_reduced_data::Vector{SteadyStateDistributionData}

    behaviour::Symbol
    m::Int64
    l::Float64
    is_default::Bool
    default::Symbol
    additional_information::Vector{Any}

    function ParameterEvaluation(p;m=4*estimate_m(p[6]),l=2000.,default=:no_default_behaviour)
        evaltime = @elapsed begin
            rts = system_zeros(p)
            max_real_eigen_vals = map(r -> maximum(real.(eigen(system_jac([r[1],r[1],r[1],r[1]],p)).values)), rts)
            stabs = max_real_eigen_vals .< 0
            nc_min_max = nc_local_min_max(p)
            det_fps_type = deterministic_fps_type(stabs,nc_min_max,rts)
            is_default = (default != :no_default_behaviour)
            
            evaltime1 = @elapsed begin
                sd,vals1,vals2 = get_ss_distribution_vals(p,rts,is_default)
            end
            if vals1!=[]
                ssd = SteadyStateDistributionData(vals1,vals2,nc_min_max)
                fpd = FPData(vals1,vals2,p,nc_min_max,ssd,det_fps_type,rts)
                evaltime2 = @elapsed begin
                    td = TransitionData(p,nc_min_max,m,l,fpd)
                end
            else
                evaltime2 = 0.0;
                ssd,fpd,td = fill(nothing,3)
            end
            ssds = map(leng -> SteadyStateDistributionData(vals1[1:leng],vals2[1:leng],nc_min_max), filter(leng->leng<length(vals1),10000*[1,2,4,8]))

            behaviour = find_behaviour(p,nc_min_max,l,ssd,fpd,td,is_default,default)
        end
        new(p,rts,max_real_eigen_vals,stabs,nc_min_max,det_fps_type,sd,ssd,fpd,td,ssds,behaviour,m,l,is_default ,default,[evaltime,evaltime1,evaltime2])
    end
end
# Vaguely scales m with the level of noise.
function estimate_m(η)
    if η < 0.001
        return 10
    elseif η < 0.01
        return ceil(Int64,10+(η-0.001)*500)
    elseif η < 0.1
        return ceil(Int64,15+(η-0.01)*50)
    elseif η < 0.2
        return ceil(Int64,20+(η-0.1)*200)
    else
        return 40
    end
end


### Structure Generation Helper Functions ###

# Finds the deterministic fp type of a set of stabilities.
function deterministic_fps_type(stabs,nc_min_max,rts)
    if  isequal(stabs,[true])
        (length(nc_min_max)!=2) && return :single_ss_bad_nc_min_max
        (rts[1] < nc_min_max[1]) && return :inactive
        (rts[1] < nc_min_max[2]) && return :intermediary_active
        return :active
    elseif isequal(stabs,[false])
        (length(nc_min_max)!=2) && return :single_ss_oscillation_bad_nc_min_max
        (rts[1] < nc_min_max[1]) && return :inactive_oscillation
        (rts[1] < nc_min_max[2]) && return :intermediary_active_oscillation
        return :active_oscillation
    elseif isequal(stabs,[true,false,false])
        (length(nc_min_max)!=2) || (rts[end] > nc_min_max[2]) && return :biunstability
        return :biunstability_intermediary_active
    elseif isequal(stabs,[true,false,true])
        (length(nc_min_max)!=2) || (rts[end] > nc_min_max[2]) && return :bistability
        return :bistability_intermediary_active
    elseif isequal(stabs,[false,false,false])
        return :triunstability
    else
        return :unknown_fp_type
    end
end
function deterministic_fps_type(p::Array{Float64})
    rts = system_zeros(p)
    max_real_eigen_vals = map(r -> maximum(real.(eigen(system_jac([r[1],r[1]],p)).values)), rts)
    stabs = max_real_eigen_vals .< 0
    nc_min_max = nc_local_min_max(p)
    return deterministic_fps_type(stabs,nc_min_max,rts)
end

# Generates the values corresponding to the steady state distribution of a system instance.
function get_ss_distribution_vals(p,rts,default)
    sols_data = Vector{NamedTuple{(:seed, :maxval, :retcode, :end_val),Tuple{UInt64,Float64,Symbol,Float64}}}()
    default && return (sols_data,[],[])

    sol = monte(p,(0.,11000.),1,u0=1.05 .* [rts[1],rts[1],rts[1],rts[1]],saveat=.1)[1]
    max_val = maximum(abs.(first.(sol.u)))
    push!(sols_data,(seed=sol.seed,maxval=max_val,retcode=sol.retcode,end_val=sol.t[end]))
    for repeat = 1:20
        (sol.retcode == :Success) && (max_val < 10*(1. + p[4])) && break
        sol_new = monte(p,(0.,11000.),1,u0=1.05 .* [rts[1],rts[1],rts[1],rts[1]],saveat=.1)[1]

        max_val = maximum(abs.(first.(sol_new.u)))
        push!(sols_data,(seed=sol_new.seed,maxval=max_val,retcode=sol_new.retcode,end_val=sol_new.t[end]))
        (sol_new.t[end] > sol.t[end]) && (max_val < 10*(1. + p[4])) && (sol = sol_new)
    end

    if max_val >= 10*(1. + p[4])
        @warn "When estimating the distribution, the resulting solution showed signs of instability, without erroring (reached very high value), p = $(p), seed=$(sol.seed)."
        vals1,vals2 = ([],[])
    elseif length(sol.t) < 7500.
        @warn "When estimating the distribution, a very short distribution were generated, p = $(p), seed=$(sol.seed)."            
        if length(sol.t) < 500.
            vals1,vals2 = ([],[])
        elseif length(sol.t) < 5000.
            vals1,vals2 = (first.(sol.u),last.(sol.u))
        else
            vals1,vals2 = (first.(sol.u[2500:end]),last.(sol.u[2500:end]))
        end
    else
        vals1,vals2 = (first.(sol.u[10000:end]),last.(sol.u[10000:end]))
    end
    return (sols_data,vals1,vals2)
end


### Determine Behaviour ###

# Finds the behaviour for the given input data.
function find_behaviour(p,nc_min_max,l,ssd,fpd,td,is_default,default;instant_activation_limit=15.,perm_max_power_thres=10.,sap_perm_max_power_thres=500.0,passage_fraction=0.85)
    is_default && return default
    any(isnothing.([ssd,fpd,td])) && return :unknown_behaviour
    (length(nc_min_max)!=2) && return (fpd.fps_type==:inactive ? :no_activation : :homogeneous_activation)

    initial_pulse_activation_type = passage_times_evaluation(td.initial_pulse_activation_times,200.,p,instant_activation_limit=instant_activation_limit,passage_fraction=passage_fraction)
    initial_activation_type = passage_times_evaluation(td.initial_activation_times,l,p,instant_activation_limit=instant_activation_limit,passage_fraction=passage_fraction)
    deactivation_type = passage_times_evaluation(td.deactivation_times,l,p,instant_activation_limit=instant_activation_limit,passage_fraction=passage_fraction)
    reactivation_type = passage_times_evaluation(td.reactivation_times,l,p,instant_activation_limit=instant_activation_limit,passage_fraction=passage_fraction)

    if fpd.fps_type == :inactive
        if any(reactivation_type .== [:instantaneous,:instantaneous_delayed])
            return ((ssd.perm_max_power < perm_max_power_thres) ? :homogeneous_intermediate_activation : :oscillation)
        elseif reactivation_type == :heterogeneous
            return :stochastic_pulsing
        elseif reactivation_type == :infinite 
            return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
        end
    elseif fpd.fps_type == :intermediary
        return ((ssd.perm_max_power < perm_max_power_thres) ? :homogeneous_intermediate_activation : :oscillation)
    elseif fpd.fps_type == :active
        if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
            return ((ssd.perm_max_power < perm_max_power_thres) ? :homogeneous_intermediate_activation : :oscillation)
        elseif deactivation_type == :heterogeneous
            return ((ssd.perm_max_power < sap_perm_max_power_thres) ? :stochastic_anti_pulsing : :oscillation) 
        elseif deactivation_type == :infinite 
            return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :homogeneous_activation : :heterogeneous_activation)
        end
    elseif fpd.fps_type == :bistability
        if any(reactivation_type .== [:instantaneous,:instantaneous_delayed])
            if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
                return :oscillation
            elseif deactivation_type == :heterogeneous
                (ssd.median_activity < nc_min_max[2]) && return ((ssd.perm_max_power < perm_max_power_thres) ? :homogeneous_intermediate_activation : :oscillation)
                return ((ssd.perm_max_power < sap_perm_max_power_thres) ? :stochastic_anti_pulsing : :oscillation) 
            elseif deactivation_type == :infinite 
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :homogeneous_activation : :heterogeneous_activation)
            end
        elseif reactivation_type == :heterogeneous
            if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
                (ssd.median_activity > nc_min_max[1]) && return ((ssd.perm_max_power < perm_max_power_thres) ? :homogeneous_intermediate_activation : :oscillation)
                return :stochastic_pulsing
            elseif deactivation_type == :heterogeneous
                return :stochastic_switching
            elseif deactivation_type == :infinite 
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :homogeneous_activation : :heterogeneous_activation)
            end
        elseif reactivation_type == :infinite 
            if any(deactivation_type .== [:instantaneous,:instantaneous_delayed])
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
            elseif deactivation_type == :heterogeneous
                return (any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed]) ? :single_response_pulse : :no_activation)
            elseif deactivation_type == :infinite 
                if any(initial_pulse_activation_type .== [:instantaneous,:instantaneous_delayed])
                    return :homogeneous_activation 
                elseif initial_pulse_activation_type == :heterogeneous
                    return :heterogeneous_activation
                elseif initial_pulse_activation_type == :infinite 
                    return :stable_bistability                     
                end
            end
        end
    end 
    @warn "The behaviour evaluator could not classify it as a known behaviour for p=$(p)."
    return :unknown_behaviour
end

function find_behaviour(pe::ParameterEvaluation;instant_activation_limit=15.,perm_max_power_thres=10.,sap_perm_max_power_thres=500.0,passage_fraction=0.85)
    return find_behaviour(pe.p,pe.nc_min_max,pe.l,pe.steady_state_distribution_data,pe.fp_data,pe.transition_data,pe.is_default,pe.default;instant_activation_limit=instant_activation_limit,perm_max_power_thres=perm_max_power_thres,sap_perm_max_power_thres=sap_perm_max_power_thres,passage_fraction=passage_fraction)
end


### Passage Time Classification ###

# Gets the times for the system, from a give initial condition, pass a threshold.
function generate_passage_times(p,u0,thres,m,l)
    sols = monte(p,(0.,l),m,u0=u0,eSolver=EnsembleThreads(),callbacks=(terminate_sim(u0,thres),),saveat=.1)
    return last.(getfield.(filter(sol -> (sol.retcode != :MaxIters), sols[:]),:t))
end

# Classifies a set of activation times as Instantaneous, Delayed Constant, Heterogeneous, or Infinite (N/A is also a type, but not assigned here).
function passage_times_evaluation(passage_times,l,p;instant_activation_limit=25.,passage_fraction=0.85)
    all(passage_times .== 0.) && return :NA
    instant_activation_limit = instant_activation_limit
    (count(passage_times.==l)>=passage_fraction*length(passage_times)) && return :infinite
    (mean(passage_times)<instant_activation_limit) && return :instantaneous
    (std(passage_times)<(instant_activation_limit/2)) && (mean(passage_times)<4*instant_activation_limit) && return :instantaneous_delayed
    return :heterogeneous
end

# Callback to termiante a simulation when it passes a specific threshold.
function terminate_sim(u0,thres);
    condition = (u0[1]<thres) ? (u,t,integrator)->(u[1]>thres) : (u,t,integrator)->(u[1]<thres)
    affect!(integrator) = terminate!(integrator)
    return DiscreteCallback(condition,affect!,save_positions = (true,true))
end
