### Generates Grid Sturctures ###

# A sturcture  to contain an evaluation grid, and the information within it. 
mutable struct EvaluationGrid
    behaviours::Matrix{Symbol}
    evaluations_condensed::Matrix{Union{Nothing,CondensedEvaluation}}
    evaluations_full::Matrix{Union{Nothing,ParameterEvaluation}}
    S_grid::Array{Float64,1}
    D_grid::Array{Float64,1}
    params::Array{Float64,1}
    color_grid_original::Array{RGB{Float64},2}
    color_grid_current::Array{RGB{Float64},2}
    file_existed::Bool

    function EvaluationGrid(parameters,S_grid,D_grid,dataset_tag;load_condensed=false,load_full=false,is_local=false,inter3=true)
        (!load_condensed) && load_full && (@warn "When full date is loaded, some function will also assume that condensed data is also loaded.")
        τ,v0,n,η = get_τ_v0_n_η(parameters)
        filename_b = get_path(is_local,inter3)*"Data/$(dataset_tag)/DeterminedBehaviour/behaviours_v0_$(v0)_n_$(n)_η_$(η)/grid_τ_$(τ)_v0_$(v0)_n_$(n)_η_$(η).jls"
        filename_ec = get_path(is_local,inter3)*"Data/$(dataset_tag)/CondensedEvaluation/behaviours_v0_$(v0)_n_$(n)_η_$(η)/grid_τ_$(τ)_v0_$(v0)_n_$(n)_η_$(η).jls"
        filename_ef = get_path(is_local,inter3)*"Data/$(dataset_tag)/FullEvaluation/behaviours_v0_$(v0)_n_$(n)_η_$(η)/grid_τ_$(τ)_v0_$(v0)_n_$(n)_η_$(η).jls"
        if isfile(filename_b)
            (!isfile(filename_ec) || !isfile(filename_ef)) && error("Some evaluation files were created, and some weren't.")
            behaviours = deserialize(filename_b)
            evaluations_condensed = (load_condensed ? deserialize(filename_ec) : fill(nothing,length(S_grid),length(D_grid)))
            evaluations_full = (load_full ? deserialize(filename_ef) : fill(nothing,length(S_grid),length(D_grid)))
        else
            behaviours = fill(:empty_behaviour,length(S_grid),length(D_grid))
            evaluations_condensed = fill(nothing,length(S_grid),length(D_grid))
            evaluations_full = fill(nothing,length(S_grid),length(D_grid))
        end
        new(behaviours,evaluations_condensed,evaluations_full,S_grid,D_grid,[τ,v0,n,η],get_color.(behaviours),get_color.(behaviours),isfile(filename_b))
    end
end


# A sturcture  to contain an evaluation grid, and the information within it. 
mutable struct EvaluationGrids
    evaluation_grids::Matrix{EvaluationGrid}
    parameters::Vector{Float64}
    par1::Symbol
    par2::Symbol
    vals1::Vector{Float64}
    vals2::Vector{Float64}

    function EvaluationGrids(parameters,S_grid,D_grid,dataset_tag;load_condensed=false,load_full=false,is_local=false,τ_values=[parameters[1]],v0_values=[parameters[2]],n_values=[parameters[3]],η_values=[parameters[4]])
        lengths = length.([τ_values,v0_values,n_values,η_values])
        (count(lengths .> 1) != 2) && error("Weird input vectors given")
        idx1 = findfirst(lengths .> 1)
        idx2 = findlast(lengths .> 1)
        
        evaluation_grids = Matrix{EvaluationGrid}(undef,lengths[idx1],lengths[idx2])
        for (τi,τ) in enumerate(τ_values), (v0i,v0) in enumerate(v0_values), (ni,n) in enumerate(n_values), (ηi,η) in enumerate(η_values)
            evaluation_grids[[τi,v0i,ni,ηi][idx1],[τi,v0i,ni,ηi][idx2]] = EvaluationGrid([τ,v0,n,η],S_grid,D_grid,dataset_tag,load_condensed=load_condensed,load_full=load_full,is_local=is_local)
        end
        return new(evaluation_grids,parameters,[:τ,:v0,:n,:η][idx1],[:τ,:v0,:n,:η][idx2],[τ_values,v0_values,n_values,η_values][idx1],[τ_values,v0_values,n_values,η_values][idx2])
    end
end

### Grid Coloring Functions ###

recolor_grids!(egs::EvaluationGrids,coloring_function;color_original=false) = foreach(eg->recolor_grid!(eg,coloring_function;color_original=color_original), egs.evaluation_grids)
function recolor_grid!(eg::EvaluationGrid,coloring_function;color_original=false)
    for  i = 1:length(eg.S_grid), j = 1:length(eg.S_grid)
        eg.color_grid_current[i,j] = coloring_function([eg.S_grid[i],eg.D_grid[j],eg.params...],eg.behaviours[i,j],eg.evaluations_condensed[i,j],eg.evaluations_full[i,j])
    end
    color_original && (eg.color_grid_original = deepcopy(eg.color_grid_current));    
end

# The basic function
function basic_grid_coloring(parameters::Vector{Float64},behaviour::Symbol,evaluation_condensed::Union{Nothing,CondensedEvaluation},evaluations_full::Union{Nothing,ParameterEvaluation})
    return get_color(behaviour)
end
# The basic function
function reduced_base(i;instant_activation_limit=15.,perm_max_power_thres=10.,passage_fraction=0.85)
    return (p,b,ec,ef) -> get_color(find_behaviour(p,ef.nc_min_max,ef.l,(!isnothing(ef.steady_state_distribution_reduced_data)&&length(ef.steady_state_distribution_reduced_data)>=i) ? ef.steady_state_distribution_reduced_data[i] : nothing,ef.fp_data,ef.transition_data,ef.is_default,ef.default,instant_activation_limit=instant_activation_limit,perm_max_power_thres=perm_max_power_thres,passage_fraction=passage_fraction))
end
# Returns the base function, but with new selection of parameter values.
function modified_base(instant_activation_limit,perm_max_power_thres,passage_fraction=0.85)
    return (p,b,ec,ef) -> get_color(find_behaviour(p,ef.nc_min_max,ef.l,ef.steady_state_distribution_data,ef.fp_data,ef.transition_data,ef.is_default,ef.default,instant_activation_limit=instant_activation_limit,perm_max_power_thres=perm_max_power_thres,passage_fraction=passage_fraction))
end
# Returns the base function, but with new selection of parameter values.
function condensed_modified_base(instant_activation_limit,perm_max_power_thres,passage_fraction)
    return (p,b,ec,ef) -> get_color(find_behaviour(p,b,ec,instant_activation_limit=instant_activation_limit,perm_max_power_thres=perm_max_power_thres,passage_fraction=passage_fraction))
end
# Corrects a coding error in evauation 10.
function ev10_correction(parameters::Vector{Float64},behaviour::Symbol,evaluation_condensed::Union{Nothing,CondensedEvaluation},evaluations_full::Union{Nothing,ParameterEvaluation})
    (behaviour==:oscillation) && (evaluation_condensed.perm_max_power<10.) && return get_color(:homogeneous_intermediate_activation)
    return get_color(behaviour)
end


###  Grid Coloring Plotting ###

# Plots a single evaluation grid.
function plot_evaluation_grid(evaluation_grid::EvaluationGrid;start_s_slice=1,plot_original_grid=false,smooth=false,idx_axis=true,xticks=nothing,yticks=nothing,set_param_title=false,markers=[],marker_color=RGB{Float64}(0.,0.,0.),title="",xguide="",yguide="",kwargs...)
    set_param_title && (title="τ=$(evaluation_grid.params[1]), v0=$(evaluation_grid.params[2]), n=$(evaluation_grid.params[3]), η=$(evaluation_grid.params[4])")
    if idx_axis 
        (xticks===nothing) && (xticks = 10:20:length(D_grid))
        (yticks===nothing) && (yticks = 10:20:length(S_grid[start_s_slice:end]))
    else
        (xticks===nothing) && (xticks = (range(1, length(D_grid), length = 9), map(i->i[1:3],string.(10 .^(range(-1,stop=2,length=9))))))
        (yticks===nothing) && (yticks = (range(1, length(S_grid[start_s_slice:end]), length = 6), map(i->i[1:3],string.(10 .^(range(log10(S_grid[start_s_slice]),stop=2,length=6))))))
    end
    plot_grid = (plot_original_grid ? evaluation_grid.color_grid_original[start_s_slice:end,1:end] : evaluation_grid.color_grid_current[start_s_slice:end,1:end] )
    p = plot((smooth ? smooth_colors(plot_grid) : plot_grid),yflip=false,aspect_ratio=:none,framestyle=:box,title=title,xguide=xguide,yguide=yguide,bottom_margin=3mm,left_margin=15mm,xticks=xticks,yticks=yticks,kwargs...)
    foreach(pos -> (p  = scatter!((pos[2],pos[1]),label="",color=marker_color)), markers)
    return p
end

# Plots a grid of evaluation grids.
function plot_evaluation_grids(egs::EvaluationGrids;plot_size=(600,400),start_s_slice=1,plot_original_grid=false,smooth=false,idx_axis=true,xticks=[],yticks=[],set_param_title=false,set_param_axes=true,markers=[],marker_color=RGB{Float64}(0.,0.,0.),title="",xguide="",yguide="",kwargs...)
    plots = map(eg -> plot_evaluation_grid(eg,start_s_slice=start_s_slice,plot_original_grid=plot_original_grid,smooth=smooth,idx_axis=idx_axis,xticks=xticks,yticks=yticks,set_param_title=set_param_title,markers=markers,marker_color=marker_color,title=title,xguide=xguide,yguide=yguide), egs.evaluation_grids)
    set_param_axes && foreach(i -> plots[i,1]=plot!(plots[i,1],title="$(egs.par1) = $(egs.vals1[i])"),1:size(plots)[1])
    set_param_axes && foreach(i -> plots[1,i]=plot!(plots[1,i],yguide="$(egs.par2) = $(egs.vals2[i])"),1:size(plots)[2])
    plot(plots...,size=plot_size,guidefontsize=13,titlefontsize=13,layout=reverse(size(plots)))
end



### Modifies Evaluation Grids ###

mark_behaviour!(egs::EvaluationGrids,args...;kwargs...) = foreach(eg->mark_behaviour!(eg,args...;kwargs...), egs.evaluation_grids)
mark_behaviours!(egs::EvaluationGrids,args...;kwargs...) = foreach(eg->mark_behaviours!(eg,args...;kwargs...), egs.evaluation_grids)
function mark_behaviour!(eg::EvaluationGrid,f::Function;use_full=false,use_p=false,mark_color=RGB{Float64}(0.,1.,1.))
    !eg.file_existed && return
    for i = 1:length(S_grid), j = 1:length(D_grid)
        if use_p
            f([eg.S_grid[i],eg.S_grid[j],eg.params...]) && (eg.color_grid_current[i,j] = mark_color)
        elseif !use_full
            !(isnothing(eg.evaluations_condensed[i,j]) || isnothing(eg.evaluations_condensed[i,j].median_activity)) && f(eg.evaluations_condensed[i,j]) && (eg.color_grid_current[i,j] = mark_color)
        else
             (!isnothing(eg.evaluations_full[i,j])) && (!eg.evaluations_full[i,j].is_default) && f(eg.evaluations_full[i,j]) && (eg.color_grid_current[i,j] = mark_color)
        end
    end
end
function mark_behaviours!(eg::EvaluationGrid,f1::Function,f2::Function; use_full=false, use_p=false, mark_color1=RGB{Float64}(0.,1.,1.), mark_color2=RGB{Float64}(0.,0.66,1.), mark_color12=RGB{Float64}(0.,1.,0.4))
    !eg.file_existed && return
    for i = 1:length(S_grid), j = 1:length(D_grid)
        if use_p
            f1([eg.S_grid[i],eg.S_grid[j],eg.params...]) && !f2([eg.S_grid[i],eg.S_grid[j],eg.params...]) && (eg.color_grid_current[i,j] = mark_color1)
            !f1([eg.S_grid[i],eg.S_grid[j],eg.params...]) && f2([eg.S_grid[i],eg.S_grid[j],eg.params...]) && (eg.color_grid_current[i,j] = mark_color2)
            f1([eg.S_grid[i],eg.S_grid[j],eg.params...]) && f2([eg.S_grid[i],eg.S_grid[j],eg.params...]) && (eg.color_grid_current[i,j] = mark_color12)
        elseif !use_full
            (isnothing(eg.evaluations_condensed[i,j]) || isnothing(eg.evaluations_condensed[i,j].median_activity)) && continue
            f1(eg.evaluations_condensed[i,j]) && !f2(eg.evaluations_condensed[i,j]) && (eg.color_grid_current[i,j] = mark_color1)
            !f1(eg.evaluations_condensed[i,j]) && f2(eg.evaluations_condensed[i,j]) && (eg.color_grid_current[i,j] = mark_color2)
            f1(eg.evaluations_condensed[i,j]) && f2(eg.evaluations_condensed[i,j]) && (eg.color_grid_current[i,j] = mark_color12)
        else
            eg.evaluations_full[i,j].is_default && continue
            f1(eg.evaluations_full[i,j]) && !f2(eg.evaluations_full[i,j]) && (eg.color_grid_current[i,j] = mark_color1)
            !f1(eg.evaluations_full[i,j]) && f2(eg.evaluations_full[i,j]) && (eg.color_grid_current[i,j] = mark_color2)
            f1(eg.evaluations_full[i,j]) && f2(eg.evaluations_full[i,j]) && (eg.color_grid_current[i,j] = mark_color12)
        end
    end
end

# Resets the color grid to the original (useful when marking colors).
reset_colors!(egs::EvaluationGrids;reset_original=true) =  foreach(eg->reset_colors!(eg,reset_original=reset_original), egs.evaluation_grids)
function reset_colors!(eg::EvaluationGrid;reset_original=true)
    reset_original && (eg.color_grid_original = get_color.(eg.behaviours))
    eg.color_grid_current = get_color.(eg.behaviours);
    nothing
end



### Retrives Information ###

# Gets the parameters and evaluation corresponding to certain index. Also plots the position of the parameter in the parameter set.
function get_evaluation(i::Int64,j::Int64,eg::EvaluationGrid;marker_color=:black,display_plot=true,display_info=true,plot_original_grid=true,smooth=false,start_s_slice=1,xticks=nothing,yticks=nothing,idx_axis=true,title="",xguide="",yguide="")
    plot_evaluation_grid(eg;start_s_slice=start_s_slice,plot_original_grid=plot_original_grid,smooth=smooth,idx_axis=idx_axis,xticks=xticks,yticks=yticks,title=title,xguide=xguide,yguide=yguide)
    display_plot && display(scatter!((j,i),color=marker_color,label=""))
    if display_info
        if !isnothing(eg.evaluations_full[start_s_slice-1+i,j]) 
            display_behaviour_summary(eg.evaluations_full[start_s_slice-1+i,j])
        elseif !isnothing(eg.evaluations_condensed[start_s_slice-1+i,j]) 
            display_behaviour_summary(eg.evaluations_condensed[start_s_slice-1+i,j],eg.behaviours[start_s_slice-1+i,j],[eg.S_grid[start_s_slice-1+i],eg.D_grid[j],eg.params...])
        else
            println(eg.behaviours[start_s_slice-1+i,j])
        end
    end
    return (p=[eg.S_grid[start_s_slice-1+i],eg.D_grid[j],eg.params...],condensed=eg.evaluations_condensed[start_s_slice-1+i,j],full=eg.evaluations_full[start_s_slice-1+i,j])
end



### Displays information ###

# Displays some short summary from a parameter evaluation.
function display_behaviour_summary(pe::ParameterEvaluation)
    percentiles = [0.0,0.05,0.10,0.15,0.20,0.25,0.33,0.40,0.41,0.42,0.43,0.44,0.45,0.46,0.47,0.48,0.49,0.5,0.51,0.52,0.53,0.54,0.55,0.56,0.57,0.58,0.59,0.6,0.66,0.75,0.8,0.85,0.9,0.95,1.0]
    target_percentile = 0.55
    perc_idx1 = findfirst(percentiles .== target_percentile)
    perc_idx2 = length(percentiles)-perc_idx1+1
    
    println("Behaviour:                               ",pe.behaviour)
    (pe.fp_data!==nothing) && println("Fixed Point (Det/Estimated):             ",pe.det_fps_type,"    ",pe.fp_data.fps_type)
    println("NC min max:                              ",pe.nc_min_max[1],"    ",pe.nc_min_max[2])
    (pe.steady_state_distribution_data!==nothing) && println("55/50/45 percentile activity:            ",pe.steady_state_distribution_data.percentile_activities[perc_idx1],"    ",pe.steady_state_distribution_data.median_activity,"    ",pe.steady_state_distribution_data.percentile_activities[perc_idx2])
    (pe.steady_state_distribution_data!==nothing) && println("Permogram max power:                     ",pe.steady_state_distribution_data.perm_max_power)
    (pe.transition_data!==nothing) && println("Pulse/Initial/De/Re activation types:    ",pe.transition_data.initial_pulse_activation_type,"    ",pe.transition_data.initial_activation_type,"    ",pe.transition_data.deactivation_type,"    ",pe.transition_data.reactivation_type)
end
function display_behaviour_summary(ce::CondensedEvaluation,b::Symbol,p::Vector{Float64})
    rts = system_zeros(p)
    max_real_eigen_vals = map(r -> maximum(real.(eigen(system_jac([r[1],r[1]],p)).values)), rts)
    stabs = max_real_eigen_vals .< 0
    nc_min_max = nc_local_min_max(p)
    det_fps_type = deterministic_fps_type(stabs,nc_min_max,rts)

    println("Behaviour:                      ",b)
    println("Fixed Point (Det/Estimated):    ",det_fps_type,"    ",ce.fps_type)
    for i = 1:length(ce.fps)
        println("Fixed Point $(i):                  ",ce.fps[i])
    end
    println("\nNC min max:                     ",nc_min_max[1],"    ",nc_min_max[2])
    println("Median activity:                ",ce.median_activity)
    println("Permogram max power:            ",ce.perm_max_power)
    println("Pulse/Initial/De/Re mean times: ",ce.mean_initial_pulse_activation_time,"    ",ce.mean_initial_activation_time,"    ",ce.mean_deactivation_time,"    ",ce.mean_reactivation_time)
    println("Pulse/Initial/De/Re ma reaced:  ",ce.initial_pulse_max_reached,"    ",ce.initial_activation_max_reached,"    ",ce.deactivation_max_reached,"    ",ce.reactivation_max_reached)
end

simulate_n_plot(ev::NamedTuple{(:p, :condensed, :full),Tuple{Array{Float64,1},CondensedEvaluation,ParameterEvaluation}};m=4,l=5000.,pre_l=500.,display_A=false) = simulate_n_plot(ev.p;m=m,l=l,pre_l=pre_l,display_A=display_A)
simulate_n_plot(ev::NamedTuple{(:p, :condensed, :full),Tuple{Array{Float64,1},CondensedEvaluation,Nothing}};m=4,l=5000.,pre_l=500.,display_A=false) = simulate_n_plot(ev.p;m=m,l=l,pre_l=pre_l,display_A=display_A)
simulate_n_plot(ev::NamedTuple{(:p, :condensed, :full),Tuple{Array{Float64,1},Nothing,ParameterEvaluation}};m=4,l=5000.,pre_l=500.,display_A=false) = simulate_n_plot(ev.p;m=m,l=l,pre_l=pre_l,display_A=display_A)
simulate_n_plot(ev::NamedTuple{(:p, :condensed, :full),Tuple{Array{Float64,1},Nothing,Nothing}};m=4,l=5000.,pre_l=500.,display_A=false) = simulate_n_plot(ev.p;m=m,l=l,pre_l=pre_l,display_A=display_A)
function simulate_n_plot(p::Vector{Float64};m=4,l=5000.,pre_l=500.,display_A=false)
    nc_min_max = nc_local_min_max(p)
    p_start = setindex!(copy(p),0.,1); S_step = p[1];
    sols = monte(p_start,(0.,l+pre_l),5,u0=[p[4],p[4]],callbacks=(stress_step_cb(pre_l,S_step),),saveat=1.,tstops=[pre_l]);
    plot(sols,vars=[1],linealpha=0.4,framestyle=:box,grid=false,left_margin=3mm,right_margin=3mm,bottom_margin=3mm,tickfontsize=8,guidefontsize=10,xguide="Time",yguide="[σ]",label="")
    p_behaviour_1 = plot_vertical_line!(500,1.35)
    plot(sols[1],vars=(display_A ? [1,2] : [1]),framestyle=:box,grid=false,linealpha=0.8,left_margin=3mm,right_margin=3mm,bottom_margin=3mm,tickfontsize=8,guidefontsize=10,xguide="Time",yguide=(display_A ? "Concentration" : "[σ]"),label=(display_A ? ["σ" "A"] : ""),color=(display_A ? [:green :purple] : :green))
    plot!([0.,l+pre_l],[nc_min_max[1],nc_min_max[1]],linestyle=:dot,color=:black,lw=1,linealpha=0.75,label="")
    plot!([0.,l+pre_l],[nc_min_max[2],nc_min_max[2]],linestyle=:dot,color=:black,lw=1,linealpha=0.75,label="")
    p_behaviour_2 = plot_vertical_line!(pre_l,1.35)
    plot(sols[1],vars=(1,2),framestyle=:box,grid=false,color=[fill(:lightgreen,Int64(pre_l)+1)...,fill(:darkgreen,Int64(l)+1)...],linealpha=0.1,left_margin=3mm,right_margin=3mm,bottom_margin=3mm,tickfontsize=8,guidefontsize=10,label="")
    p_behaviour_3 = make_nullcline!(p)
    behaviour_plot = plot(p_behaviour_1,p_behaviour_2,p_behaviour_3,bottom_margin=5mm,size=(1300,300),layout=(1,3))
end



### Log File Scanning ###

# Stores the data from a log file.
struct LogFile
    completed::Bool
    has_warn::Bool
    has_special_warn::Bool
    has_error::Bool
    timed_out::Bool
    nMaxIters::Int64
    warns::Dict{String,Int64}
    error_opening_line::Union{Nothing,String}
    error::Union{Nothing,String}
    p::Union{Nothing,Float64}
    runtime::Union{Nothing,Float64}
    other_messages::Vector{String}
    iteration::Union{Nothing,Int64}

    function LogFile(filename)
        f = open(filename, "r")
        lines = readlines(f)
        find1 = findfirst("parameter_scan_",filename)
        find2 = findfirst(".log",filename)
        if (find1==nothing) || (find2==nothing)
            iteration = nothing
        else
            iteration = parse(Int64,filename[find1[end]+1:find2[1]-1])
        end

        nMaxIters = 0
        timed_out = false
        has_special_warn = false
        has_error = false
        completed = false
        error_opening_line = nothing
        error = nothing
        warns = Dict{String,Int64}()
        other_messages = Vector{String}()
        p,runtime = (nothing,nothing)
        for line in lines            
            ll = length(line)

            (error!=nothing) && (error = error*"\n"*line)

            (ll>15) && (line[1:15] == "Starts run for ") && continue
            (ll==81) && (line=="└ @ DiffEqBase ~/.julia/packages/DiffEqBase/3iigH/src/integrator_interface.jl:329") && continue
            if (length(line)>17) && (line[1:17] == "Finished run for ")
                find1 = findfirst(" : [",line)
                find2 = findfirst("]. Runtime: ",line)
                if (find1!=nothing) && (find2!=nothing)
                    parse.(Float64,split(line[find1[end]+1:find2[1]-1],","))
                    runtime = parse(Float64,line[find2[end]+1:end])      
                    completed = true         
                end
                continue
            end

            if (ll==50) && (line=="┌ Warning: Interrupted. Larger maxiters is needed.")
                nMaxIters += 1
                continue
            end
            if occursin("slurmstepd: error: *** JOB",line) && occursin(" CANCELLED AT ",line) && occursin(" DUE TO TIME LIMIT ",line)
                timed_out = true
                continue
            end
            if occursin("ERROR: ",line) || occursin("LoadError:",line) || occursin(" MethodError:",line)
                has_error = true
                error_opening_line = line
                error = line
                continue
            end
            if occursin("┌ Warning: ",line)
                has_special_warn = true
                if haskey(warns,line)
                    warns[line] += 1
                else
                    warns[line] = 1
                end
                continue
            end
            (error==nothing) && push!(other_messages,line)
        end
        has_warn = (has_special_warn || nMaxIters>0)

        close(f)
        new(completed,has_warn,has_special_warn,has_error,timed_out,nMaxIters::Int64,warns,error_opening_line,error,p,runtime,other_messages,iteration)
    end
end

# Loads a log file from desigante paraemters and run input.
function load_logfile(parameters,dataset_tag,parameter_sets;is_local=false,inter3=true)
    iteration = findfirst(map(i -> parameters==parameter_sets[i], 1:length(parameter_sets)))
    return LogFile(get_path(is_local,inter3)*"/Logs/$(dataset_tag)/parameter_scan_$(iteration).log")
end

# Gets an array of LogFiles.
function get_logfiles(foldername)
    return map(filename -> LogFile(foldername*filename), readdir(foldername))
end

struct LogFilesSummary
    nbr_files::Int64
    nbr_completed::Int64
    nbr_has_warn::Int64
    nbr_has_special_warn::Int64
    nbr_has_error::Int64
    nbr_timed_out::Int64
    nMaxIters_counts::Vector{Int64}
    all_warns::Dict{String,Int64}
    error_openers::Dict{String,Int64}
    all_errors::Vector{String}
    runtimes::Vector{Float64}
    other_messages::Vector{String}

    function LogFilesSummary(foldername)
        lfs = get_logfiles(foldername)
        nbr_files = length(lfs)
        nbr_completed = count(getfield.(lfs,:completed))
        nbr_has_warn = count(getfield.(lfs,:has_warn))
        nbr_has_special_warn = count(getfield.(lfs,:has_special_warn))
        nbr_has_error = count(getfield.(lfs,:has_error))
        nbr_timed_out = count(getfield.(lfs,:timed_out))
        nMaxIters_counts = sort(getfield.(lfs,:nMaxIters))

        all_warns = Dict{String,Int64}()
        all_errors = Vector{String}()
        error_openers = Dict{String,Int64}()
        runtimes = Vector{Float64}()
        other_messages = Vector{String}()
        for lf in lfs
            (lf.runtime!=nothing) && push!(runtimes,lf.runtime)
            foreach(om ->push!(other_messages,om), lf.other_messages)
            if lf.error_opening_line != nothing
                if haskey(error_openers,lf.error_opening_line)
                    error_openers[lf.error_opening_line] += 1
                else
                    error_openers[lf.error_opening_line] = 1
                end
            end
            (lf.error!=nothing) && push!(all_errors,lf.error)
            for key in keys(lf.warns)
                if haskey(all_warns,key)
                    all_warns[key] += lf.warns[key]
                else
                    all_warns[key] = lf.warns[key]
                end
            end
        end
        new(nbr_files,nbr_completed,nbr_has_warn,nbr_has_special_warn,nbr_has_error,nbr_timed_out,nMaxIters_counts,all_warns,error_openers,all_errors,runtimes,other_messages)
    end
end

# Prints the content in a log file.
function print_logfile(lf::LogFile)
    println("File Complete: $(lf.completed)") 
    (lf.runtime!=nothing) && println("Runtime: $(lf.runtime)s")
    (lf.iteration!=nothing) && println("Run iteration: $(lf.iteration)")
    (lf.p!=nothing) && println("[τ,v0,n,η] = $(lf.p)")
    if lf.nMaxIters == 0
        println("MaxIters Warning Encountered: false") 
    else
        println("MaxIters Warning Encountered  true") 
        println("Number of MaxIters Enocuntered: $(lf.nMaxIters)") 
    end
    println("Special Warning Encountered: $(lf.has_special_warn)") 
    if length(lf.warns) != 0
        println("\nNon Maxiter warnings:")
        for key in keys(lf.warns)
            println(lf.warns[key])
            println(key)
        end
    end
    println("HPC Temined Out: $(lf.timed_out)") 
    println("Error Encountered: $(lf.has_error)") 
    if (lf.error!=nothing) 
        println("\nError Encountered:")
        println(lf.error)
    end
    if length(lf.other_messages) != 0
        println("\nOther Messages:")
        for om in lf.other_messages
            println(om)
        end
    end
end

# Prints the content in a log file summary.
function print_logfilesummary(lfs::LogFilesSummary)
    println("Number of runs: $(lfs.nbr_files)")
    println("Number of completed runs: $(lfs.nbr_completed)")
    println("Number of runs with MaxIters warnings: $(count(lfs.nMaxIters_counts .!= 0))")
    println("Number of runs with othe warnings: $(lfs.nbr_has_special_warn)")
    println("Number of runs that errored: $(lfs.nbr_has_error)")
    println("Number of runs which timed out: $(lfs.nbr_timed_out)")
    println("Total number of MaxIters warnings: $(sum(lfs.nMaxIters_counts))")
    println("Average runtime: $(mean(lfs.runtimes))")
    if !isempty(lfs.error_openers)
        println("\nError message openers:")
        for key in keys(lfs.error_openers)
            println("Occurences: $(lfs.error_openers[key])")
            println(key)
        end
    end
    if !isempty(lfs.all_warns)
        println("\nNon-MaxIters warnings:")
        for key in keys(lfs.all_warns)
            println("Occurences: $(lfs.all_warns[key])")
            println(key)
        end
    end
end


### File utility
function get_path(is_local,inter3)
    is_local && return "../"
    inter3 && return "../../../../../../../mount/hpc_uni/projects/parameter_space_scan_3_intermediaries/"
    return "../../../../../../../mount/hpc_uni/projects/parameter_space_scan/"
end