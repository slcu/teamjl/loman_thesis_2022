### Package Imports ###
using StochasticDiffEq
using Polynomials

### Declares Constants ###
const BehaviourClass = NamedTuple{(:fp_type, :initial_activation_type, :deactivation_type, :reactivation_type),NTuple{4,Symbol}}

### Model Declaration ###
function sys_f(du,u,p,t)
    σ,A1,A2,A3 = u
    S,D,τ,v0,n,η = p
    du[1] = v0+(S*σ)^n/((S*σ)^n+(D*A3)^n+1) - σ
    du[2] = (1/τ)*(σ-A1)
    du[3] = (1/τ)*(A1-A2)
    du[4] = (1/τ)*(A2-A3)
end
function sys_g(du,u,p,t)
    σ,A1,A2,A3 = u
    S,D,τ,v0,n,η = p
    du[1,1] = η*sqrt(max(v0+(S*σ)^n/((S*σ)^n+(D*A3)^n+1),0.))
    du[1,2] = -η*sqrt(max(σ,0.))
    du[1,3] = 0
    du[1,4] = 0
    du[1,5] = 0
    du[1,6] = 0
    du[1,7] = 0
    du[1,8] = 0
    du[2,1] = 0
    du[2,2] = 0
    du[2,3] = η*sqrt(max((1/τ)*σ,0.))
    du[2,4] = -η*sqrt(max((1/τ)*A1,0.))
    du[2,5] = 0
    du[2,6] = 0
    du[2,7] = 0
    du[2,8] = 0
    du[3,1] = 0
    du[3,2] = 0
    du[3,3] = 0
    du[3,4] = 0
    du[3,5] = η*sqrt(max((1/τ)*A1,0.))
    du[3,6] = -η*sqrt(max((1/τ)*A2,0.))
    du[3,7] = 0
    du[3,8] = 0
    du[4,1] = 0
    du[4,2] = 0
    du[4,3] = 0
    du[4,4] = 0
    du[4,5] = 0
    du[4,6] = 0
    du[4,7] = η*sqrt(max((1/τ)*A2,0.))
    du[4,8] = -η*sqrt(max((1/τ)*A3,0.))
end

### System Properties ###

# The system jacobian.
function system_jac(x,p)
    σ,A1,A2,A3 = x; S,I,τ,v0,n = p;
    J11 = ((n * (S * σ) ^ n) / ((1 + (I * A3) ^ n + (S * σ) ^ n) * σ) - (n * (S * σ) ^ (2n)) / ((1 + (I * A3) ^ n + (S * σ) ^ n) ^ 2 * σ)) - 1.0
    J14 = (-n * (S * σ) ^ n * (I * A3) ^ n) / (A3 * (1 + (I * A3) ^ n + (S * σ) ^ n) ^ 2)
    J21 = (1/τ)
    J22 = -(1/τ)
    J32 = (1/τ)
    J33 = -(1/τ)
    J43 = (1/τ)
    J44 = -(1/τ)
    return [J11 0 0 J14; J21 J22 0 0; 0 J32 J33 0; 0 0 J43 J44]
end

# Finds the zeros of the system at a specific parameter value.
function system_zeros(p)
    S,D,τ,v0,n = p
    coefficients = zeros(Int64(n)+2)
    coefficients[Int64(n)+2] = -S^n-D^n
    coefficients[Int64(n)+1] = v0*(S^n+D^n)+S^n
    coefficients[2] = -1
    coefficients[1] = v0
    return sort(real.(filter(x->(imag(x)==0)&&real(x)>0.,roots(Polynomial(coefficients)))))
end

# Retrives the local maxima and minima of the nullcline.
function nc_local_min_max(p)
    S,D,τ,v0,n = p
    pol = Polynomial([v0+v0^2,1/n-2*v0-1,1])
    return sort(real.(filter(x->(imag(x)==0)&&real(x)>0.,roots(pol))))
end

# For a given parameter set, finds the maximum v0 for which the nullcline have 2 local min/max es.
function nc_2_max_v0(p)
    τ,v0,n,η = get_τ_v0_n_η(p)
    return (n/4)+1/(4n)-0.5
end

# For a given parameter set, given v0>nc_2_max_v0(p), finds the threshold between an active/inactive system.
function large_v0_inactive_active_threshold(p)
    τ,v0,n,η = get_τ_v0_n_η(p)
    return (n/4)+1/(4n)-0.5+(n-1)/(2*n)
end

### Simulation Functions ###

#Makes several stochastic simulations.
function monte(p, tspan, n; init_fp=1, u0=[system_zeros(p)[init_fp],system_zeros(p)[init_fp]], eSolver=EnsembleThreads(), solver=ImplicitEM(), callbacks=(), kwargs...)
    prob = SDEProblem(sys_f,sys_g,u0,tspan,deepcopy(p),noise_rate_prototype=zeros(4,8))
    ensemble_prob = EnsembleProblem(prob,prob_func=(p,i,r)->p)
    sols = solve(ensemble_prob,solver,eSolver;trajectories=n,callback=CallbackSet(positive_domain(),callbacks...), maxiters=5e6, force_dtmin=true, kwargs...)
    foreach(sol ->(sol.retcode == :MaxIters)&&(@warn "Solver hit MaxIters for p=$(p), u0=$(u0), seed=$(sol.seed)"), sols)
    return sols
end


### Callbacks ###

#A callback for keeping a simulation within the positive domain.
function positive_domain()
    condition(u,t,integrator) = minimum(u) < 0
    affect!(integrator) = integrator.u .= integrator.uprev
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end


### Miscellaneous Utility Functions ###

# If element is in dictionary, increment its value one step. Else initialise an entry with value 1.
function up1!(dict,element)
    haskey(dict,element) ? (dict[element]+=1) : (dict[element]=1)
    nothing
end

# For a parameter set, returns the 4 paraemters τ, v0, n, η.
function get_τ_v0_n_η(parameters)
    (length(parameters)==4) && return parameters
    (length(parameters)==6) && return parameters[3:6]
    error("Parameter entry set have weird length (nether 4 nor 6).")
end