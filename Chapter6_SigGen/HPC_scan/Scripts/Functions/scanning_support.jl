### Support Function for Scanning Parameter Space ###

# For a given parameter set, as well as S and D paraemter grids, returns a behaviour matrix with the behaviour classification at every point in parameter space.
function scan_sd_space(S_grid,D_grid,parameters;print_unclassified_behaviours=false,kwargs...)
    (length(parameters) != 4) && error("Provided a parameter array of the wrong size (size should be equal to 4).")
    #behaviour_grid = Matrix{BehaviourClass}(undef,length(S_grid),length(D_grid))
    behaviour_grid = fill((fp_type = :undef, initial_activation_type = :NA, deactivation_type = :NA, reactivation_type = :NA),length(S_grid),length(D_grid))
    unclassfied_behaviour_dict = (print_unclassified_behaviours ? Dict{BehaviourClass,Int64}() : nothing)

    start_idx = 1
    for (Di,D) in enumerate(D_grid)
        scan_upwards!(behaviour_grid,start_idx,S_grid,Di,D,parameters,unclassfied_behaviour_dict,kwargs...)
        scan_downwards!(behaviour_grid,start_idx-1,S_grid,Di,D,parameters,unclassfied_behaviour_dict,kwargs...)
        start_idx = findlast(get_behaviour.(behaviour_grid[:,Di]) .!= :homogeneous_activation)
    end
    if print_unclassified_behaviours && !isempty(unclassfied_behaviour_dict)
        println("Unclassified behaviour types encountered:")
        foreach(key -> println(key,"\t",unclassfied_behaviour_dict[key]), keys(unclassfied_behaviour_dict))
    end
    return behaviour_grid
end

# Scans for increasing index of S, adding the determined behaviours to the behaviour grid.
function scan_upwards!(behaviour_grid,Si_start,S_grid,Di,D,parameters,unclassfied_behaviour_dict,kwargs...)
    consecutive_homogeneous_activation_count = 0
    for (Si,S) in enumerate(S_grid[Si_start:end])
        behaviour_grid[Si+Si_start-1,Di] = determine_behaviour_expanded!([S,D,parameters...],unclassfied_behaviour_dict,kwargs...)
        if get_behaviour(behaviour_grid[Si+Si_start-1,Di]) == :homogeneous_activation
            if (consecutive_homogeneous_activation_count += 1) == 4
                foreach(Sii -> behaviour_grid[Sii,Di] = (fp_type=:auto_homogeneous_activation, initial_activation_type=:NA, deactivation_type=:NA, reactivation_type=:NA), (Si+Si_start-1+1):length(S_grid))
                break
            end
        else
            consecutive_homogeneous_activation_count = 0
        end
    end
end

# Scans for decreasing index of S, adding the determined behaviours to the behaviour grid.
function scan_downwards!(behaviour_grid,Si_start,S_grid,Di,D,parameters,unclassfied_behaviour_dict,kwargs...)
    consecutive_no_activation_count = 0
    for (Si,S) in Iterators.reverse(enumerate(S_grid[1:Si_start]))
        behaviour_grid[Si,Di] = determine_behaviour_expanded!([S,D,parameters...],unclassfied_behaviour_dict,kwargs...)
        if get_behaviour(behaviour_grid[Si,Di]) == :no_activation
            if (consecutive_no_activation_count += 1) == 4
                foreach(Sii -> behaviour_grid[Sii,Di] = (fp_type=:auto_no_activation, initial_activation_type=:NA, deactivation_type=:NA, reactivation_type=:NA), 1:(Si-1))
                break
            end
        else
            consecutive_no_activation_count = 0
        end
    end
end


# Expanded determine behaviour function. Reruns if an unclassfied behaviour is found, as well as (potentially) registers unclassified behaviours).
function determine_behaviour_expanded!(parameters,unclassfied_behaviour_dict;attempts=10,kwargs...)
    behaviour_classification = (fp_type=:NA, initial_activation_type=:NA, deactivation_type=:NA, reactivation_type=:NA)
    for i = 1:attempts
        behaviour_classification = determine_behaviour(parameters,kwargs...)
        (behaviour_classification != :unclassified_behaviour) && (return behaviour_classification)
    end
    (unclassfied_behaviour_dict !== nothing) && up1!(unclassfied_behaviour_dict,behaviour_classification)
    return behaviour_classification
end

### Support Function for Evaluating Parameter Space ###

# For a given parameter set, as well as S and D paraemter grids, returns a behaviour matrix with the behaviour classification at every point in parameter space.
function evaluate_sd_space(S_grid,D_grid,parameters;print_unclassified_behaviours=false,print_Di=false,kwargs...)
    (length(parameters) != 4) && error("Provided a parameter array of the wrong size (size should be equal to 4).")
    evaluation_grid = Matrix{Union{Nothing,ParameterEvaluation}}(nothing,length(S_grid),length(D_grid))
    start_idx = 1
    for (Di,D) in enumerate(D_grid)
        print_Di && println("Starting new iteration: ",Di,"\t",start_idx)
        evaluation_scan_upwards!(evaluation_grid,start_idx,S_grid,Di,D,parameters,kwargs...)
        evaluation_scan_downwards!(evaluation_grid,start_idx-1,S_grid,Di,D,parameters,kwargs...)
        
        start_idx = findlast(map(ev -> (ev.behaviour!=:homogeneous_activation), evaluation_grid[:,Di]))
        if isnothing(start_idx) 
            @warn "Starting index is nothing for Di = $(Di), p = $(parameters)."
            start_idx = length(S_grid)-1
        end
    end
    return Matrix{ParameterEvaluation}(evaluation_grid)
end


# Scans for increasing index of S, adding the determined behaviours to the behaviour grid.
function evaluation_scan_upwards!(evaluation_grid,Si_start,S_grid,Di,D,parameters,kwargs...)
    consecutive_homogeneous_activation_count = 0
    for (Si,S) in enumerate(S_grid[Si_start:end])
        evaluation_grid[Si+Si_start-1,Di] = evaluate_behaviour_expanded([S,D,parameters...],kwargs...)
        if evaluation_grid[Si+Si_start-1,Di].behaviour == :homogeneous_activation
            if (consecutive_homogeneous_activation_count += 1) == 4
                foreach(Sii -> evaluation_grid[Sii,Di] = ParameterEvaluation([S,D,parameters...],default=:homogeneous_activation,kwargs...), (Si+Si_start-1+1):length(S_grid))
                break
            end
        else
            consecutive_homogeneous_activation_count = 0
        end
    end
end

# Scans for decreasing index of S, adding the determined behaviours to the behaviour grid.
function evaluation_scan_downwards!(evaluation_grid,Si_start,S_grid,Di,D,parameters,kwargs...)
    consecutive_no_activation_count = 0
    for (Si,S) in Iterators.reverse(enumerate(S_grid[1:Si_start]))
        evaluation_grid[Si,Di] = evaluate_behaviour_expanded([S,D,parameters...],kwargs...)
        if evaluation_grid[Si,Di].behaviour == :no_activation
            if (consecutive_no_activation_count += 1) == 4
                foreach(Sii -> evaluation_grid[Sii,Di] = ParameterEvaluation([S,D,parameters...],default=:no_activation,kwargs...), 1:(Si-1))
                break
            end
        else
            consecutive_no_activation_count = 0
        end
    end
end


# Expanded determine behaviour function. Reruns if an unclassfied behaviour is found, as well as (potentially) registers unclassified behaviours).
function evaluate_behaviour_expanded(parameters;attempts=10,kwargs...)
    for attempt = 1:(attempts-1)
        behaviour_evaluation = ParameterEvaluation(parameters,kwargs...)
        (behaviour_evaluation.behaviour != :unknown_behaviour) && (return behaviour_evaluation)
    end
    return ParameterEvaluation(parameters,m=20,l=5000.)
end

### Improved SD-space evaluator.

# For a given parameter set, as well as S and D parameter grids, returns a behaviour matrix with the behaviour classification at every point in parameter space.
function evaluate_sd_space_fast(S_grid,D_grid,parameters;print_unclassified_behaviours=false,print_Di=false)
    (length(parameters) != 4) && error("Provided a parameter array of the wrong size (size should be equal to 4).")
    Sl = length(S_grid); Dl = length(D_grid);
    evaluation_grid = Matrix{Union{Nothing,ParameterEvaluation}}(nothing,Sl,Dl)

    # Finds lower bound of behavioural region.
    @time for (Di,D) in enumerate(D_grid)
        for Si in findfirst(evaluation_grid[:,Di] .== nothing):Sl
            S = S_grid[Si]
            evaluation_grid[Si,Di] = evaluate_behaviour_expanded([S,D,parameters...])
            if (evaluation_grid[Si,Di].behaviour == :no_activation)
                evaluation_grid[Si,Di+1:end] .= fill(ParameterEvaluation([S,D,parameters...],default=:no_activation),Dl-Di)
            else
                break
            end
        end
    end

    # Finds upper bound of behavioural region.
    @time for (Di,D) in Iterators.reverse(enumerate(D_grid))
        all(evaluation_grid[:,Di] .!= nothing) && continue
        for Si in findlast(evaluation_grid[:,Di] .== nothing):-1:findfirst(evaluation_grid[:,Di] .== nothing)
            S = S_grid[Si]        
            evaluation_grid[Si,Di] = evaluate_behaviour_expanded([S,D,parameters...])
            if (evaluation_grid[Si,Di].behaviour == :homogeneous_activation)
                evaluation_grid[Si,1:Di-1] .= fill(ParameterEvaluation([S,D,parameters...],default=:homogeneous_activation),Di-1)
            elseif (evaluation_grid[Si,Di].behaviour == :heterogeneous_activation)
                evaluation_grid[Si,1:Di-1] .= fill(ParameterEvaluation([S,D,parameters...],default=:heterogeneous_activation),Di-1)
            else
                break
            end
        end
    end

    # Fills in empty bvehaviours.
    for Di in 1:Dl
        println("First scan D idx: $(Di)")
        @time for Si in (1+mod(Di,2)):2:Sl
            (evaluation_grid[Si,Di]==nothing) && (evaluation_grid[Si,Di] = evaluate_behaviour_expanded([S_grid[Si],D_grid[Di],parameters...]))
        end
    end
    for (Di,D) in enumerate(D_grid)
        println("Second scan D idx: $(Di)")
        @time for  (Si,S) in enumerate(S_grid)
            (evaluation_grid[Si,Di]!=nothing) && continue
            idxs = filter(x->(0<x[1]<=Sl)&&(0<x[2]<=Dl),[[Si,Di-1],[Si-1,Di],[Si,Di+1],[Si+1,Di]])
            if all(map(idx -> evaluation_grid[idx...].behaviour==evaluation_grid[idxs[1]...].behaviour, idxs))
                evaluation_grid[Si,Di] = ParameterEvaluation([S,D,parameters...],default=evaluation_grid[idxs[1]...].behaviour)
            else
                evaluation_grid[Si,Di] = evaluate_behaviour_expanded([S,D,parameters...])
            end
        end
    end

    return Matrix{ParameterEvaluation}(evaluation_grid)
end


### File System Support Function ###

# Checks if a directory exists, if not, creates it.
function check_make_dir(dir_name)
    isdir(dir_name) ? nothing : mkdir(dir_name)
end

### Generate information Subtructures ###

# Gets just the behaviours.
function get_behaviour_grid(evs::Matrix{ParameterEvaluation})
    return getproperty.(evs,:behaviour)
end

# Gets a condensed version of the parameter evaluation.
struct CondensedEvaluation
    median_activity::Union{Nothing,Float64}
    perm_max_power::Union{Nothing,Float64}
    fps::Union{Nothing,Vector{Vector{Float64}}}
    fps_type::Union{Nothing,Symbol}
    mean_initial_pulse_activation_time::Union{Nothing,Float64}
    initial_pulse_max_reached::Union{Nothing,Int64}
    mean_initial_activation_time::Union{Nothing,Float64}
    initial_activation_max_reached::Union{Nothing,Int64}
    mean_deactivation_time::Union{Nothing,Float64}
    deactivation_max_reached::Union{Nothing,Int64}
    mean_reactivation_time::Union{Nothing,Float64}
    reactivation_max_reached::Union{Nothing,Int64}

    function CondensedEvaluation(pe::ParameterEvaluation)
        median_activity = (pe.steady_state_distribution_data!==nothing ?  pe.steady_state_distribution_data.median_activity : nothing)
        perm_max_power = (pe.steady_state_distribution_data!==nothing ? pe.steady_state_distribution_data.perm_max_power : nothing)
        fps = (pe.fp_data!==nothing ? pe.fp_data.fps : nothing)
        fps_type = (pe.fp_data!==nothing ? pe.fp_data.fps_type : nothing)
        mean_initial_pulse_activation_time = (pe.transition_data!==nothing ? mean(pe.transition_data.initial_pulse_activation_times) : nothing)
        initial_pulse_max_reached = (pe.transition_data!==nothing ? count(map(t -> t == pe.l, pe.transition_data.initial_pulse_activation_times)) : nothing)
        mean_initial_activation_time = (pe.transition_data!==nothing ? mean(pe.transition_data.initial_activation_times) : nothing)
        initial_activation_max_reached = (pe.transition_data!==nothing ? count(map(t -> t == pe.l, pe.transition_data.initial_activation_times)) : nothing)
        mean_deactivation_time = (pe.transition_data!==nothing ? mean(pe.transition_data.deactivation_times) : nothing)
        deactivation_max_reached = (pe.transition_data!==nothing ? count(map(t -> t == pe.l, pe.transition_data.deactivation_times)) : nothing)
        mean_reactivation_time = (pe.transition_data!==nothing ? mean(pe.transition_data.reactivation_times) : nothing)
        reactivation_max_reached = (pe.transition_data!==nothing ? count(map(t -> t == pe.l, pe.transition_data.reactivation_times)) : nothing)

        new(median_activity, perm_max_power, fps, fps_type, mean_initial_pulse_activation_time, initial_pulse_max_reached, mean_initial_activation_time, initial_activation_max_reached, mean_deactivation_time, deactivation_max_reached, mean_reactivation_time, reactivation_max_reached)
    end
end
function get_condensed_evaluation_grid(evs::Matrix{ParameterEvaluation})
    return CondensedEvaluation.(evs)
end

