### HPC Scan - Scripts

This folder contains the scripts for running an HPC scan. Required functions are in the "Functions" folder. The "run_scan_hpc.sh" file submits a job to the HPC (this system is adapted to the Cambridge university HPC, HPC systems at other institutions might be set up differently). The HPC receives a job array, which runs the "run_scan_hpc.jl" file several times (each run takes a portion of the input). If 10000 parameter sets are scanned, and a 1000-length job array is used, each job scans 10 parameter sets. Before any scan, run the "pre_run_scan_hpc.jl" scripts with the correct option. It sets up folders, and provides a couple of useful reminders.

This scan simply scans the behaviour of the general sigma factor model across 6-dimensional parameter space.