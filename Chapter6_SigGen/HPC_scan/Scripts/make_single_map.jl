using Pkg
Pkg.activate(".")

### Declares Required Functions ###

# Checks if a directory exists, if not, creates it.
function check_make_dir(dir_name)
    isdir(dir_name) ? nothing : mkdir(dir_name)
end

### Set Run Parameters ###
target_folder = "hpc_evaulation_16"

### Sets Parameters ###
const S_grid = range(0.1,stop=10.0,length=1000)
const D_grid = range(0.1,stop=10.0,length=1000)

p = [5.0,0.05,3.0,0.1]

### Check Required Directories ###
check_make_dir("../Data/$(target_folder)")
check_make_dir("../Data/$(target_folder)/FullEvaluation")
check_make_dir("../Data/$(target_folder)/CondensedEvaluation")
check_make_dir("../Data/$(target_folder)/DeterminedBehaviour")
check_make_dir("../Logs/$(target_folder)")

check_make_dir("../Data/$(target_folder)/FullEvaluation/behaviours_v0_$(p[2])_n_$(p[3])_η_$(p[4])")
check_make_dir("../Data/$(target_folder)/CondensedEvaluation/behaviours_v0_$(p[2])_n_$(p[3])_η_$(p[4])")
check_make_dir("../Data/$(target_folder)/DeterminedBehaviour/behaviours_v0_$(p[2])_n_$(p[3])_η_$(p[4])")

### Fetches Packages ###
using Serialization

### Fetches Files ###
include("Functions/model_base.jl")
include("Functions/behaviour_lists.jl")
include("Functions/determine_behaviour.jl")
include("Functions/evaluate_behaviour.jl")
include("Functions/scanning_support.jl")

### Runs Parameter Scan ###
elapsed_time = @elapsed (evaluation = evaluate_sd_space_fast(S_grid,D_grid,p))
serialize("../Data/$(target_folder)/FullEvaluation/behaviours_v0_$(p[2])_n_$(p[3])_η_$(p[4])/grid_τ_$(p[1])_v0_$(p[2])_n_$(p[3])_η_$(p[4]).jls",evaluation)
serialize("../Data/$(target_folder)/CondensedEvaluation/behaviours_v0_$(p[2])_n_$(p[3])_η_$(p[4])/grid_τ_$(p[1])_v0_$(p[2])_n_$(p[3])_η_$(p[4]).jls",get_condensed_evaluation_grid(evaluation))
serialize("../Data/$(target_folder)/DeterminedBehaviour/behaviours_v0_$(p[2])_n_$(p[3])_η_$(p[4])/grid_τ_$(p[1])_v0_$(p[2])_n_$(p[3])_η_$(p[4]).jls",get_behaviour_grid(evaluation))
println("Finished run for $(p) : [$(p[1]),$(p[2]),$(p[3]),$(p[4])]. Runtime: $(elapsed_time)")