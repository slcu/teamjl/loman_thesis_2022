
### Declares Required Functions ###

# Checks if a directory exists, if not, creates it.
function check_make_dir(dir_name)
    isdir(dir_name) ? nothing : mkdir(dir_name)
end

### Set Run Parameters ###
target_folder = "hpc_evaulation_15"

### Sets Parameters ###
const S_grid = 10 .^(range(-1,stop=2,length=300))
const D_grid = 10 .^(range(-1,stop=2,length=300))
const τ_grid = [0.1,0.15,0.20,0.30,0.50,0.75,1.0,1.5,2.0,3.0,5.0,7.50,10.0,15.0,20.0,30.0,50.0,75.0,100.0]
const v0_grid = [0.01,0.02,0.03,0.05,0.075,0.1,0.15,0.20]
const n_grid = [2.0,3.0,4.0]
const η_grid = [0.001,0.002,0.005,0.01,0.02,0.05,0.1]

parameter_sets = [[τ,v0,n,η] for τ in τ_grid, v0 in v0_grid, n in n_grid, η in η_grid]
println("Number of iterations required: $(prod(size(parameter_sets)))")
println("Remember to change the destination of the output log files.")
println("Remember to change the destination of the output files.")
println("Remember to change the parameters in the real run.")

### Check Required Directories ###
check_make_dir("../Data/$(target_folder)")
check_make_dir("../Data/$(target_folder)/FullEvaluation")
check_make_dir("../Data/$(target_folder)/CondensedEvaluation")
check_make_dir("../Data/$(target_folder)/DeterminedBehaviour")
check_make_dir("../Logs/$(target_folder)")

for v0 in v0_grid, n in n_grid, η in η_grid
    check_make_dir("../Data/$(target_folder)/FullEvaluation/behaviours_v0_$(v0)_n_$(n)_η_$(η)")
    check_make_dir("../Data/$(target_folder)/CondensedEvaluation/behaviours_v0_$(v0)_n_$(n)_η_$(η)")
    check_make_dir("../Data/$(target_folder)/DeterminedBehaviour/behaviours_v0_$(v0)_n_$(n)_η_$(η)")
end
