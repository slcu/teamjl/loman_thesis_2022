### Set Run Parameters ###
target_folder = "hpc_evaulation_15"

### Sets Parameters ###
const S_grid = 10 .^(range(-1,stop=2,length=300))
const D_grid = 10 .^(range(-1,stop=2,length=300))
const τ_grid = [0.1,0.15,0.20,0.30,0.50,0.75,1.0,1.5,2.0,3.0,5.0,7.50,10.0,15.0,20.0,30.0,50.0,75.0,100.0]
const v0_grid = [0.01,0.02,0.03,0.05,0.075,0.1,0.15,0.20]
const n_grid = [2.0,3.0,4.0]
const η_grid = [0.001,0.002,0.005,0.01,0.02,0.05,0.1]
parameter_sets = [[τ,v0,n,η] for τ in τ_grid, v0 in v0_grid, n in n_grid, η in η_grid]

### Fetches Packages ###
using Serialization

### Fetches Files ###
include("Functions/model_base.jl")
include("Functions/behaviour_lists.jl")
include("Functions/determine_behaviour.jl")
include("Functions/evaluate_behaviour.jl")
include("Functions/scanning_support.jl")


### Set origin and target folders.
folder_origin = "../Data/$(target_folder)/FullEvaluation/"
folder_target = "BehaviourMap/"

# Delete files
for p in parameter_sets
    #filename_target = "BehaviourMap/behaviours_v0_$(p[2])_n_$(p[3])_η_$(p[4])/grid_τ_$(p[1])_v0_$(p[2])_n_$(p[3])_η_$(p[4]).jls"
    #isfile(filename_target) && rm(filename_target)
end

# Reclassify files.
for p in parameter_sets
    filename_origin = folder_origin*"behaviours_v0_$(p[2])_n_$(p[3])_η_$(p[4])/grid_τ_$(p[1])_v0_$(p[2])_n_$(p[3])_η_$(p[4]).jls"
    filename_target = folder_target*"behaviours_v0_$(p[2])_n_$(p[3])_η_$(p[4])/grid_τ_$(p[1])_v0_$(p[2])_n_$(p[3])_η_$(p[4]).jls"
    !isfile(filename_origin) && continue
    isfile(filename_target) && continue
    bm_origin = deserialize(filename_origin)
    bm_target = find_behaviour.(bm_origin)
    serialize(filename_target,bm_target)
end


