using Pkg; Pkg.activate(".");

### Initiation ###
println("Starts run for $(ARGS[1])")

### Set Run Parameters ###
target_folder = "hpc_evaulation_15"

### Sets Parameters ###
const S_grid = 10 .^(range(-1,stop=2,length=300))
const D_grid = 10 .^(range(-1,stop=2,length=300))
const τ_grid = [0.1,0.15,0.20,0.30,0.50,0.75,1.0,1.5,2.0,3.0,5.0,7.50,10.0,15.0,20.0,30.0,50.0,75.0,100.0]
const v0_grid = [0.01,0.02,0.03,0.05,0.075,0.1,0.15,0.20]
const n_grid = [2.0,3.0,4.0]
const η_grid = [0.001,0.002,0.005,0.01,0.02,0.05,0.1]

parameter_sets = [[τ,v0,n,η] for τ in τ_grid, v0 in v0_grid, n in n_grid, η in η_grid]
target_p_set = parse(Int64,ARGS[1])
(target_p_set > prod(size(parameter_sets))) && error("Input target p_set: $(target_p_set) is less than total size of possile parameter sets: $(prod(size(parameter_sets))).")
p = parameter_sets[target_p_set]

### Check File Existence ###
if isfile("../Data/$(target_folder)/FullEvaluation/behaviours_v0_$(p[2])_n_$(p[3])_η_$(p[4])/grid_τ_$(p[1])_v0_$(p[2])_n_$(p[3])_η_$(p[4]).jls")
    @warn "Evaluation already completed"
    exit()
end

### Fetches Packages ###
using Serialization

### Fetches Files ###
include("Functions/model_base.jl")
include("Functions/behaviour_lists.jl")
include("Functions/determine_behaviour.jl")
include("Functions/evaluate_behaviour.jl")
include("Functions/scanning_support.jl")

### Runs Parameter Scan ###
elapsed_time = @elapsed (evaluation = evaluate_sd_space_fast(S_grid,D_grid,p))
serialize("../Data/$(target_folder)/FullEvaluation/behaviours_v0_$(p[2])_n_$(p[3])_η_$(p[4])/grid_τ_$(p[1])_v0_$(p[2])_n_$(p[3])_η_$(p[4]).jls",evaluation)
serialize("../Data/$(target_folder)/CondensedEvaluation/behaviours_v0_$(p[2])_n_$(p[3])_η_$(p[4])/grid_τ_$(p[1])_v0_$(p[2])_n_$(p[3])_η_$(p[4]).jls",get_condensed_evaluation_grid(evaluation))
serialize("../Data/$(target_folder)/DeterminedBehaviour/behaviours_v0_$(p[2])_n_$(p[3])_η_$(p[4])/grid_τ_$(p[1])_v0_$(p[2])_n_$(p[3])_η_$(p[4]).jls",get_behaviour_grid(evaluation))
println("Finished run for $(target_p_set) : [$(p[1]),$(p[2]),$(p[3]),$(p[4])]. Runtime: $(elapsed_time)")

