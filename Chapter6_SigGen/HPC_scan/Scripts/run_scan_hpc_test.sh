#!/bin/bash

#SBATCH -A LOCKE-SL2-CPU
#SBATCH -J parameter_scan
#SBATCH -D /home/tel30/rds/hpc-work/projects/general_model_article_scan/Scripts
#SBATCH -o ../Logs/parameter_scan_test.log
#SBATCH -p cclake  ### or cclake-himem
#SBATCH -c 1                   # max 32 CPUs
#SBATCH --mem-per-cpu=5980MB   # max 5980MB or 12030MB for skilake-himem
#SBATCH -t 01:00:00            # HH:MM:SS with maximum 12:00:00 for SL3 or 36:00:00 for SL2

module load julia/1.6.2
julia run_scan_hpc.jl 100
