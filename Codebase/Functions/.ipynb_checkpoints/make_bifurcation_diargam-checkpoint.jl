# Internal structure for storing the important information in a bifurcation diagram.
struct BifurcationDiagram
    branches::Vector{NamedTuple{(:p, :u, :stab),Tuple{Array{Float64,1},Array{Float64,1},Bool}}}

    # Generates a bifurcation diagram.
    BifurcationDiagram(system::ReactionSystem,p::Vector{Float64},args...;kwargs...) = BifurcationDiagram(Model(system,p),args...;kwargs...)
    function BifurcationDiagram(model::Model,p_span::Tuple{Float64,Float64},par::Symbol;var=model.v_syms[1],dsmax=(p_span[2]-p_span[1])/1000.0,dsmin=dsmax/1000.0,maxSteps=10000)
        p_idx = get_par_idx(model,par); v_idx = get_var_idx(model,var);
        p_start = setindex!(copy(model.p_vals),p_span[1],p_idx)  
        odefun = ODEFunction(convert(ODESystem,model.system),jac=true)
        jet = BifurcationKit.getJet((u,p) -> odefun(u,p,0) , (u,p) -> odefun.jac(u,p,0); matrixfree=false)
        
        u0 = detsim(model.system,p_start,(0.,100.0);u0=fill(10.0,length(model.v_syms))).u[end]
        
        solve(ODEProblem(sys,rand(length(sys.states)),(0.,100.0),params_input),Rosenbrock23()).u[end];
        opts_br = ContinuationPar(pMin = p_span[1], pMax = p_span[2], dsmax = dsmax, dsmin = dsmin, ds=sqrt(dsmax*dsmin), maxSteps=maxSteps,
        detectBifurcation = 3, nInversion = 6, maxBisectionSteps = 25,nev = 20);

        # Calculates the diagram.
        bif = bifurcationdiagram(jet..., u0, p_start, (@lens _[p_idx]), 2,
            (x,p,level)->setproperties(opts_br);
            tangentAlgo = BorderedPred(),
            recordFromSolution=(x, p) -> x[v_idx], verbosity = 0, plot=false);
        
        idx_changes = [1,findall(getfield.(bif.γ.branch,:n_unstable)[1:end-1] .!= getfield.(bif.γ.branch,:n_unstable)[2:end])...,length(bif.γ.branch)]
        new(map((i1,i2) -> (p=getfield.(bif.γ.branch[i1:i2],:param),u=getfield.(bif.γ.branch[i1:i2],:x),stab=bif.γ.branch.n_unstable[i2]==0), idx_changes[1:end-1],idx_changes[2:end]))
    end
end
function plot_bif(bif::BifurcationDiagram;kwargs...)
    plot()
    for branch in branches
        plot!(branch.p,branch.u,color=branch.stab ? :blue : :red,linestyle=branch.stab ? :solid : :dash;kwargs...)
    end
    plot!()
end