### Figure Saving ###

# Saves a figure in four different ways (for thesis candidate figures).
function save_figure(figure,figurefolder;tag="",init="Figures")
    savefig(figure,"$(init)/$(figurefolder)/$(tag)generated_full.png")
    savefig(figure,"$(init)/$(figurefolder)/$(tag)generated_full.svg")
    savefig(plot!(figure;xguide="",yguide="",title="",legend=:none),"$(init)/$(figurefolder)/$(tag)inkscape_base.png")
    savefig(plot!(figure;xguide="",yguide="",title="",legend=:none),"$(init)/$(figurefolder)/$(tag)inkscape_base.svg");
end

# Saves a figure in two different ways (for other figures).
function save_figure_minor(figure,figurefolder,figurename)
    savefig(figure,"Figures/$(figurefolder)/$(figurename).png")
    savefig(figure,"Figures/$(figurefolder)/$(figurename).svg")
end