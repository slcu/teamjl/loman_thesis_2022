### Fetches Packages ###
using BifurcationKit
using LinearAlgebra
using Setfield


### Creates a Single Bifurcation Diagram ###

# Internal structure for stroing a bifurcation diagram (and making it).
struct BifurcationDiagram
    par::Symbol
    p_span::Tuple{Float64,Float64}
    branches::Vector{NamedTuple{(:p, :u, :stab),Tuple{Array{Float64,1},Array{Float64,1},Bool}}}
    peridoci_orbits::Union{Nothing,Any}
    bk_output::Any
    

    # Generates a bifurcation diagram.
    BifurcationDiagram(system::ReactionSystem,p::Vector{Float64},args...;kwargs...) = BifurcationDiagram(Model(system,p),args...;kwargs...)
    function BifurcationDiagram(model::Model,p_span::Tuple{Float64,Float64},par::Symbol;disjoint=false,var=model.v_syms[1],dsmax=(p_span[2]-p_span[1])/200.0,dsmin=dsmax/1000.0,maxSteps=100000)
        p_idx = get_par_idx(model,par); v_idx = get_var_idx(model,var);
        p_start = setindex!(copy(model.p_vals),p_span[1],p_idx)  
        odefun = ODEFunction(convert(ODESystem,model.system),jac=true)
        F = (u,p) -> odefun(u,p,0)      
        J = (u,p) -> odefun.jac(u,p,0)

        u0 = detsim(model.system,p_start,(0.,10000.0);u0=fill(1.0,length(model.v_syms))).u[end]
        
        if !disjoint
            opts_br = ContinuationPar(pMin = p_span[1], pMax = p_span[2], dsmax = dsmax, dsmin = dsmin, ds=sqrt(dsmax*dsmin), maxSteps=maxSteps,
                detectBifurcation = 3, nInversion = 6, maxBisectionSteps = 25,nev = 20);

            # Calculates the diagram.
            bk_output, = continuation(F,J, u0, p_start,  (@lens _[p_idx]), opts_br;
                recordFromSolution=(x, p) -> x[v_idx],
                tangentAlgo = BorderedPred(),
                plot = false, verbosity = 0, normC = x->norm(x, Inf))
            unmerged_branches = extract_path.(filter(x -> all(getfield.(x.branch,:x) .>= 0), [bk_output]));
            branches = simple_branch_merge(unmerged_branches,(0.1,13.5))                
        else
            opts = ContinuationPar(dsmax=dsmax, dsmin=dsmin, ds=sqrt(dsmax*dsmin), maxSteps=maxSteps, pMin=p_span[1], pMax=p_span[2], detectBifurcation=3,
                                    newtonOptions = NewtonPar(tol = 1e-9, verbose = false, maxIter = 15))                                    
            DO = DeflationOperator(2.0, dot, 1., [u0]);
            
            bk_output, = continuation(F, J, p_start, (@lens _[p_idx]) ,opts , DO, verbosity = 0, plot=false,                                                    
                recordFromSolution=(x, p) -> x[v_idx],
                perturbSolution = (x,p,id) -> (x  .+ 0.8 .* rand(length(x))), 
                callbackN = (x, f, J, res, iteration, itlinear, options; kwargs...) -> res <1e7); 
            unmerged_branches = extract_path.(filter(x -> all(getfield.(x.branch,:x) .>= 0), bk_output));
            branches = simple_branch_merge(unmerged_branches,(0.1,13.5))
        end
        new(par,p_span,vcat(split_branch.(branches)...),nothing,bk_output)
    end
end

# Extracts paths from continuation output.
function extract_path(branch)
    output = (p=getfield.(branch.branch,:param), u=getfield.(branch.branch,:x), stabs=getfield.(branch.branch,:stable) .== 1)
    if output.p[1]>output.p[end]
        reverse!(output.p)
        reverse!(output.u)
        reverse!(output.stabs)
    end
    return output    
end

# Merges branches which belongs together.
function simple_branch_merge(branches,p_span)
    psl = p_span[2]-p_span[1]
    new_branches = []; skipIdxs = [];
    for (i,b) in enumerate(branches)
        in(i,skipIdxs) && continue
        (b.p[1]==p_span[1]) && (b.p[end]==p_span[2]) && (push!(new_branches,b); continue;)
        for j = (i+1):length(branches)
            if (abs(b.p[1]-branches[j].p[1]) < (psl/200)) && (abs(b.p[end]-branches[j].p[end]) < (psl/200))
                push!(skipIdxs,j); push!(skipIdxs,i);
                push!(new_branches,(p=vcat(reverse(b.p),branches[j].p),u=vcat(reverse(b.u),branches[j].u),stabs=vcat(reverse(b.stabs),branches[j].stabs)))
                break
            end
        end
        !in(i,skipIdxs) && push!(new_branches,b)
    end
    return new_branches
end

# Splits a branch into stable and unstable branches.
function split_branch(branch)
    idx_changes = [1,findall(branch.stabs[1:end-1] .!= branch.stabs[2:end])...,length(branch.stabs)]
    return map((i1,i2) -> (p=branch.p[i1:i2],u=branch.u[i1:i2],stab=branch.stabs[i2]), idx_changes[1:end-1],idx_changes[2:end])
end;


### Creates a Range of Bifurcation Diagrams ###

# Internal structure for stroing a range of bifurcation diagrams (and making them).
struct BifurcationDiagrams
    p_grid::Vector{Float64}
    par::Symbol
    bifs::Vector{BifurcationDiagram}
    n::Int64

    # Generates a bifurcation diagram.
    BifurcationDiagrams(system::ReactionSystem,p::Vector{Float64},args...;kwargs...) = BifurcationDiagrams(Model(system,p),args...;kwargs...)
    function BifurcationDiagrams(model::Model,p_span::Tuple{Float64,Float64},par_cont::Symbol,p_grid::Vector{Float64},par_disc::Symbol;kwargs...)
        bifs = map(p_val -> BifurcationDiagram(model.system,setindex!(deepcopy(model.p_vals),p_val,get_par_idx(model,par_disc)),p_span,par_cont;kwargs...), p_grid)
        new(p_grid,par_disc,bifs,length(p_grid))
    end
end


### Plots Bifurcation Diagrams ###

# Plots a bifurcation diagram.
plot_bif(args...;kwargs...) = (plot(); plot_bif!(args...;kwargs...);)
function plot_bif!(bif::BifurcationDiagram;xguide=bif.par,xlimit=bif.p_span,label="",cS=:blue,cU=:red,lsS=:solid,lsU=:dot,kwargs...)
    for branch in bif.branches
        plot!(branch.p,branch.u,color=branch.stab ? cS : cU,linestyle=branch.stab ? lsS : lsU;xguide=xguide,xlimit=xlimit,label=label,kwargs...)
    end
    plot!()
end

# Creates and plots bifurcation diagram
make_plot_bif(model::Model,p_span::Tuple{Float64,Float64},par::Symbol;disjoint=false,var=model.v_syms[1],dsmax=(p_span[2]-p_span[1])/1000.0,dsmin=dsmax/1000.0,maxSteps=100000,label="",kwargs...) = plot_bif(BifurcationDiagram(model,p_span,par;disjoint=disjoint,var=var,dsmax=dsmax,dsmin=dsmin,maxSteps=maxSteps);label=label,kwargs...)
make_plot_bif(system::ReactionSystem,p::Vector{Float64},p_span::Tuple{Float64,Float64},par::Symbol;disjoint=false,var=Symbol(system.states[1].val.f),dsmax=(p_span[2]-p_span[1])/1000.0,dsmin=dsmax/1000.0,maxSteps=100000,label="",kwargs...) = plot_bif(BifurcationDiagram(system,p,p_span,par;disjoint=disjoint,var=var,dsmax=dsmax,dsmin=dsmin,maxSteps=maxSteps);label=label,kwargs...)
make_plot_bif!(model::Model,p_span::Tuple{Float64,Float64},par::Symbol;disjoint=false,var=model.v_syms[1],dsmax=(p_span[2]-p_span[1])/1000.0,dsmin=dsmax/1000.0,maxSteps=100000,label="",kwargs...) = plot_bif!(BifurcationDiagram(model,p_span,par;disjoint=disjoint,var=var,dsmax=dsmax,dsmin=dsmin,maxSteps=maxSteps);label=label,kwargs...)
make_plot_bif!(system::ReactionSystem,p::Vector{Float64},p_span::Tuple{Float64,Float64},par::Symbol;disjoint=false,var=Symbol(system.states[1].val.f),dsmax=(p_span[2]-p_span[1])/1000.0,dsmin=dsmax/1000.0,maxSteps=100000,label="",kwargs...) = plot_bif!(BifurcationDiagram(system,p,p_span,par;disjoint=disjoint,var=var,dsmax=dsmax,dsmin=dsmin,maxSteps=maxSteps);label=label,kwargs...)

# Plots a set of bifurcation diagrams.
plot_bifs(args...;kwargs...) = (plot(); plot_bifs!(args...;kwargs...);)
function plot_bifs!(bifs::BifurcationDiagrams;xguide=bifs.bifs[1].par,xlimit=bifs.bifs[1].p_span,label="",labels=fill(label,1,bifs.n),cS=:blue,cU=:red,cSs=fill(cS,1,bifs.n),cUs=fill(cU,1,bifs.n),lsS=:solid,lsU=:dot,kwargs...)
    for (i,bif) in enumerate(bifs.bifs)
        plot_bif!(bif;xguide=xguide,xlimit=xlimit,label=labels[i],cS=cSs[i],cU=cUs[i],lsS=lsS,lsU=lsU,kwargs...)
    end
    plot!()
end

# Creates and plots a set of bifurcation diagrams
make_plot_bifs(model::Model,p_span::Tuple{Float64,Float64},par_cont::Symbol,p_grid::Vector{Float64},par_disc::Symbol;disjoint=false,var=model.v_syms[1],dsmax=(p_span[2]-p_span[1])/1000.0,dsmin=dsmax/1000.0,maxSteps=100000,label="",kwargs...) = plot_bifs(BifurcationDiagrams(model,p_span,par_cont,p_grid,par_disc;disjoint=disjoint,var=var,dsmax=dsmax,dsmin=dsmin,maxSteps=maxSteps);label=label,kwargs...)
make_plot_bifs(system::ReactionSystem,p::Vector{Float64},p_span::Tuple{Float64,Float64},par_cont::Symbol,p_grid::Vector{Float64},par_disc::Symbol;disjoint=false,var=Symbol(system.states[1].val.f),dsmax=(p_span[2]-p_span[1])/1000.0,dsmin=dsmax/1000.0,maxSteps=100000,label="",kwargs...) = plot_bifs(BifurcationDiagrams(system,p,p_span,par_cont,p_grid,par_disc;disjoint=disjoint,var=var,dsmax=dsmax,dsmin=dsmin,maxSteps=maxSteps);label=label,kwargs...)
make_plot_bifs!(model::Model,p_span::Tuple{Float64,Float64},par_cont::Symbol,p_grid::Vector{Float64},par_disc::Symbol;disjoint=false,var=model.v_syms[1],dsmax=(p_span[2]-p_span[1])/1000.0,dsmin=dsmax/1000.0,maxSteps=100000,label="",kwargs...) = plot_bifs!(BifurcationDiagrams(model,p_span,par_cont,p_grid,par_disc;disjoint=disjoint,var=var,dsmax=dsmax,dsmin=dsmin,maxSteps=maxSteps);label=label,kwargs...)
make_plot_bifs!(system::ReactionSystem,p::Vector{Float64},p_span::Tuple{Float64,Float64},par_cont::Symbol,p_grid::Vector{Float64},par_disc::Symbol;disjoint=false,var=Symbol(system.states[1].val.f),dsmax=(p_span[2]-p_span[1])/1000.0,dsmin=dsmax/1000.0,maxSteps=100000,label="",kwargs...) = plot_bifs!(BifurcationDiagrams(system,p,p_span,par_cont,p_grid,par_disc;disjoint=disjoint,var=var,dsmax=dsmax,dsmin=dsmin,maxSteps=maxSteps);label=label,kwargs...)