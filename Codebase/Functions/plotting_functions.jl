### Fetch Packages ###
using Plots, Plots.PlotMeasures


### Declarres an Empty Plot ###
p0 = plot(xguide="",yguide="",xticks=[],yticks=[],frame=:none)


### Plots Basic Shapes ###

# Plots a vertical line (to e.g. mark stress addition).
plot_vertical_line(args...;kwargs...) = (plot(); plot_vertical_line!(args...;kwargs...);)
plot_vertical_line!(x_value,heigth;base=0.0,lw=4,la=1.0,linestyle=:dot,color=:red,label="",kwargs...) = plot!([x_value,x_value],[base,heigth];lw=lw,la=la,color=color,linestyle=linestyle,label=label,kwargs...)
# Plots several vertical lines.
plot_vertical_lines(args...;kwargs...) = (plot(); plot_vertical_lines!(args...;kwargs...);)
function plot_vertical_lines!(x_values,heigth;base=0.0,lw=3,la=1.0,linestyle=:dot,color=:red,label="",kwargs...)
    foreach(x_val->plot_vertical_line!(x_val,heigth;base=base,lw=lw,la=la,linestyle=linestyle,color=color,label=""), x_values)
    return plot!([],[];linestyle=linestyle,lw=lw,la=la,color=color,label=label,kwargs...)
end

# Plots a pattern of stress activation/deactivation.
plot_stress_pattern(args...;kwargs...) = (plot(); plot_stress_pattern!(args...;kwargs...);)
function plot_stress_pattern!(addition_times,removal_times,heigth;base=0.0,lw=4,la=1.0,linestyle=:dot,addition_color=:red,removal_color=:black,addition_label="",removal_label="",kwargs...)
    println("Probably remove and repalce with function: plot_vertical_lines_multi.")
    plot_vertical_lines!(addition_times,heigth;base=base,lw=lw,la=la,linestyle=linestyle,color=addition_color,label=addition_label,kwargs...)
    plot_vertical_lines!(removal_times,heigth;base=base,lw=lw,linestyle=linestyle,color=removal_color,label=removal_label,kwargs...)
end

# Plots a pattern of several types of vertical lines.
plot_vertical_lines_multi(args...;kwargs...) = (plot(); plot_vertical_lines_multi!(args...;kwargs...);)
function plot_vertical_lines_multi!(x_values,heigth;base=0.0,lw=4,lws=fill(lw,length(x_values)),la=1.0,las=fill(la,length(x_values)),linestyle=:dot,linestyles=fill(linestyle,length(x_values)),color=:red,colors=fill(color,length(x_values)),label="",labels=fill(label,length(x_values)),kwargs...)
    map(i -> plot_vertical_lines!(x_values[i],heigth;base=base,lw=lws[i],la=las[i],linestyle=linestyles[i],color=colors[i],label=labels[i],kwargs...), 1:length(x_values))
    plot!()
end


# Plots a pattern of stress activation/deactivation and induction.
function plot_stress_induction_pattern(addition_times,removal_times,induction_times,heigth)
    plot_stress_pattern(addition_times,removal_times,heigth)
    foreach(it -> plot!([it,it],[0,heigth],label="",linewidth=4,linestyle = :dot,color=:purple), induction_times)
    return plot!()
end
function plot_stress_induction_pattern!(addition_times,removal_times,induction_times,heigth)
    plot_stress_pattern!(addition_times,removal_times,heigth)
    foreach(it -> plot!([it,it],[0,heigth],label="",linewidth=4,linestyle = :dot,color=:purple), induction_times)
    return plot!()
end

# Plots a step (e.g. to mark stress levels)
plot_step(args...;kwargs...) = (plot(); plot_step!(args...;kwargs...);)
plot_step!(step_time,end_time,step_heigth;start_time=0.0,base_heigth=0.0,lw=4,la=1.0,linestyle=:solid,color=:red,label="",kwargs...) = plot!([start_time,step_time,step_time,end_time],[base_heigth,base_heigth,step_heigth,step_heigth];lw=lw,la=la,color=color,linestyle=linestyle,label=label,kwargs...)
