## Fetch Packages ###
using DifferentialEquations


### ODE Simulation ###
detsim(system::ReactionSystem,p_vals::Vector{Float64},args...;kwargs...) = detsim(Model(system,p_vals),args...;kwargs...)
function detsim(model::Model, tspan; u0=get_u0_rre(model;tleng=tspan[2]), solver=Rosenbrock23(), p_steps=(), v_steps=(), callbacks=(), kwargs...)
    prob = ODEProblem(model.system,u0,tspan,deepcopy(model.p_vals))
    return OrdinaryDiffEq.solve(prob,solver;callback=CallbackSet(par_steps(p_steps,model.p_syms)...,var_steps(v_steps,model.v_syms)...,callbacks...),tstops=find_tstops(p_steps,v_steps),kwargs...)
end


### SDE Simulation  ###

# Makes a single stochastic simulations.
stochsim(system::ReactionSystem,p_vals::Vector{Float64},args...;kwargs...) = stochsim(Model(system,p_vals),args...;kwargs...)
function stochsim(model::Model, tspan::Tuple{Float64,Float64}; dt=0.001, adaptive=true, u0=get_u0s_cle(model;tleng=tspan[2],dt=dt,adaptive=adaptive)[1], solver=ImplicitEM(), p_steps=(), v_steps=(), callbacks=(), kwargs...)
    prob = SDEProblem(model.system,u0,tspan,deepcopy(model.p_vals);noise_scaling=model.noise_scaling)
    return StochasticDiffEq.solve(prob,solver;callback=CallbackSet(positive_domain(),par_steps(p_steps,model.p_syms)...,var_steps(v_steps,model.v_syms)...,callbacks...),tstops=find_tstops(p_steps,v_steps), dt=dt, adaptive=adaptive, kwargs...)
end

# Makes several stochastic simulations.
monte(system::ReactionSystem,p_vals::Vector{Float64},args...;kwargs...) = monte(Model(system,p_vals),args...;kwargs...)
function monte(model::Model, tspan::Tuple{Float64,Float64}, n; dt = 0.001, adaptive = true, u0=nothing, u0s=get_monte_u0s(model,tspan,n,u0;dt=dt,adaptive=adaptive), solver=ImplicitEM(), eSolver=EnsembleThreads(), p_steps=(), v_steps=(), callbacks=(), kwargs...)
    prob = SDEProblem(model.system,u0s[1],tspan,deepcopy(model.p_vals);noise_scaling=model.noise_scaling)
    ensemble_prob = EnsembleProblem(prob,prob_func=(p,i,r)->remake(p;p=deepcopy(model.p_vals),u0=u0s[i]),safetycopy=false)
    return StochasticDiffEq.solve(ensemble_prob,solver,eSolver;trajectories=n,callback=CallbackSet(positive_domain(),par_steps(p_steps,model.p_syms)...,var_steps(v_steps,model.v_syms)...,callbacks...),tstops=find_tstops(p_steps,v_steps), dt=dt, adaptive=adaptive, kwargs...)
end
get_monte_u0s(model,tspan,n,u0;kwargs...) = (u0 !== nothing) ? fill(u0,n) : get_u0s_cle(model,n;tleng=tspan[2],kwargs...)


### SSA Simulation ###

# Makes a single ssa simulations.
ssasim(system::ReactionSystem,p_vals::Vector{Float64},args...;kwargs...) = ssasim(Model(system,p_vals),args...;kwargs...)
function ssasim(model::Model, tspan::Tuple{Float64,Float64}; u0=get_u0s_gillespie(model;tleng=tspan[2])[1],solver=SSAStepper(), saveat=1., callbacks=(), p_steps=(), v_steps=(), kwargs...)
    dprob = DiscreteProblem(model.system, u0, tspan, deepcopy(model.p_vals))
    jprob = JumpProblem(model.system, dprob, Direct(), save_positions=(false,false))    
    return DiffEqJump.solve(jprob,solver;callback=CallbackSet(par_steps(p_steps,model.p_syms)...,var_steps(v_steps,model.v_syms)...,callbacks...),tstops=find_tstops(p_steps,v_steps), saveat=saveat, kwargs...)
end

# Makes several ssa simulations.
ssamonte(system::ReactionSystem,p_vals::Vector{Float64},args...;kwargs...) = ssamonte(Model(system,p_vals),args...;kwargs...)
function ssamonte(model::Model, tspan::Tuple{Float64,Float64}, n; u0=nothing, u0s=get_ssamonte_u0s(model,tspan,n,u0), eSolver=EnsembleThreads(), solver=SSAStepper(),  saveat=1., callbacks=(), p_steps=(), v_steps=(), kwargs...)
    dprob = DiscreteProblem(model.system, u0s[1], tspan, deepcopy(model.p_vals))
    jprob = JumpProblem(model.system, dprob, Direct(),save_positions=(false,false))    
    ensemble_prob = EnsembleProblem(jprob,prob_func=(p,i,r)->remake(p;p=deepcopy(model.p_vals),u0=u0s[i]),safetycopy=false)
    return DiffEqJump.solve(ensemble_prob,solver,eSolver;trajectories=n,callback=CallbackSet(par_steps(p_steps,model.p_syms)...,var_steps(v_steps,model.v_syms)...,callbacks...),tstops=find_tstops(p_steps,v_steps), saveat=saveat, kwargs...)
end
get_ssamonte_u0s(model::Model, tspan::Tuple{Float64,Float64},n,u0;kwargs...) = (u0 !== nothing) ? fill(u0,n) : get_u0s_gillespie(model,n;tleng=tspan[2],kwargs...)


### Callbacks ###

#A callback for making a step increase in a single parameter.
function par_step(p_idx,step_time,step_value)
    condition(u,t,integrator) = (t==step_time)
    affect!(integrator) = integrator.p[p_idx] += step_value
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end
#A callback for making several step increases, potentially over several parameters.
function par_steps(p_steps,p_syms)
    output = Vector{DiscreteCallback}()
    for steps in my_split(p_steps), step in steps[2:end]
        push!(output,par_step(findfirst(p_syms.==steps[1]),step...))
    end
    return output
end

#A callback for making a step increase in a single variable.
function var_step(v_idx,step_time,step_value)
    condition(u,t,integrator) = (t==step_time)
    affect!(integrator) = integrator.u[v_idx] += step_value
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end
#A callback for making several step increases, potentially over several variables.
function var_steps(v_steps,v_syms)
    output = Vector{DiscreteCallback}()
    for steps in my_split(v_steps), step in steps[2:end]
        push!(output,var_step(findfirst(v_syms.==steps[1]),step...))
    end
    return output
end

#A callback for keeping a simulation within the positive domain.
function positive_domain()
    condition(u,t,integrator) = minimum(u) < 0
    affect!(integrator) = integrator.u .= integrator.uprev
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end


### Callbacks Auxillary Functions ###

#Splits an array acording to my desires.
function my_split(array)
    starts = findall(typeof.(array).==Symbol)
    ends = [starts[2:end]...,length(array)+1].-1
    return [array[starts[i]:ends[i]] for i in 1:length(starts)]
end

#Finds the tstops for a given callback vector.
function find_tstops(p_steps,v_steps)
    first.(filter(i->typeof(i)!=Symbol,[p_steps...,v_steps...]))
end