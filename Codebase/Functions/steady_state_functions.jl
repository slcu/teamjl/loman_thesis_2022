### Simulation Based Steady State Finding ###

# ODE-based ss finder.
function get_u0_rre(model;u0_base=fill(10.0,length(model.v_syms)),tleng=1000.0)
    (model.u0_rre === nothing) && (model.u0_rre = detsim(model,(0.,tleng);u0=u0_base).u[end])
    return model.u0_rre
end

# SDE-based ss finder.
function get_u0s_cle(model,n=1;u0_base=fill(10.0,length(model.v_syms)),tleng=100.,nsims=ceil(Int64,n/5+1),dt=0.001,adaptive=true)
    if length(model.u0s_cle) < 10*nsims
        sols = monte(model,(0.,tleng),nsims; u0=u0_base,dt=dt,adaptive=adaptive,saveat=1.)
        foreach(sol -> push!(model.u0s_cle,rand(sol.u[ceil(Int64,length(sol.t)/2):end],10)...), sols)
    end
    return rand(model.u0s_cle,n)
end

# SSA-based ss finder.
function get_u0s_gillespie(model,n=1;u0_base=fill(10,length(model.v_syms)),tleng=tspan[2]/10.,nsims=ceil(Int64,n/5+1))
    if length(model.u0s_gillespie) < 10*nsims
        sols = ssamonte(model,(0.,tleng),nsims; u0=u0_base)
        foreach(sol -> push!(model.u0s_gillespie,rand(sol.u[ceil(Int64,length(sol.t)/2):end],10)...), sols)
    end
    return rand(model.u0s_gillespie,n)
end