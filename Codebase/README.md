### Codebase

This folder contains a library of functions that are used throughout the thesis. It also contains a copy of the Manifest.toml and Project.toml files defining the main environment used throughout the thesis.