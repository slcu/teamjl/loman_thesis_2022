# How bacteria modulate mixed positive/negative feedback loops to create desired phenotypes

This repository contains all files related to Torkel Loman's PhD thesis. This includes files for making simulations, analysing data, and generating plots corresponding to those in the thesis. Files with code contain comments to aid understanding. All code is written in the Julia programming language (version 1.7).

### Reproducibility

All folders contain a "Project.toml" and a "Manifest.toml" folder (these are identical across almost all chapters). These can be used to recreate the exact same package versions that I used to run them initially. The only sections using a different environment are those scripts that computer periodic orbits of bifurcation diagram, those performing the benchmarks of Catalyst, and those that scan model behaviours across parameter space on an HPC.

### Further instructions

Each folder contains a separate README, further describing its content.

### Adding missing folders.

Due to their large sizes, some folders are not included in this repository (there should be 4 such folders in total, 3 with data and 1 with figures). These have been replaced with text files with instructions for where to download the actual folder (which is then put into the directory of the text file).
